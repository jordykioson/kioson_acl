<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function getMyipaddress()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP'])){ $ip=$_SERVER['HTTP_CLIENT_IP']; }
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }
	else { $ip=$_SERVER['REMOTE_ADDR']; }
	return $ip;
}
function sekarang(){
	return gmdate('Y-m-d H:i:s',time()+25200);
}

function dateNow() {
	return gmdate('Y-m-d',time()+25200);
}

function timeNow() {
	return gmdate('H:i:s',time()+25200);
}

function cetak($str){
	echo htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function sendEmail($emailData){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://api.jordymalonda.com/email.php"); // change this to Push Notification API
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);

	$data = array('data' => json_encode($emailData));

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);

	return $output;
}