<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class History_model extends CI_Model {
        private $table = 'tb_batch_history';

        public function add($data){
            $count = count($data);
            $pesan = 'Gagal';
            $res = $this->db->insert_batch($this->table , $data);
            if($res != 0){
                $pesan = "Sukses insert {$count} data";
            }
            echo json_encode(compact('pesan'));
        }

        public function addById($id){
            $pesan = "Mohon periksa kembali status batch id anda";
            $sql="SELECT * FROM tb_batch where batch_id = ".$this->db->escape($id) . " AND status = '1'";
            // $this->db->get_where();
            $query=$this->db->query($sql);
            if ($data=$query->result()) {
				foreach($data as $key => $value){
                    $campaing_name_id = $value->campaign_name_id;
                    $las = $value->last_action_sequence;
                    $kcp_id = $value->kcp_id;
                    $mobile_no = $value->mobile_no;
                    $tag_id = $value->tag_id;
                    $action_date = $value->updated_date;
                    $status = $value->status;
                }
                $param = array(
                    'campaign_name_id' => $campaing_name_id,
                    'sequence' => $las
                );
                $que = $this->db->get_where('tb_sequences' , $param);
                if($result = $que->result()){
                    foreach ($result as $key => $value) {
                        $sequences_id = $value->sequences_id;
                    }
                    $historyData[] = array(
                        'batch_id' => $id,
                        'kcp_id' => $kcp_id,
                        'mobile_no' => $mobile_no,
                        'campaign_name_id' => $campaing_name_id,
                        'sequence_id' => $sequences_id,
                        'tag_id' => $tag_id,
                        'action_status' => 'DONE',
                        'action_date' => $action_date,  
                        'status' => $status
                    );
                    $this->add($historyData);
                }  
            }else{
                echo json_encode(compact('pesan'));
            }
        }
        

    }