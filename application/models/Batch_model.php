<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Batch_model extends CI_Model {
        
        private $table = "tb_batch";

        public function getAll(){
            $que = $this->db->get($this->table);
            if($que->num_rows != 0){
                $data = array(array('kcp_id' => "0000" , 'mobile_no' => "0000", 'campaign_name_id' => "0"));
            }
            else{
                $data = $que->result_array();
            }
            return json_encode(compact('data'));
        }

        public function getBatchInactive($id){
            $sql="SELECT a.*, b.tag_name, c.* FROM {$this->table} a INNER JOIN tb_tags b ON a.tag_id = b.tag_id INNER JOIN tb_campaign_name c ON a.campaign_name_id = c.id where campaign_name_id = ".$this->db->escape($id) . " AND a.status = '0'";
            $query=$this->db->query($sql);
            if ($data=$query->result()) {
				return json_encode(compact('data'));
			}
        }

        public function updateLastAction($id , $param){
			log_message('DEBUG', 'Update Last Action Sequence Status executed by ' . $this->session->name);
			$pesan="gagal";
			$data = array(
               'last_action_sequence' => $param
			);		
			$this->db->where('batch_id', $id);
			$response = $this->db->update($this->table, $data); 
			if ($response) {
				$pesan = 'sukses update batch_id' . $id;
			}
			return json_encode(compact('pesan'));
        }
        
        public function getIntervalDay($id , $delay_time){
			$sql="select updated_date + interval {$delay_time} day as interval_day from {$this->table}  where batch_id = {$id}";
			$query=$this->db->query($sql);
			if($data = $query->result()){
				return $data[0]->interval_day;
			}
        }
        public function updateDate($id){
			log_message('DEBUG', 'Update Updated_date executed by ' . $this->session->name);
			$pesan="gagal";
			$data = array(
			//    'updated_date' => '2018-04-21 19:00:00' // untuk testing
				'updated_date' => sekarang()
			);		
			$this->db->where('batch_id', $id);
			$response = $this->db->update($this->table, $data); 
			if ($response) {
				$que = $this->db->select('updated_date')->where('batch_id',$id)->get($this->table);
				$result = $que->result();
				if($result != 0){
					foreach($result as $key => $value){
						$data = $value->updated_date;
					}
				}
			}
			return $data;
		}
    }