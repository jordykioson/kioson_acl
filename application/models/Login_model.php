<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Login_model extends CI_Model {
		var $hash='cWd2MjxjLQ8PVvkuV8yXDR4Vjw3qeTPDFFa2Vaqi2zLALzf5Da2giL8mECz4Jkup';
		function __construct(){
			// parent::__construct();
			// $this->encryption->initialize(
			// 	array(
			// 		'cipher' => 'aes-256',
			// 		'mode' => 'ctr',
			// 		'key' => '7RjQuHqGk552uTi5'
			// 	)
			// );
			// $this->auth();
		}


		public function logout(){
			log_message('DEBUG', $this->session->name . " Logged Out .");
			$this->session->sess_destroy();
			redirect(base_url());
		}


		// Testing Session
		public function testSession(){
			$username = 'jordy';
			$password = 'jordy@crm';
			$data = $this->session->set_userdata(compact('username', 'password'));
			print_r($_SESSION);
		}

		private function getLevel($level){
			$sql="SELECT keterangan FROM tb_level WHERE level = {$level}";
			$que=$this->db->query($sql);
			$res=$que->row();
			return $res->keterangan;
		}


		public function authenticate(){
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$iplogin= getMyipaddress();
			$lastlogin= sekarang();
			$status='Gagal';
			$pesan='Mohon periksa Nama Pengguna dan Kata Kuncinya';
			$password = hash('sha256', $this->hash.$password);

			$sql="SELECT * FROM tb_users WHERE username=".$this->db->escape($username)." AND  password='{$password}'";
			$que=$this->db->query($sql);
			if($res=$que->row()){
				$ID=$res->user_id;
				$status='Sukses';
				$pesan='Login Berhasil';
				$name=$res->name;
				$level= $this->getLevel($res->level);
				$access = $this->getAccess($ID);
				$this->session->set_userdata(compact('level','username','name','ID' , 'access'));
				$sql="UPDATE tb_users SET last_login_time='{$lastlogin}',last_login_ip='{$iplogin}' WHERE user_id='{$res->user_id}'";
				$this->db->query($sql);
				log_message('DEBUG', $this->session->name . " Logged Out .");
			}
			return json_encode(compact('status','pesan','name','level'));
		}

		private function getAccess($id){
			$this->load->model('Access_model' , 'access');
			$data = $this->access->getAccess($id);
			return $data;
		}

	}
