<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model {


	var $table = 'tb_reporting';
	var $column_order = ['kcp_name','campaign_name','tag','campaign_name','phone_number','date_hitted',null];  
	var $column_search = ['kcp_name','campaign_name','tag','campaign_name','phone_number','date_hitted']; 
	var $order = ['datetime_hitted' => 'desc']; 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		// Add clone Post Campaign
		$campaign =  $this->input->post('campaign');
		$campaign2 =  $this->input->post('campaign2');
		$campaign3 =  $this->input->post('campaign3');
		// $c=[];
		// $c = ['never top up','never kyc'];
	
		if($this->input->post('campaign'))
		{
			$this->db->where("campaign_name IN ('$campaign','$campaign2','$campaign3')
			 ");
			//  $this->db->where("status_progress = 'Call (On Progress)' 
			//  ");
		}
		// else if($this->input->post('campaign') && $this->input->post('status')){

		// }
		// Add Clone Post tag
		$tag =  $this->input->post('tag');
		$tag2 =  $this->input->post('tag2');
		$tag3 =  $this->input->post('tag3');


		if($this->input->post('tag'))
		{
			// $this->db->where("tag = '$tag' 
			// OR tag = '$tag2' OR tag = '$tag3'
			//  ");
			 $this->db->where("tag IN ('$tag','$tag2','$tag3')
			 ");
		}
		// Add Clone Post Status
		$status =  $this->input->post('status');
		$status2 =  $this->input->post('status2');
		$status3 =  $this->input->post('status3');


		if($this->input->post('status'))
		{
			$this->db->where("status_progress IN ('$status','$status2','$status3') 
			");
		}

		

		$this->db->from($this->table);
		//nylain ini for limit show
		// $this->db->where('campaign_name < ',4 );
		// $this->db->where('tag','Never Top up');


		$i = 0;
	
		foreach ($this->column_search as $item) 
		{
			if($_POST['search']['value']) 
			{
				
				if($i===0) 				
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($_POST['order'])) 
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_custom_field();
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_custom_field();		
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	
	public function get_by_id($r_id)
	{
		$query=$this->db->query("select
		campaign_name,retailer_id,kcp_name,phone_number,max(status) as status,address,
		sum(SMS) as SMS,
		sum(Push_Notification) as Push_Notification,
		sum(Call1) as Call1,
		sum(Call2) as Call2,
		sum(Call3) as Call3
	  from (select
		tb_reporting.*,
		case when type_name = 'SMS' then date(date_hitted) end as SMS,
		case when type_name = 'Push_Notification' then date(date_hitted)end as Push_Notification,
		case when type_name = 'Call1' then date(date_hitted) end as Call1,
		case when type_name = 'Call2' then date(date_hitted) end as Call2,
		case when type_name = 'Call3' then date(date_hitted) end as Call3
	  from tb_reporting where retailer_id = '".$r_id."') as reporting
	  group by campaign_name");
			// $result = $query->result_array();
		return $query->result_array();
	
		
	}

	public function get_last_status_report($r_id)
	{
		$query=$this->db->query("select t.status
		from tb_reporting t
		inner join (
			select campaign_name, max(datetime_hitted) as MaxDate
			from tb_reporting
			
			group by campaign_name
		) tm on t.campaign_name = tm.campaign_name and t.datetime_hitted = tm.MaxDate
		where retailer_id = '".$r_id."' order by datetime_hitted desc ");
			// $result = $query->result_array();
		return $query->result_array();
	
		
	}


	public function _get_custom_field()
    {
	//   print_r($this->input->post('date_start'));
	//   print_r($this->input->post('date_end'));
	//   echo $start;die();
	// print_r($start.'- dasda');die();
	// print_r($start.' -ewe ');

        if(isset($_POST['columns'][5]['search']['value']) and $_POST['columns'][5]['search']['value'] !='' )
		{

            $this->db->where('type_name',$_POST['columns'][5]['search']['value']);

		}

		if(isset($_POST['columns'][3]['search']['value']) and $_POST['columns'][3]['search']['value'] !='')
		{
			
             $this->db->where('campaign_name',$_POST['columns'][3]['search']['value']);
		}
		if(isset($_POST['columns'][4]['search']['value']) and $_POST['columns'][4]['search']['value'] !='')
		{
             $this->db->where('tag',$_POST['columns'][4]['search']['value']);
		}


		if(isset($_POST['columns'][6]['search']['value']) and $_POST['columns'][6]['search']['value'] !='')

		{
	
			 $this->db->where('date_hitted < ',$_POST['columns'][6]['search']['value']);
			 
		}

	}

	public function get_list_campaigns()
	{
		$this->db->select('campaign_name');
		$this->db->from($this->table);
		$this->db->order_by('campaign_name','asc');
		$query = $this->db->get();
		$result = $query->result();

		$campaigns = array();
		foreach ($result as $row) 
		{
			$campaigns[] = $row->campaign_name;
		}
		return $campaigns;
		// print_r($campaigns);
	}

	public function get_list_tags()
	{
		$this->db->select('tag');
		$this->db->from($this->table);
		$this->db->order_by('tag','asc');
		$query = $this->db->get();
		$result = $query->result();

		$tags = array();
		foreach ($result as $row) 
		{
			$tags[] = $row->tag;
		}
		return $tags;
		// print_r($campaigns);
	}
	public function get_list_status()
	{
		$this->db->select('status_progress');
		$this->db->from($this->table);
		$this->db->order_by('status_progress','asc');
		$query = $this->db->get();
		$result = $query->result();

		$statuses = array();
		foreach ($result as $row) 
		{
			$statuses[] = $row->status_progress;
		}
		return $statuses;
		// print_r($campaigns);
	}
	


	
	public function add($data){
		$count = count($data);
		$pesan = 'Gagal';
		$res = $this->db->insert_batch('tb_reporting' , $data);
		if($res != 0){
			$pesan = "Sukses insert {$count} data";
		}
		echo json_encode(compact('pesan'));
	}

	public function addById($id){
		
		$pesan = "Mohon periksa kembali status batch id anda";
		$sql="SELECT a.*, b.tag_name, c.* FROM tb_batch a INNER JOIN tb_tags b ON a.tag_id = b.tag_id INNER JOIN	tb_campaign_name c ON a.campaign_name_id = c.id where batch_id = ".$this->db->escape($id) . " AND a.status = '1'";
		// $this->db->get_where();
		$query=$this->db->query($sql);
		if ($data=$query->result()) {
			foreach($data as $key => $value){
				$campaign_name_id = $value->campaign_name_id;
				$kcp_name = $value->kcp_name;
				$campaign_name = $value->campaign_name;
				$tag_name = $value->tag_name;
				$kcp_id = $value->kcp_id;
				$mobile_no = $value->mobile_no;
				$action_date = $value->updated_date;
				$status = $value->status;
				$sequences = $value->last_action_sequence;
				$address = $value->address;
			}

			// $que = $this->db->get_where('tb_sequences' , $param);
			$sql12="SELECT a.*, b.action_name FROM tb_sequences a inner join tb_actions b on a.action_id = b.action_id WHERE campaign_name_id = '{$campaign_name_id}' AND sequence = '{$sequences}'";				
			$que=$this->db->query($sql12);
			if($result = $que->result()){
				foreach ($result as $key => $value) {
					$action_name = $value->action_name;
				}
				$historyData[] = array(
					'batch_id' => $id,
					'retailer_id' => $kcp_id,
					'kcp_name' => $kcp_name,
					'campaign_name' => $campaign_name,
					'tag' => $tag_name,
					'type_name' => $action_name,
					'phone_number' => $mobile_no,
					'date_hitted' => $action_date,
					'status' => $status,
					'address' => $address,
				);
				$this->add($historyData);
			}  
		}else{
			echo json_encode(compact('pesan'));
		}
	}

}
