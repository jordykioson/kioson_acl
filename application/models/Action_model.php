<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Action_model extends CI_Model {

		private $table="tb_actions";

		
		public function getById($id){
			log_message('DEBUG', 'get action by id executed by ' . $this->session->name);
			$this->db->select('action_name');
			$query = $this->db->get_where($this->table, array('action_id' => $id));
			$data = $query->result();
			if ($data) {
				$pesan = "sukses";
			}
			else {
				$pesan="gagal";
			}
			return json_encode(compact('data' , 'pesan'));
		}

		
	}