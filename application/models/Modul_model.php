<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Modul_model extends CI_Model {

		private $table = "tb_modul";

		public function getById($id){
			log_message('DEBUG', 'get modul by id executed by ' . $this->session->name);
			$this->db->select('path, nama');
			$query = $this->db->get_where($this->table, array('modul_id' => $id));
			$data = $query->result();
			if ($data) {
				$pesan = "sukses";
			}
			else {
				$pesan="gagal";
			}
			return json_encode(compact('data' , 'pesan'));
		}

		public function getAll(){
			log_message('DEBUG', 'get all Modul executed by ' . $this->session->name);
			$query = $this->db->get($this->table);
			$data = $query->result_array();
			if ($data) {
				$pesan = "sukses";
				return $data;	
			}
		}

	}
