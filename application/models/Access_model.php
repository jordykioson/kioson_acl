<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Access_model extends CI_Model {
		var $hash='cWd2MjxjLQ8PVvkuV8yXDR4Vjw3qeTPDFFa2Vaqi2zLALzf5Da2giL8mECz4Jkup';
		private $table = "tb_users_modul_relation";

		function __construct(){
			// return $this->getName();
		}

		public function firstAdd($id){
			$this->load->model('Modul_model','modul');
			$modul = $this->modul->getAll();
			$x = count($modul);
			$bulkData = array();
			if ($id) {
				$query = $this->db->get_where($this->table, array('user_id' => $id));
				if ($query->row()) {
					$pesan="sukses";
					return $pesan;
				}
				else{
					for ($i=0; $i < $x; $i++) { 
						$data = array(
							'user_id' => $id,
							'modul_id' => $modul[$i]['modul_id'],
							'access' => 'READ',
							'created_by' => $this->session->name
						);
						$bulkData[] = $data;
					}
					$response = $this->db->insert_batch($this->table , $bulkData);
					if ($response) {
						$pesan = 'sukses';
						return $pesan;
					}
				}
			}
		}

		public function edit($data){
			log_message('DEBUG', 'Edit User Access executed by ' . $this->session->name);

			$status = 'gagal';

			$response = $this->db->update_batch($this->table, $data , 'relation_id'); 
			if ($response) {
				$status = 'sukses';
			}
			return $status;
		}

		public function delete($id){
			log_message('DEBUG', 'Delete Access ' . $id . ' executed by ' . $this->session->name);
			$status="Gagal";
			$pesan="Gangguan Sistem";
			$query = $this->db->delete($this->table, array('relation_id' => $id));			
			$affected = $this->db->affected_rows($query);
			if ($affected != 0) {
				$status="sukses";
				$pesan= $affected . " Data Terhapus";
			}
			return $status; 
		}

		public function getAccess($id){
			$result = array();
			$query = $this->db->query("SELECT r.relation_id , m.path , r.access , m.nama FROM tb_users_modul_relation r JOIN tb_modul m ON m.modul_id = r.modul_id WHERE r.user_id = '{$id}'");
			if ($data = $query->result_array()) {
				$result = $data;
			}
			return $result;
		}
		
		public function fdsAccess($id){
			$result = array();
			$wt = array();
			$query = $this->db->query("SELECT m.path , r.access  FROM tb_users_modul_relation r JOIN tb_modul m ON m.modul_id = r.modul_id WHERE r.user_id = '{$id}'");
			if ($data = $query->result_array()) {
				foreach ($data as $key => $value) {
					$result = array(
						'page' => $value['path'],
						'access' => $value['access']
					);
					if ($value['path'] == 'fds_config' || $value['path'] == 'fds_blacklist') {
						$wt[]=$result;
					}
				}
				$message = 'success';
			}
			else{
				$message = 'failed';
			}
			return json_encode(compact( 'message','wt'));
		}

	}
