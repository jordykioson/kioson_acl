<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_model extends CI_Model {

//Data from tb_reporting
	var $table = 'tb_reporting';
	var $column_order = ['retailer_id','kcp_name','campaign_name','tag','status_progress','phone_number','date_hitted',null]; //set column field database for datatable orderable
	var $column_search = ['retailer_id','kcp_name','campaign_name','tag','status_progress','phone_number','date_hitted']; //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = ['datetime_hitted' => 'desc']; // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		

		$campaign =  $this->input->post('campaign');
		$campaign2 =  $this->input->post('campaign2');
		$campaign3 =  $this->input->post('campaign3');


		if($this->input->post('campaign'))
		{
			$this->db->where("campaign_name IN ('$campaign','$campaign2','$campaign3')
			 ");
		}

		$tag =  $this->input->post('tag');
		$tag2 =  $this->input->post('tag2');
		$tag3 =  $this->input->post('tag3');


		if($this->input->post('tag'))
		{
			$this->db->where("tag IN ('$tag','$tag2','$tag3')
			 ");
		}

		$status =  $this->input->post('status');
		$status2 =  $this->input->post('status2');
		$status3 =  $this->input->post('status3');


		if($this->input->post('status'))
		{
			// $this->db->where("status_progress = '$status' 
			// OR status_progress = '$status2' OR status_progress = '$status3'
			//  ");
			 $this->db->where("status_progress IN ('$status','$status2','$status3') 
			 ");
		}


		// $this->db->where("DATE_FORMAT(date_hitted,'%Y-%m-%d')","YYYY-MM-DD");
		$this->db->from($this->table);
		//nylain ini for limit show
		// $this->db->where('status < ',4 );
		// $this->db->where('tag','Never Top up');


		$i = 0;
	
		foreach ($this->column_search as $item) 
		{
			if($_POST['search']['value']) 
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($_POST['order'])) 
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($start)
	{
		$this->_get_custom_field($start);
		$this->_get_datatables_query();
		if($_POST['length'] != -1)	
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->like('type_name', 'Call', 'after');
		// $this->db->where("DATE(date_hitted,'%Y-%m-%d')","YYYY-MM-DD");

		$query = $this->db->get();
		return $query->result();
		// $hege = ["Cecilie", "Lone"];
		// print_r($hege);
	}

	function count_filtered($start)
	{
		$this->_get_custom_field($start);		
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($r_id)
	{
		// print_r($r_id,'a');
		// echo $r_id.'aaaaa';
		//  die();
		$query=$this->db->query("SELECT n.* 
		FROM tb_reporting n 
		INNER JOIN (
		  SELECT campaign_name, MAX(datetime_hitted) AS datetime_hitted
		  FROM tb_reporting  where retailer_id = '" .$r_id."' GROUP BY campaign_name
		) AS max USING (campaign_name, datetime_hitted) where retailer_id = '" .$r_id."'");
			// $result = $query->result_array();
		return $query->result_array();
		// print_r($result);
	}

	public function get_historycase_by_id($r_id)
	{
		$query=$this->db->query("SELECT * ,date(date_hitted) as date_hitted_date
		FROM tb_reporting where retailer_id = '" .$r_id."' AND status_call != 0 order by datetime_hitted desc");
			// $result = $query->result_array();
		return $query->result_array();
		// print_r($result);
	}
		
	

	public function save($data)
	{

		$this->db->insert_batch($this->table, $data);
		return $this->db->affected_rows();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function _get_custom_field($start)
    {
	

		if(isset($_POST['columns'][6]['search']['value']) and $_POST['columns'][6]['search']['value'] !='')
		{
			 $this->db->where('date_hitted < ',$_POST['columns'][6]['search']['value']);
            //  $this->db->where('date_hitted < ',$start);
		}

    }
	
	public function update_feedback($retailer_id, $feedback) {
		$this->db->set('feedback', $feedback);
		$this->db->set('status', 'status+1', FALSE);
		// $this->db->set('status_call', 'status_call+1', FALSE);
		
		$this->db->set('status_progress', 'CalOn Progress', FALSE);

		$this->db->set('date_hitted','NOW()',false);
		$this->db->where('retailer_id', $retailer_id);
		$this->db->update($this->table);
		return $this->db->affected_rows();
	}

	public function get_list_campaigns()
	{
		$this->db->select('campaign_name');
		$this->db->from($this->table);
		$this->db->order_by('campaign_name','asc');
		$query = $this->db->get();
		$result = $query->result();

		$campaigns = array();
		foreach ($result as $row) 
		{
			$campaigns[] = $row->campaign_name;
		}
		return $campaigns;
		// print_r($campaigns);
	}

	public function get_list_tags()
	{
		$this->db->select('tag');
		$this->db->from($this->table);
		$this->db->order_by('tag','asc');
		// $this->db->where('campaign_name', 'never top up');
		// $this->db->group_by('tag');

		$query = $this->db->get();
		$result = $query->result();

		$tags = array();
		foreach ($result as $row) 
		{
			$tags[] = $row->tag;
		}
		return $tags;
		// print_r($campaigns);
	}

	public function get_list_status()
	{

		$this->db->select('status_progress');
		$this->db->from($this->table);
		$this->db->order_by('status_progress','asc');
		$query = $this->db->get();
		$result = $query->result();

		$statuses = array();
		foreach ($result as $row) 
		{
			$statuses[] = $row->status_progress;
		}
		return $statuses;
		// print_r($campaigns);
	}
	
}
