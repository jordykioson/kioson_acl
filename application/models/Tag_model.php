<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Tag_model extends CI_Model {

		private $table = 'tb_tags';


		function __construct(){
			// return $this->getName();
		}

		public function add(){
			log_message('DEBUG', 'Add New Tag/Label executed by ' . $this->session->name);
			$tag_name = $this->input->post('tag_name');
			$tag_query = $this->input->post('tag_query');
			$tag_description = $this->input->post('tag_desciption'); 
			$tag_requested_by = $this->input->post('tag_request_by');

			$status='Gagal';
			$pesan='Mohon periksa Nama Label';
			if ($tag_name) {
				$query = $this->db->get_where($this->table, array('tag_name' => $tag_name));
				if ($query->row()) {
					$pesan="Nama Label sudah ada, gunakan nama label yang lain";
				} else {
					$data = [
						// $id = "";
						"tag_name" => $tag_name,
						"query" => $tag_query,
						"description" => $tag_description,
						"created_by" => $this->session->ID,
						"requested_by" => $tag_requested_by, 
						"created_date" => sekarang()
					];
					$this->db->insert($this->table , $data);
					$query = $this->db->get_where($this->table, array('tag_name' => $tag_name));
					if ($query->row()) {
						$status = 'sukses';
					} else {
						$pesan = 'Terdapat Masalah Sistem';
					}
				}
			}
			return json_encode(compact('status','pesan'));
		}

		public function getAll(){
			log_message('DEBUG', 'Get All Tag executed by ' . $this->session->name);
			$query = $this->db->get($this->table);

			if ($data=$query->result()) {
				for ($i=0; $i < count($data) ; $i++) { 
					$creater = $data[$i]->created_by;
					$requester = $data[$i]->requested_by;
					$data[$i]->created_by = $this->getUserName($creater);
					$data[$i]->requested_by = $this->getUserName($requester);

				}
			}
			return json_encode(compact('data'));
		}

		public function delete($id){
			log_message('DEBUG', 'Delete Tag ' . $id . ' executed by ' . $this->session->name);
			$status="Gagal";
			$pesan="Gangguan Sistem";
			$query = $this->db->delete($this->table, array('tag_id' => $id));			
			$affected = $this->db->affected_rows($query);
			if ($affected != 0) {
				$status="Sukses";
				$pesan= $affected . " Data Terhapus";
			}
			return json_encode(compact('status', 'pesan')); 
		}

		public function edit($id){
			log_message('DEBUG', 'Edit Tag executed by ' . $this->session->name);

			$status = 'gagal';
			$tag_name = $this->input->post('tag_name');
			$tag_query = $this->input->post('tag_query');
			$tag_description = $this->input->post('tag_desciption');

			$data = array(
               'tag_name' => $tag_name,
               'tag_query' => $tag_query,
               'tag_description' => $tag_description
			);

			$this->db->where('tag_id', $id);
			$response = $this->db->update($this->table, $data); 
			if ($response) {
				$status = 'sukses';
			}
			return json_encode(compact('status'));
		}

		private function getUserName($id){
			$this->load->model('User_model','user');
			$response = $this->user->getById($id);
			$decode = json_decode($response);
			$res = $decode->data[0]->name;
			return $res;
		}

		public function getById($id){
			log_message('DEBUG', 'get tag by id executed by ' . $this->session->name);
			$this->db->select('tag_name');
			$query = $this->db->get_where($this->table, array('tag_id' => $id));
			$data = $query->result();
			if ($data) {
				$pesan = "sukses";
			}
			else {
				$pesan="gagal";
			}
			return json_encode(compact('data' , 'pesan'));
		}

		public function getQuery($id){
			log_message('DEBUG', 'get query by tag_id executed by ' . $this->session->name);
			$this->db->select('query');
			$query = $this->db->get_where($this->table, array('tag_id' => $id));
			$data = $query->result();
			if ($data) {
				$pesan = "sukses";
			}
			else {
				$pesan="gagal";
			}
			return json_encode(compact('data' , 'pesan'));
		}

	}
