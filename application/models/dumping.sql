******TABLE USERS_MODUL_RELATION********
CREATE TABLE `internal_acl`.`tb_users_modul_relation` (
  `relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `modul_id` int(11) NOT NULL,
  `access` VARCHAR(10) NOT NULL,
  `created_by` VARCHAR(255) DEFAULT NULL,
  `created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`relation_id`),
  FOREIGN KEY (`user_id`) REFERENCES tb_users(`user_id`),
  FOREIGN KEY (`modul_id`) REFERENCES tb_modul(`modul_id`));

*****TABLE CAMPAIGN_DETAILS**************
CREATE TABLE `internal_acl`.`tb_campaign_details` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `period_cron` varchar(45) DEFAULT NULL,
  `start_cron` date DEFAULT NULL,
  `repeater` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `requested_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `action_time` time DEFAULT NULL,
  PRIMARY KEY (`campaign_id`),
  FOREIGN KEY (`campaign_name_id`) REFERENCES tb_campaign_name(`id`),
  FOREIGN KEY (`tag_id`) REFERENCES tb_tags(`tag_id`),
  FOREIGN KEY (`created_by`) REFERENCES tb_users(`user_id`),
  FOREIGN KEY (`requested_by`) REFERENCES tb_users(`user_id`));

********** TABLE_SEQUENCE**************
CREATE TABLE `internal_acl`.`tb_sequences` (
  `sequences_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name_id` int(11) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `delay_time` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`sequences_id`),
  FOREIGN KEY (`campaign_name_id`) REFERENCES tb_campaign_name(`id`),
  FOREIGN KEY (`action_id`) REFERENCES tb_actions(`action_id`));

**********************MODUL********************************
CREATE TABLE `internal_acl`.`tb_modul` (
  `modul_id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`modul_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1

******************USERS************************************
CREATE TABLE `internal_acl`.`tb_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `created_time` datetime NOT NULL,
  `last_login_time` datetime NOT NULL,
  `last_login_ip` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
FOREIGN KEY (`level`) REFERENCES tb_level(`level`));

********************CAMPAIGN_NAME************************************
CREATE TABLE `internal_acl`.`tb_campaign_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

*************************LEVEL**********************************
CREATE TABLE `internal_acl`.`tb_level` (
  `level` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

*************************ACTIONS**************************************
CREATE TABLE `internal_acl`.`tb_actions` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1

************************TAGS**************************************
CREATE TABLE `internal_acl`.`tb_tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) DEFAULT NULL,
  `query` longtext,
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `requested_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
)

*********************BATCH**************************************

CREATE TABLE `internal_acl`.`tb_batch` (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `kcp_id` varchar(45) DEFAULT NULL,
  `kcp_name` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `address` longtext,
  `tag_id` int(11) DEFAULT NULL,
  `last_action_sequence` int(11) DEFAULT NULL,
  `campaign_name_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT "0",
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`batch_id`),
  FOREIGN KEY (`tag_id`) REFERENCES tb_tags(`tag_id`),
  FOREIGN KEY (`campaign_name_id`) REFERENCES tb_campaign_name(`id`));

********************* BATCH HISTORY **************************************

CREATE TABLE `tb_batch` (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `kcp_id` varchar(45) DEFAULT NULL,
  `kcp_name` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `address` longtext,
  `tag_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `campaign_name_id` int(11) DEFAULT NULL,
  `last_action_sequence` varchar(45) DEFAULT '0',
  PRIMARY KEY (`batch_id`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `tb_batch_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tb_tags` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

  ***************** CS CALL *******************************************

  CREATE TABLE `tb_cs_call` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` varchar(45) NOT NULL,
  `kcp_name` varchar(255) DEFAULT NULL,
  `campaign_name` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_hitted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone_number` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `feedback` varchar(45) DEFAULT NULL,
  `feedback_detail` varchar(255) DEFAULT NULL,
  `called_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=latin1
  
************** REPORTING ******************************************

CREATE TABLE `tb_reporting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `retailer_id` varchar(45) NOT NULL,
  `kcp_name` varchar(255) NOT NULL,
  `campaign_name` text DEFAULT NULL,
  `tag` varchar(255) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `address` longtext,
  `date_hitted` DATETIME NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1