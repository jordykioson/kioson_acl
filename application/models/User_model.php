<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_model extends CI_Model {

	private $table = "tb_users";
	private $table_relation = "tb_users_modul_relation";

	var $hash='cWd2MjxjLQ8PVvkuV8yXDR4Vjw3qeTPDFFa2Vaqi2zLALzf5Da2giL8mECz4Jkup';

	public function create(){
		// $this->load->helper('main_helper');
		$username = $this->input->post('username');
		$fullname = $this->input->post('fullname');
		$level = $this->input->post('level');
		$iplogin= getMyipaddress();
		$lastlogin= sekarang();
		$password=hash('sha256',$this->hash.$this->input->post('password'));
		$sql="INSERT INTO tb_users SET username='{$username}',password='{$password}',created_time=NOW(),name='{$fullname}',level={$level},last_login_ip='{$iplogin}'";
		if($this->db->query($sql)){
			return "sukses";
		}
	}

	public function getDetail(){
		$sql="SELECT u.user_id , u.name , u.last_login_ip, u.created_time , l.keterangan FROM tb_users u 
				JOIN tb_level l ON l.level = u.level
				GROUP BY u.name ASC";
		$query = $this->db->query($sql);
		$data = $query->result();
		if($data){
			$pesan = "sukses";
		}
		return json_encode(compact('data' ,'pesan'));
	}

	public function getAll(){
		log_message('DEBUG', 'get all user executed by ' . $this->session->name);
		$this->db->select('user_id, username, name, level');
		$query = $this->db->get('tb_users');
		$data = $query->result();
		if ($data) {
			$pesan = "sukses";
			log_message('DEBUG', 'get all user succeed');
		}
		return json_encode(compact('data' ,'pesan'));	
	}

	public function getById($id){
		log_message('DEBUG', 'get user by id executed by ' . $this->session->name);
		$this->db->select('user_id, username, name, level');
		$query = $this->db->get_where($this->table, array('user_id' => $id));
		$data = $query->result();
		if ($data) {
			$pesan = "sukses";
		}
		else {
			$pesan="gagal";
		}
		return json_encode(compact('data' , 'pesan'));
	}

	public function add(){
		$status='Gagal';
		$pesan='Mohon periksa Nama Pengguna dan Kata Kuncinya';
		if($this->input->post('username') && $this->input->post('password') && $this->input->post('nama')  && $this->input->post('level')){
			$username=$this->input->post('username');
			$sql="SELECT * FROM tb_users WHERE username=".$this->db->escape($username);
			$que=$this->db->query($sql);
			if($res=$que->row()){
				$pesan='Nama pengguna sudah ada, gunakan nama pengguna yang lain';
			} else if(!$this->cekusername($username)){
				$pesan='Mohon gunakan hanya huruf dan atau angka minimal 5 dan maksimal 8 karakter';
			} else {
				$password=hash('sha256',$this->login->hash.$this->input->post('password'));
				$level=$this->input->post('level');
				$nama=$this->input->post('name');
				$sql="INSERT INTO tb_users (username,password,name,level) VALUES (".$this->db->escape($username).",'{$password}',".$this->db->escape($name).",".$this->db->escape($level).")";
				$this->db->query($sql);
				$sql="SELECT * FROM tb_users WHERE username=".$this->db->escape($username);
				$que=$this->db->query($sql);
				if($res=$que->row()){
					$status='Sukses';
				} else {
					$pesan='Terdapat masalah sistem';
				}
			}
		}
		echo json_encode(compact('status','pesan'));
	}

	public function delete($id){
		
		log_message('DEBUG', 'Delete User ' . $id . ' executed by ' . $this->session->name);
		$status="Gagal";
		$pesan="Gangguan Sistem";
		$query = $this->db->delete($this->table, array('user_id' => $id));			
		$affected = $this->db->affected_rows($query);
		if ($affected != 0) {
			$query_2 = $this->db->delete($this->table_relation, array('user_id' => $id));			
			$affected_2 = $this->db->affected_rows($query_2);
			if ($affected_2 != 0) {
				$status="sukses";
				$pesan= $affected . " Data Terhapus";
			}
		}
		return $status; 
		
	}

	private function cekusername($username){
		if(preg_match('/^[a-z0-9]{5,8}$/s',$username)){
			return true;
		} else {
			return false;
		}
	}

}