<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Campaign_model extends CI_Model {
		function __construct(){
			parent::__construct();
		}

		private $table = "tb_campaign_details";
		private $table_name = "tb_campaign_name";
		// function index(){
		// 	if($this->input->post('campaign_name') || $this->input->post('name')){
		// 		$this->addedit();
		// 	} else {
		// 		$sql="SELECT t1.*,t2.username FROM tb_campaign t1 JOIN tb_users t2 ON t2.ID=t1.created_by ORDER BY t1.created_time DESC";
		// 		$q_campaign=$this->db->query($sql);
		// 		return compact('q_campaign');
		// 	}
		// }

		public function getAll_cron($id="") {
			log_message('DEBUG', 'Get All Campaign Where Status Active executed by Cron');
			$data = array();
			if($id!=""){
				$sql="SELECT * FROM {$this->table} a INNER JOIN {$this->table_name} b ON a.campaign_name_id = b.id WHERE status = '1' GROUP BY campaign_name_id";
			}else {
				$sql="SELECT * FROM {$this->table} WHERE status = '1'";
			}
			$que=$this->db->query($sql);
			
			if ($data=$que->result()) {
				for ($i=0; $i < count($data) ; $i++) { 
					$creater = $data[$i]->created_by;
					$requester = $data[$i]->requested_by;
					$campaign = $data[$i]->campaign_name_id;
					$data[$i]->created_by = $this->getUserName($creater);
					$data[$i]->requested_by = $this->getUserName($requester);
					$data[$i]->campaign_name = $this->getCampaignName($campaign);

				}
			}
				return json_encode(compact('data'));
		}

		// Get All Campaign
		public function getAll(){

			log_message('DEBUG', 'Get All Campaign executed by ' . $this->session->name);
			$data = array();
			$sql="SELECT * FROM {$this->table} GROUP BY campaign_name_id";
			$que=$this->db->query($sql);
			
			if ($data=$que->result()) {
				for ($i=0; $i < count($data) ; $i++) { 
					$creater = $data[$i]->created_by;
					$requester = $data[$i]->requested_by;
					$campaign = $data[$i]->campaign_name_id;
					$data[$i]->created_by = $this->getUserName($creater);
					$data[$i]->requested_by = $this->getUserName($requester);
					$data[$i]->campaign_name = $this->getCampaignName($campaign);
				}
			}
				return json_encode(compact('data'));
		}

		public function getCampaignDetails($id){

			log_message('DEBUG', 'Get Campaign Details executed by ' . $this->session->name);
			$data = array();
			$sql="SELECT * FROM {$this->table} WHERE campaign_name_id = '{$id}'";
			$que=$this->db->query($sql);
			
			if ($data=$que->result()) {
				for ($i=0; $i < count($data) ; $i++) { 
					$creater = $data[$i]->created_by;
					$requester = $data[$i]->requested_by;
					$campaign = $data[$i]->campaign_name_id;
					$tag = $data[$i]->tag_id;
					$data[$i]->created_by = $this->getUserName($creater);
					$data[$i]->requested_by = $this->getUserName($requester);
					$data[$i]->campaign_name = $this->getCampaignName($campaign);
					$data[$i]->tag_name = $this->getTagName($tag);

				}
			}
				return json_encode(compact('data'));
		}

		public function getById($id){

			log_message('DEBUG', 'Get Campaign Details executed by ' . $this->session->name);
			$data = array();
			$sql="SELECT * FROM {$this->table} WHERE campaign_id = '{$id}'";
			$que=$this->db->query($sql);
			
			if ($data=$que->result()) {
				$pesan="sukses";
			}
				return json_encode(compact('data' , 'pesan'));
		}

		private function getUserName($id){
			$this->load->model('User_model','user');
			$response = $this->user->getById($id);
			$decode = json_decode($response);
			$res = $decode->data[0]->name;
			return $res;
		}

		private function getCampaignName($id){
			$this->db->select('campaign_name');
			$query = $this->db->get_where($this->table_name, array('id' => $id));
			$response = $query->result();
			$res = $response[0]->campaign_name;
			return $res;
		}

		private function getTagName($id){
			$this->load->model('Tag_model','tag');
			$response = $this->tag->getById($id);
			$decode = json_decode($response);
			$res = $decode->data[0]->tag_name;
			return $res;
		}


		public function update(){
			log_message('DEBUG', 'Update Campaign Name executed by ' . $this->session->name);
			$sql='';
			$status='Gagal';
			$pesan='Mohon periksa nama campaign';
			$respon='';
			// $nm_field=$this->input->post('name');
			$value=$this->input->post('campaign_name');
			$ID= $this->input->post('campaign_name_id');

			if ($ID) {
				$this->db->where('id' , $ID);
				$query = $this->db->get($this->table_name);
				$que = $query->row();
				if ($que) {
					$data = [
						'campaign_name' => $value
					];
					$this->db->where('campaign_name' , $que->campaign_name);
					$this->db->update($this->table_name, $data);
					$pesan = "Data Berhasil di Perbaharui"; 
					$status="Sukses";
				}
			}
			return json_encode(compact('status','pesan'));
		}

		public function add($data){
			log_message('DEBUG', 'Add New Campaign executed by ' . $this->session->name);

			$status='Gagal';
			$pesan='Mohon periksa Nama Campaign';
			$campaign_name = $this->input->post('campaign_name');

			if ($campaign_name) {
				$sql="SELECT * FROM {$this->table_name} WHERE campaign_name=".$this->db->escape($campaign_name);
				$query=$this->db->query($sql);
				if($res=$query->row()){
					$pesan='Nama campaign sudah ada, gunakan nama campaign yang lain';
				} else {
					$value = array (
						'campaign_name' => $campaign_name
					);

					$this->db->insert($this->table_name, $value);
					$query = $this->db->get_where($this->table_name, array('campaign_name' => $campaign_name));
					$result = $query->row();
					$campaign_name_id = $result->id;

					if ($campaign_name_id) {
						for ($i=0; $i < count($data) ; $i++) { 
							$data[$i]['campaign_name_id'] = $campaign_name_id;
						}

						$this->db->insert_batch($this->table , $data);		

						$sql="SELECT * FROM {$this->table} WHERE campaign_name_id=".$this->db->escape($campaign_name_id);
						$que=$this->db->query($sql);

						if($response=$que->row()){
							$status='Sukses';
						} else {
							$pesan='Terdapat masalah saat insert campaign_details';
						}
					}
					else {
						$pesan = "Terdapat masalah saat insert campain_name";
					}					
				}
			}
			
			return json_encode(compact('status','pesan','campaign_name_id'));
		}

		public function updateStatus($id , $status){
			log_message('DEBUG', 'Edit Campaign Status executed by ' . $this->session->name);
			$pesan="gagal";
			$data = array(
			   'status' => $status,
			   'updated_date' => sekarang()
			);		
			$this->db->where('campaign_name_id', $id);
			$response = $this->db->update('tb_campaign_details', $data); 
			if ($response) {
				$pesan = 'sukses';
			}
			return json_encode(compact('pesan'));
		}

		public function updateDate($interval , $id){
			$sql="UPDATE {$this->table} set start_cron = start_cron + interval {$interval} day WHERE campaign_name_id = ".$this->db->escape($id);
			$query=$this->db->query($sql);
		}

	}
