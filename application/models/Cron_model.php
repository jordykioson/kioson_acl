<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Cron_model extends CI_Model {
		var $hash='cWd2MjxjLQ8PVvkuV8yXDR4Vjw3qeTPDFFa2Vaqi2zLALzf5Da2giL8mECz4Jkup';
		private $table_campaign = "tb_campaign_name";
		private $table_batch = "tb_batch";


		function __construct(){
			// return $this->getName();
		}

		/* ============================================== START CRON TAG ====================================================== */
			private function getQuery($id){
				$this->load->model('Tag_model','tag');
				$response = $this->tag->getQuery($id);
				$decode = json_decode($response);
				$res = $decode->data[0]->query;
				return $res;
			}

			private function getDataTagged($query , $tag_id, $campaign_name_id) /*must have parameter query*/
			{
				$response = array();
				$otherdb = $this->load->database('ipay', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
				$response = $otherdb->query($query);
				$res = $response->result_array();
				$count = count($res);
				for ($i=0; $i < $count; $i++) { 
					$res[$i]['tag_id'] = $tag_id;
					$res[$i]['campaign_name_id'] = $campaign_name_id;
					$res[$i]['created_date'] = sekarang();
				}
				return $res;
			}

			private function getBatchData($tag_id , $campaign_name_id){
				$param = array (
					'tag_id' => $tag_id,
					'campaign_name_id' => $campaign_name_id,
					'status' => '0'
				);
				$que = $this->db->get($this->table_batch);
				if($que->num_rows != 0){
					$result = array(array('kcp_id' => "0000" , 'mobile_no' => "0000", 'campaign_name_id' => "0"));
				}
				else{
					$result = $que->result_array();
				}
				return $result;
			}

			private function updateStatusBatch($id) {
				$data = array (
					"status" => '1',
					"updated_date" => sekarang()
				);
				$this->db->where('batch_id' , $id);
				$res = $this->db->update($this->table_batch, $data);
				return $res;
			}

			public function insertBatch(){
				log_message('DEBUG', json_encode(array('Inserting Batch execute by Cron')));
				$this->load->model('Campaign_model','campaign');
				$response = $this->campaign->getAll_cron();
				$decode = json_decode($response);
				$for = count($decode->data);
				if($for!=0) {
					for ($i=0; $i < $for ; $i++) { 
						$tag_id = $decode->data[$i]->tag_id;
						$campaign_name_id = $decode->data[$i]->campaign_name_id;
						$campaign_name = $decode->data[$i]->campaign_name;
						$query = $this->getQuery($tag_id);
						$data = $this->getDataTagged($query , $tag_id, $campaign_name_id);
						$param = array (
							'tag_id' => $tag_id,
							'campaign_name_id' => $campaign_name_id,
							'status' => '0'
						);
						$que = $this->db->get_where('tb_batch',$param);
						$result = $que->result_array();
						$dataDuplicate = $data;
						$limiterFromTag = count($data);
						$limiterFromTable = count($result);
						/* START CHECKING IF DATA FROM TAG QUERY ALREADY EXIST IN BATCH TABLE */
						/* IF DOESN'T EXIST, INSERT TO BATCH */
						/* IF EXIST, DATA FROM TAG QUERY UNSET(ARRAY) */
						for($k=0; $k<$limiterFromTag; $k++) {
							for($j=0; $j<$limiterFromTable; $j++) {
								if ($data[$k]['kcp_id'] == $result[$j]['kcp_id'] && $data[$k]['tag_id'] == $result[$j]['tag_id'] && $data[$k]['campaign_name_id'] == $result[$j]['campaign_name_id'] && $result[$j]['status'] == '0') {
									unset($data[$k]);
									break;
								}
							}
						}
						// INSERT TO BATCH
						if(count($data)!=0){
							$res = $this->db->insert_batch($this->table_batch , $data);
							if ($res != 0) {
								$pesan="{$campaign_name} Sukses Insert {$res} Data";
								$messages[] = $pesan;
							}
						}else {
							$pesan = "{$campaign_name} Have no data to be inserted";
							$messages[] = $pesan;
						}

						/* START CHECKING IF DATA IN TABLE, EXIST IN QUERY TAG */
						/* IF NOT EXIST, STATUS CORRESPONDED DATA CHANGE TO 1 */
						$newLimiterFromTag = count($dataDuplicate);
						$newLimiterFromTable = count($result);
						//dataDuplicate = dari query
						//result = dari table
						for($m=0; $m<$newLimiterFromTable; $m++) {
							for($u=0; $u<$newLimiterFromTag; $u++) {
								if ($dataDuplicate[$u]['kcp_id'] == $result[$m]['kcp_id'] && $dataDuplicate[$u]['tag_id'] == $result[$m]['tag_id'] && $dataDuplicate[$u]['campaign_name_id'] == $result[$m]['campaign_name_id'] && $result[$m]['status'] == '0') {
									unset($result[$m]);
									break;
								}
							}
						}


						if(count($result)==0) {
							$pesan = "{$campaign_name} Have no data to be updated";
							$messages[] = $pesan;
						}else {
							$counter = 0;
							foreach($result as $key) {
								$id = $key['batch_id'];
								$response_update = $this->updateStatusBatch($id);
								$counter += $response_update;
								// $this->load->model('History_model','history');
								// $this->history->addById($id);
								$this->load->model('Report_model','report');
								$ress = $this->report->addById($id);
								// print_r($ress); die();
							}							
							$pesan="{$campaign_name} Sukses update {$counter} Data";
							$messages[] = $pesan;
						}
						

					}
				}
				$pesan = json_encode(compact('messages'));
				echo $pesan;
				log_message('DEBUG', $pesan);
			}

			/* ============================================== END CRON TAG ====================================================== */

			


			/* ============================================== START CRON ACTION ====================================================== */

			public function action(){
				$this->load->model('Campaign_model','campaign');
				$response = $this->campaign->getAll_cron();
				$decode = json_decode($response);
				// $messages = "Campaign Tidak Aktif";
				$for = count($decode->data);
				if ($for!=0) {
					for ($i=0; $i < $for ; $i++) { 
						$tag_id = $decode->data[$i]->tag_id;
						$campaign_name_id = $decode->data[$i]->campaign_name_id;
						$campaign_name = $decode->data[$i]->campaign_name;
						$result = $this->getBatchData($tag_id , $campaign_name_id);
						$count = count($result);
						for ($i=0; $i < $count; $i++) { 
							echo $result[$i]['campaign_name_id'];
						}
					}
				}
			}

			/* ============================================== END CRON ACTION ====================================================== */


			public function getCampaignTagId($id){
				$this->load->model('Campaign_model','campaign');
				$response = $this->campaign->getById($id);
				$decode = json_decode($response);
				$res = $decode->data[0]->tag_id;
				return $res;
			}

			public function getSequences($id){
				$this->load->model('Sequences_model','sequences');
				$response = $this->sequences->getDetails($id);
				// $decode = json_decode($response);
				// $res = $decode->data[0]->tag_id;
				return $response;
			}

			public function getCampaignDetails($id){
				$this->load->model('Campaign_model','campaign');
				$response = $this->campaign->getCampaignDetails($id);
				// $decode = json_decode($response);
				// $res = $decode->data[0]->tag_id;
				return $response;
			}

			public function campaign(){
				$query = $this->db->get($this->table_campaign);
				$res = $query->result();
				$count = count($res);
				for ($i=0; $i < $count; $i++) { 
					$campaign_name_id = $res[$i]->id;
					echo $this->getCampaignDetails($campaign_name_id) ."<br>";
					echo $this->getSequences($campaign_name_id) ."<br>";
				}
			}
	}
