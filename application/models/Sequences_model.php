<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Sequences_model extends CI_Model {

		private $table="tb_sequences";

		public function add($data){
			$res = $this->db->insert_batch('tb_sequences' , $data);	
			log_message('DEBUG', 'Add New Sequences executed by ' . $this->session->name);
			
			if ($res == true) {
				$status = "Sukses";
				$pesan = "Data Berhasil masuk";
			}
			else {
				$status = "Gagal";
				$pesan = "Terdapat Masalah Sistem";
			}
			return json_encode(compact('status' , 'pesan'));
		}

		public function getDetails($id, $sequence=''){
			log_message('DEBUG', 'Get Sequences Details executed by ' . $this->session->name);
			$data = array();
			if($sequence!='') {
				$sql="SELECT a.*, b.action_name FROM {$this->table} a inner join tb_actions b on a.action_id = b.action_id WHERE campaign_name_id = '{$id}' AND sequence = '{$sequence}'";				
			}else {
				$sql="SELECT * FROM {$this->table} WHERE campaign_name_id = '{$id}'";
			}
			$que=$this->db->query($sql);
			
			if ($data=$que->result()) {
				for ($i=0; $i < count($data) ; $i++) { 
					$action = $data[$i]->action_id;
					$data[$i]->action_name = $this->getActionName($action);
				}
			}
				return json_encode(compact('data'));
		}

		private function getActionName($id){
			$this->load->model('Action_model','action');
			$response = $this->action->getById($id);
			$decode = json_decode($response);
			$res = $decode->data[0]->action_name;
			return $res;
		}	

	}