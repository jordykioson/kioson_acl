<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/***************************************** Header *****************************************/
$this->load->view('header_page');

/***************************************** Sidebar *****************************************/
$this->load->view('sidebar_page');
?>

<!--______________________________________ Content _______________________________________-->
<style>
    .content-container {
        height: 685px;
    }
</style>

<div class="content-container">

    <!-- Content page base on response from controller -->
    <?php if($page_selected == 'campaign') {
        $this->load->view('campaign_page');
    }else if($page_selected == 'tags') {
        $this->load->view('tags_page');
    }else if($page_selected == 'watchTower') {
        $this->load->view('watchTower_page');
    }else if($page_selected=='report') {
        $this->load->view('report_page');
    }else if($page_selected=='cs') {
        $this->load->view('cs_page');
    }else if($page_selected=='user') {
        $this->load->view('user_page');
    }?>
    <!-- Content page base on response from controller -->

</div>

<!--______________________________________ Footer _______________________________________-->
<?php 
$this->load->view('footer_page');
?>