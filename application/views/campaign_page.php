<?php 
    $status = 0;
    $toggleOn = base_url()."assets/toggle_button_on.png";
    $toggleOff = base_url()."assets/toggle_button_off.png";

    /* Getting access session of the user */
    $writeAccess = false;
    $readAccess = false;
    foreach($this->session->access as $key) {
        if($key['path'] == 'crm_campaign' && $key['access'] == 'FULL') {
            $writeAccess = true;
        }else if($key['path'] == 'crm_campaign' && $key['access'] == 'READ') {
            $readAccess = true;
        }
    }
?>
    
        <div class="add-campaign" align="right">

            <!-- Move to add campaign page if user has access -->
            <?php if($writeAccess){ ?>
            <label class="add-campaign-btn" onclick="openModal('<?php if($writeAccess){echo 'FULL';}else if($readAccess){echo 'READ';}?>')">+ Tambah Campaign</label>
            <?php } ?>
            <!-- Move to add campaign page if user has access -->

        </div>
        <div style="">
            <table style="width:100%">
                <thead style="background-color:#f2f2f2;">
                    <tr style="height:32px;">
                        <th style="width: 35.8%;padding-left:25px;padding-top:8px;" class="header-table">Campaign Name
                            <!-- drop down icon for sorting function (for now hidden because has no function) -->
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden"> 
                        </th>
                        <th style="width: 14.7%;padding-left:12px;padding-top:8px;" class="header-table">Created Date
                            <!-- drop down icon for sorting function (for now hidden because has no function) -->
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 17%;padding-left:11px;padding-top:8px;" class="header-table">Created By
                            <!-- drop down icon for sorting function (for now hidden because has no function) -->
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 17.12%;padding-left:12px;padding-top:8px;" class="header-table">Requested By
                            <!-- drop down icon for sorting function (for now hidden because has no function) -->
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <?php if($writeAccess){ ?>
                        <th style="width: 15.3%;padding-left:11px;padding-right:11px;padding-top:8px;" class="header-table">Status</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody style="padding-top:12px;padding-bottom:10px">

                <!-- if campaign data value is not 0, display the campaign data -->
                <?php if(isset($response)) { ?>
                    <?php foreach($response as $res) { ?>
                    <tr style="font-size: 12px;font-family:'Roboto'">
                        <td style="padding-left:25px;padding-top:12px;padding-bottom:10px">
                            <a href="javascript:viewDetail('<?php echo $res->campaign_name_id;?>')" style="color: #1594b9;"><?php echo $res->campaign_name; ?></a>
                        </td>
                        <td style="padding-left:12px;padding-top:12px;padding-bottom:10px"><?php echo $res->created_date; ?></td>
                        <td style="padding-left:11px;padding-top:12px;padding-bottom:10px"><?php echo $res->created_by; ?></td>
                        <td style="padding-left:12px;padding-top:12px;padding-bottom:10px"><?php echo $res->requested_by; ?></td>
                        <?php if($writeAccess){ ?>
                        <td style="padding-left:11px;padding-top:12px;padding-bottom:10px">
                            
                            <!-- toggle status icon, if status = 1, toggle-btn-active shown, if status = 0, toggle-btn-inactive shown -->
                            <img id="toggle-btn-active<?php echo $res->campaign_id; ?>" class="toggle-icon" 
                                onclick="togglePressInactive('<?php echo $res->campaign_name_id; ?>','<?php echo $res->campaign_id; ?>','<?php if($writeAccess){echo 'FULL';}else if($readAccess){echo 'READ';}?>')" 
                                src="<?php echo $toggleOn;?>" 
                                alt="toggle_button" style="display:none">

                            <img id="toggle-btn-inactive<?php echo $res->campaign_id; ?>" class="toggle-icon" 
                                onclick="togglePressActive('<?php echo $res->campaign_name_id; ?>','<?php echo $res->campaign_id; ?>','<?php if($writeAccess){echo 'FULL';}else if($readAccess){echo 'READ';}?>')" 
                                src="<?php echo $toggleOff; ?>" 
                                alt="toggle_button" style="display:none">
                            <?php if($res->status=='1') { ?>
                                <script>
                                    $('#toggle-btn-active<?php echo $res->campaign_id;?>').css('display','inline-block');
                                    $('#toggle-btn-inactive<?php echo $res->campaign_id;?>').css('display','none');
                                </script>
                            <?php }else { ?>
                                <script>
                                    $('#toggle-btn-active<?php echo $res->campaign_id;?>').css('display','none');
                                    $('#toggle-btn-inactive<?php echo $res->campaign_id;?>').css('display','inline-block');                                    
                                </script>
                            <?php }?>                   
                            <span id="toggle-td<?php echo $res->campaign_id;?>"><?php if($res->status=='1'){echo 'Active';}else{echo 'Inactive';} ?></span>
                            <!-- toggle status icon, if status = 1, toggle-btn-active shown, if status = 0, toggle-btn-inactive shown -->

                        </td>
                        <?php } ?>

                        <!-- data that needed but not displayed -->
                        <div style="display:none" id="id_<?php echo $res->campaign_id;?>"><?php echo $res->campaign_id; ?></div>
                        <div style="display:none" id="repeater_<?php echo $res->campaign_id;?>"><?php echo $res->repeater;?></divstyle>
                        <!-- data that needed but not displayed -->

                    </tr>
                    <?php } ?>
                    <?php }?>
                </tbody>
            </table>
        </div>
    
        <!-- Modal Box add campaign -->
        <div class="modal-campaign" id="modal-campaign" style="display:none">
            <div class="modal-campaign_content">
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">
                <div align="center" style="margin-bottom:20px;">
                    <label for="" style="color:#f7981d;font-size:14px;font-family:'Roboto';font-weight:500;margin-bottom:0px;line-height:16px;">Add Campaign</label>
                </div>
                <div id="error_message_campaign" style="font-size:12px;color:red;text-align:center;padding-bottom:10px;"></div>

                <form action="<?php echo base_url();?>Main/addCampaign/" method="post" id="form_add_campaign"> 

                    <!-- Campaign Name & Requested By -->
                    <div style="float:left;margin-right:20px;">
                        <label for="" class="campaign-style">Campaign Name</label>
                        <input type="text" name="campaign_name" id="campaign-val" placeholder="Write Campaign Name Here" class="input-campaign-style" style="width:450px;">
                    </div>
                    <div style="float:right">
                        <label for="" class="campaign-style">Requested By</label>
                        <select name="request_by" id="request-val" class="input-campaign-style" style="width:290px;">
                            <option value="" class="input-campaign-style">- Choose Requester Here -</option>
                            <?php foreach($response_user as $key_user) {?>
                                <option value="<?php echo $key_user->user_id; ?>"><?php echo $key_user->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both;"></div>

                    <!-- Start Time & Repeater & Action Time -->
                    <div style="float:left;margin-top:8px;">
                        <label for="" class="campaign-style">Start Time</label>
                        <input type="date" id="start_date-val" name="start_date" placeholder="Write Campaign Name Here" class="input-campaign-style" style="width:190px;margin-right:10px;" >
                    </div>
                    <div style="float:left;margin-top:8px;">
                        <label for="" class="campaign-style">Repeat</label>
                        <select name="repeat_num" id="repeat_num" class="input-campaign-style" style="width:190px;margin-right:10px;">
                            <option value="0">- Select Repeater Method -</option>
                            <option value="1">Daily</option>
                            <option value="7">Weekly</option>
                            <option value="30">Monthly</option>
                            <option value="365">Yearly</option>
                        </select>
                    </div>
                    <div style="float:left;margin-top:8px;">
                        <label for="" class="campaign-style">Action Time</label>
                        <input type="time" id="action_time" name="action_time" class="input-campaign-style" style="width:100px;" >
                    </div>

                    <!-- tag (limited into 3) -->
                    <div>
                        <label for="" class="campaign-style" style="width:290px;padding-top:8px">Tag</label>
                    </div>
                    <div style="display:inline-block" id="tag-container">

                        <!-- tag 1, if added new tag, javascript will copy this div and put new div below this div -->
                        <div class="tag_step1">
                            <select name="tag-1" id="tag-1" class="input-campaign-style tag-select" style="">
                                <option value="" class="input-campaign-style">- Select tag from database -</option>
                                <?php foreach($response_tag as $key) { ?>
                                    <option value="<?php echo $key->tag_id;?>" class="input-campaign-style"><?php echo $key->tag_name;?></option>
                                <?php } ?>
                            </select>
                            <span id="close-tag-icon1" onclick="closeTag()" style="display:none;cursor:pointer">
                                <img src="<?php echo base_url();?>assets/close_tag_icon.png" alt="close_tag" style="width:13px;vertical-align:baseline">
                            </span>
                        </div>  
                        <!-- tag 1, if added new tag, javascript will copy this div and put new div below this div -->
                        
                        <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="add-tag" onclick="addTag()" style="width:13px;position:absolute;margin-top:-23px;margin-left:295px;" class="add-icon">                        
                    </div>
                                      
                    <!-- Step (limited into 3) -->
                    <div class="campaign-style" style="margin-top:8px;">
                        Step
                    </div>
                        <div style="height:177px;">
                            <!-- box step 1, if added new step, javascript will copy this div and put new div below this div -->
                            <div class="box-step1" style="vertical-align:top">
                                <img src="<?php echo base_url();?>/assets/close_step_icon.png" alt="close_btn" class="close-modal" style="display:none" id="close-btn-icon1" onclick="closeStep()">
                                <label for="" id="label-1" class="campaign-style" style="color:#ffffff">Step 1</label>
                                <select name="action-step-1" id="action-step-1" class="input-campaign-style select-step" onchange="changeAction(this.value,1)" style="margin-top:5px">
                                    <option value="">- Choose Action -</option>
                                    <option value="push_notification">Push Notification</option>
                                    <option value="sms">SMS</option>
                                    <option value="call">Call</option>
                                </select>
                                <label for="" class="campaign-style" id="title-label1" style="color:#ffffff;display:none;">Title</label>
                                <textarea name="title-step-1" id="title-step-1" class="input-campaign-style textarea-step" rows="2" style="display:none"></textarea>
                                <label for="" class="campaign-style" id="content-label1" style="color:#ffffff;display:none;">Content</label>
                                <textarea name="content-step-1" id="content-step-1" class="input-campaign-style textarea-step" rows="4" maxlength="100" style="display:none"></textarea>
                                <label for="" class="campaign-style" style="color:#ffffff">Delay Time</label>
                                <div>
                                    <input min=0 type="number" id="day-delay-1" name="day-delay-1" class="input-campaign-style delay-day">
                                    <label for="" class="campaign-style delay-label" style="">Day</label>
                                </div>                    
                            </div> 
                            <!-- box step 1, if added new step, javascript will copy this div and put new div below this div -->
                             
                            <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" onclick="addStep()" style="" class="add-icon add_icon">                  
                        </div>

                    <!-- for clearing after using style float -->
                    <div style="clear:both;"></div>
                    <!-- for clearing after using style float -->
                    
                    <div style="width:100%" align="right" class="btn-submit_">
                        <input type="submit" value="Submit" class="btn-campaign-submit-modal" style="cursor:pointer" onclick="return validation()">
                    </div>
                </form>

            </div>
        </div>
        
        <!-- Modal Box View Details -->
        <div class="modal-campaign" id="modal-campaign_view" style="display:none">
            <div class="modal-campaign_content" style="width:654px;">
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">
                <div align="center" style="margin-bottom:20px;">
                    <label for="" style="color:#f7981d;font-size:14px;font-family:'Roboto';font-weight:500;margin-bottom:0px;line-height:16px;">Campaign Details</label>
                </div>
                <div style="font-size:10px;color:#59595c">
                    <div>
                        <label for="" style="width:120px;font-weight:500">Campaign Name</label>

                        <!-- Value will be assigned by javascript function viewDetail() -->
                        <label for="" id="view_campaign_name"></label>
                    </div>
                    <div>
                        <label for="" style="width:120px;font-weight:500">Related Tag</label>

                        <!-- Value will be assigned by javascript function viewDetail() -->                        
                        <ul id="view_ul_tag" style="display:inline-block;padding-left:15px;vertical-align:top">
                        </ul>
                    </div>
                    <div>
                        <label for="" style="width:120px;font-weight:500">Requested By</label>

                        <!-- Value will be assigned by javascript function viewDetail() -->                        
                        <label for="" id="view_requested_by"></label>
                    </div>
                    <div>
                        <label for="" style="width:120px;font-weight:500">Repeat</label>

                        <!-- Value will be assigned by javascript function viewDetail() -->
                        <label for="" id="view_repeater"></label>
                    </div>
                    <div>
                        <label for="" style="width:120px;font-weight:500">Action Time</label>

                        <!-- Value will be assigned by javascript function viewDetail() -->
                        <label for="" id="view_action_time"></label>
                    </div>
                    <div>
                        <label for="" style="vertical-align:top;width:120px;;font-weight:500">Step</label>

                        <!-- Value will be assigned by javascript function viewDetail() -->
                        <ul id="view_ul_step" style="display:inline-block;padding-left:15px;">
                        </ul>
                    </div>
                </div>
            </div>
        </div>

<script>

    /* error message appear when adding new campaign and the name already exist */
    <?php if(isset($error_name)){ ?>
        $('#modal-campaign').show();    
        $('#error_message_campaign').html('Campaign Name already exist');
    <?php } ?>

    /* update status campaign (toggle icon) into active */
    function togglePressActive(name_id, id, access) {
        /* securing access */
        if(access === 'FULL'){
            $.post("<?php echo base_url();?>Main/updateStatusCampaign/"+name_id+'/1', function(data) {
                $('#toggle-td'+id).html('Active');
                $('#toggle-btn-inactive'+id).css('display','none');
                $('#toggle-btn-active'+id).css('display','inline-block');
            });
        }else if(access === 'READ') {
            alert('You dont\'t have authorize to this page');
        }
    }

    /* update status campaign (toggle icon) into inactive */
    function togglePressInactive(name_id, id, access) {      
        /* securing access */
        if(access === 'FULL') {
            $.post("<?php echo base_url();?>Main/updateStatusCampaign/"+name_id+'/0', function(data) {
                $('#toggle-td'+id).html('Inactive');
                $('#toggle-btn-active'+id).css('display','none');
                $('#toggle-btn-inactive'+id).css('display','inline-block');
            });
        }else if(access === 'READ') {
            alert('You dont\'t have authorize to this page');
        }
    }

    /* viewing campaign detail when campaign name clicked */
    function viewDetail(id) {
        
        $.post("<?php echo base_url();?>Main/campaign", { dataDetail: id }, function(data) {
            $('#modal-campaign_view').fadeIn('slow');
            /* Getting response data from controller */
            let objek = JSON.parse(data);
            let dataObj = JSON.parse(objek[1]).data;
            let dataSequence = JSON.parse(objek[0]).data;
            let campaignName = dataObj[0].campaign_name;
            let requestedBy = dataObj[0].requested_by;
            let repeater = dataObj[0].repeater;
            let repeaterStr;
            let actionTime = dataObj[0].action_time;
            let totalTag = dataObj.length;
            let tagName = dataObj;

            let totalStep = dataSequence.length;
            
            if(repeater === '1') {
                repeaterStr = 'Daily';
            }else if(repeater === '7') {
                repeaterStr = 'Weekly';
            }else if(repeater === '30') {
                repeaterStr = 'Monthly';
            }else if(repeater === '365') {
                repeaterStr = 'Yearly';
            }

            /* ===========================Assigning value to html tag=========================== */
            $('#view_campaign_name').html(campaignName);
            $('#view_requested_by').html(requestedBy);
            $('#view_repeater').html(repeaterStr);
            $('#view_action_time').html(actionTime);

            /* Assigning tag name value */
            for(let i = 0; i < totalTag; i++) {
                $('#view_ul_tag').append(`<li>${tagName[i].tag_name}</li>`);
            }

            /* Assigning Step value */
            for(let u = 0; u < totalStep; u++) {
                /* Step 1 */
                if(dataSequence[u].action_id === '1') {
                    $('#view_ul_step').append(
                                `<li>Step ${dataSequence[u].sequence} - ${dataSequence[u].action_name} (Delay ${dataSequence[u].delay_time} Days)
                                    <ul style="padding-left:15px">
                                        <li style="font-weight:500">Titles
                                            <li style="list-style-type:none">${dataSequence[u].title}</li>
                                        </li>
                                    </ul>
                                    <ul style="padding-left:15px">
                                        <li style="font-weight:500">Content
                                            <li style="list-style-type:none">${dataSequence[u].content}</li>
                                        </li>
                                    </ul>
                                </li>`);
                /* Step 2 */
                }else if(dataSequence[u].action_id === '2') {
                    $('#view_ul_step').append(
                                `<li>Step ${dataSequence[u].sequence} - ${dataSequence[u].action_name} (Delay ${dataSequence[u].delay_time} Days)
                                    <ul style="padding-left:15px">
                                        <li style="font-weight:500">Content
                                            <li style="list-style-type:none">${dataSequence[u].content}</li>
                                        </li>
                                    </ul>
                                </li>`);
                /* Step 3 */
                }else if(dataSequence[u].action_id === '3') {
                    $('#view_ul_step').append(
                                `<li>Step ${dataSequence[u].sequence} - ${dataSequence[u].action_name} (Delay ${dataSequence[u].delay_time} Days)</li>`);
                }
                
            }            
        });
    }

    function closeModal() {
        $('.modal-campaign').fadeOut('slow');
        $('#modal-campaign_view').fadeOut('slow');
        $('#view_ul_tag').children().remove();      // Deleting html tag under id=view_ul_tag
        $('#view_ul_step').children().remove();     // Deleting html tag under id=view_ul_step
        $('#error_message_campaign').html('');
    }

    function openModal(access) {
        if(access === 'FULL'){
            $('#modal-campaign').fadeIn('slow');
        }else if(access === 'READ'){
            alert('You have no authorize on this page');
        }
    }

    function closeStep(id) {
        let jumlah = $("[class^=box-step]").length;     // Getting amount how many html tag which has 'box-step' in its class name ex: (box-step1, box-step2) = 2
        
        /* If closing box step 3, showing close icon of box step 2 */
        if(jumlah === '3') {
            $('#close-btn-icon2').css('display','block');
        }        
        $('.box-step'+id).remove();
    }

    function closeTag(id) {
        let jumlah = $("[class^=tag_step]").length;
        let height = $('.modal-campaign_content').outerHeight();
        if(jumlah=='3') {
            $('#close-tag-icon2').css('display','inline-block');
        }   
        $('.tag_step'+id).remove();
        $('.modal-campaign_content').css('height',height-30);
    }

    function addStep() {
        let clone = $("[class^=box-step]:last").clone(false,false)[0];      // Clone the last html tag with which has 'box-step' in its class name   
        let className = $("[class^=box-step]:last").attr('class');              // Get the class name of the last box-step in html
        let num = className.split('box-step');                                  // Get the number of box-step. ex: box-step2 , num[0] = box-step, num[1] = 2
        let newNum = parseInt(num[1]) + 1;
        let total = $("[class^=box-step]").length;

        if(total === '3'){
            alert('Already on limit Step');
        }else{
            /* Renaming the name and id and some of style and attribute into  */
            $('.box-step' + num[1]).after(clone);
            $("[class^=box-step]:last").attr('class','box-step' + newNum);
            $('[id^=label-]:last').html('Step ' + newNum);
            $('[id^=label-]:last').attr('id','label-' + newNum);

            $('[id^=content-label]:last').attr('id','content-label' + newNum);
            $('[id^=title-label]:last').attr('id','title-label' + newNum);

            $('[id^=action-step-]:last').attr('onchange','changeAction(this.value,' + newNum + ')');
            $('[id^=action-step-]:last').attr('name','action-step-' + newNum);
            $('[id^=action-step-]:last').attr('id','action-step-' + newNum);

            $('[id^=title-step-]:last').attr('name','title-step-' + newNum);
            $('[id^=title-step-]:last').attr('id','title-step-' + newNum);

            $('[id^=content-step-]:last').attr('name','content-step-' + newNum);
            $('[id^=content-step-]:last').attr('id','content-step-' + newNum);

            $('[id^=day-delay-]:last').attr('name','day-delay-' + newNum);
            $('[id^=day-delay-]:last').attr('id','day-delay-' + newNum);

            $('[id^=time-delay-]:last').attr('name','time-delay-' + newNum);
            $('[id^=time-delay-]:last').attr('id','time-delay-' + newNum);

            $('[id^=close-btn-icon]:last').attr('onclick',`closeStep(${newNum})`);
            $('[id^=close-btn-icon]:last').css('display','block');
            $('[id^=close-btn-icon]:last').attr('id','close-btn-icon' + newNum);

            if(total === '2') {
                $('#close-btn-icon2').css('display','none');
            }

            $('#content-label' + newNum).css('display','none');
            $('#content-step-' + newNum).css('display','none');
            $('#title-label' + newNum).css('display','none');
            $('#title-step-' + newNum).css('display','none');
            $('.box-step' + newNum).css('height','104');  
            if(newNum === 3){
                let action1 = $('#action-step-1').val();
                let action2 = $('#action-step-2').val();
                let action3 = $('#action-step-3').val();
            }
            /* Renaming the name and id and some of style and attribute into  */
        }
    }

    function addTag() {
        let clone = $("[class^=tag_step]:last")
        .clone(false,false)[0];
        let className = $("[class^=tag_step]:last").attr('class');
        let num = className.split('tag_step');
        let newNum = parseInt(num[1]) + 1;
        let height = $('.modal-campaign_content').outerHeight();  
        let jumlah = $("[class^=tag_step]").length;
        if(jumlah === '3'){
            alert('Already on limit Tag');
        }else{
            $(clone).appendTo('#tag-container');
            $('.modal-campaign_content').css('height',height+30);
            $('[class^=tag_step]:last').attr('class','tag_step' + newNum);
            $('[id^=tag-]:last').attr('name','tag-' + newNum);
            $('[id^=tag-]:last').attr('id','tag-' + newNum);

            $('[id^=close-tag-icon]:last').attr('onclick',`closeTag(${newNum})`);
            $('[id^=close-tag-icon]:last').css('display','inline-block');
            $('[id^=close-tag-icon]:last').attr('id','close-tag-icon' + newNum);
            if(jumlah === '2') {
                $('#close-tag-icon2').css('display','none');
            }
        }
    }
    
    function changeAction(val,num_step){
        let height = $('.modal-campaign_content').outerHeight(); 
        let tagHeight = $('#tag-container').outerHeight(); 
        let action1 = $('#action-step-1').val();
        let action2 = $('#action-step-2').val();
        let action3 = $('#action-step-3').val();
        
        if(val === 'sms') {
            $('#content-label' + num_step).css('display','inline-block');
            $('#content-step-' + num_step).css('display','inline-block')
            $('.box-step' + num_step).css('height','217px');    
            $('#title-label' + num_step).css('display','none');
            $('#title-step-' + num_step).css('display','none');
        }else if(val === 'push_notification') {
            $('#title-label' + num_step).css('display','inline-block');
            $('#title-step-' + num_step).css('display','inline-block');
            $('#content-label' + num_step).css('display','inline-block');
            $('#content-step-' + num_step).css('display','inline-block')
            $('.box-step' + num_step).css('height','294');    
        } else{
            $('#content-label' + num_step).css('display','none');
            $('#content-step-' + num_step).css('display','none');
            $('#title-label' + num_step).css('display','none');
            $('#title-step-' + num_step).css('display','none');
            $('.box-step' + num_step).css('height','104');
        }

        if((action1 === '' || action1 === 'call') && (action2 === '' || action2 === 'call' || action2 === undefined)){
            $('.btn-campaign-submit-modal').css('margin-top','0px');
            $('.modal-campaign_content').css('height','auto');                         
            if(action3 === '' || action3 === 'call') {
                $('.btn-campaign-submit-modal').css('margin-top','0px');
                $('.modal-campaign_content').css('height','auto');
            }
        }

        if(action3 === 'sms') {
            $('.modal-campaign_content').css('height', tagHeight + 513);
            $('.btn-campaign-submit-modal').css('margin-top','90px');                
        }else if(action3 === 'push_notification') {
            $('.modal-campaign_content').css('height', tagHeight + 563);
            $('.btn-campaign-submit-modal').css('margin-top','135px');
        }
        
        if(action1 === 'sms' || action2 === 'sms') {
            $('.modal-campaign_content').css('height', tagHeight + 513);
            $('.btn-campaign-submit-modal').css('margin-top','90px');
            if(action3 === 'push_notification') {
                $('.modal-campaign_content').css('height', tagHeight + 563);
                $('.btn-campaign-submit-modal').css('margin-top','135px');
            }else if(action3 !== undefined) {
                $('.modal-campaign_content').css('height', tagHeight + 513);
                $('.btn-campaign-submit-modal').css('margin-top','90px');                
            }
        }
        if(action1 === 'push_notification' || action2 === 'push_notification') {
            $('.modal-campaign_content').css('height', tagHeight + 563);
            $('.btn-campaign-submit-modal').css('margin-top','135px');
            if(action3 !== undefined) {
                $('.modal-campaign_content').css('height', tagHeight + 563);
                $('.btn-campaign-submit-modal').css('margin-top','135px');
            }
        }

        if((action1 === '' || action1 === 'call') && (action2 === '' || action2 === 'call')){
            $('.btn-campaign-submit-modal').css('margin-top','0px');
            $('.modal-campaign_content').css('height','auto');                         
            if(action3 === '' || action3 === 'call') {
                $('.btn-campaign-submit-modal').css('margin-top','0px');
                $('.modal-campaign_content').css('height','auto');
            }else if(action3 === 'sms') {
                $('.modal-campaign_content').css('height', tagHeight + 513);
                $('.btn-campaign-submit-modal').css('margin-top','90px');                
            }else if(action3 === 'push_notification') {
                $('.modal-campaign_content').css('height', tagHeight + 563);
                $('.btn-campaign-submit-modal').css('margin-top','135px');
            }
        }
        
    }

    function validation() {
        let campaignName = $('#campaign-val').val();
        let requestBy = $('#request-val').val();
        let startTime = $('#start_date-val').val();
        let repeater = $('#repeat_num').val();
        let actionTime = $('#action_time').val();
        let errorCode = false;

        $('#campaign-val').css('border','none');         
        $('#start_date-val').css('border','none');                       
        $('#repeat_num').css('border','none');                
        $('#action_time').css('border','none');  
        $('#tag-1').css('border','none');
        $('#tag-2').css('border','none');
        $('#tag-3').css('border','none');
        $('#content-step-1').css('border','none');  
        $('#content-step-1').css('border','none');
        $('#title-step-1').css('border','none');
        $('#action-step-1').css('border','none');
        $('#day-delay-1').css('border','none');
        $('#content-step-2').css('border','none');  
        $('#content-step-2').css('border','none');
        $('#title-step-2').css('border','none');
        $('#action-step-2').css('border','none');
        $('#day-delay-2').css('border','none');
        $('#content-step-3').css('border','none');  
        $('#content-step-3').css('border','none');
        $('#title-step-3').css('border','none');
        $('#action-step-3').css('border','none');
        $('#day-delay-3').css('border','none');
        $('#error_message_campaign').html('');
        $('.btn-campaign-submit-modal').css('box-shadow','none');

        // CAMPAIGN NAME VALIDATION
        if(campaignName === '') {
            $('#error_message_campaign').html('Please insert Campaign Name');
            $('#campaign-val').focus();
            $('#campaign-val').css('border','1px solid red');
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        // REQUEST BY VALIDATION
        if(requestBy === '') {
            $('#error_message_campaign').html('Please insert Requested By');
            $('#request-val').focus();
            $('#request-val').css('border','1px solid red');          
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        // START TIME VALIDATION
        if(startTime === '') {
            $('#error_message_campaign').html('Please insert Start Time');
            $('#start_date-val').focus(); 
            $('#start_date-val').css('border','1px solid red');                       
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        // REPEATER VALIDATION
        // if(repeater === '') {
        //     $('#error_message_campaign').html('Please insert Repeater');
        //     $('#repeat_num').focus();
        //     $('#repeat_num').css('border','1px solid red');                
        //     $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
        //     errorCode = true;
        //     return false;
        // }

        // ACTION TIME
        if(actionTime === '') {
            $('#error_message_campaign').html('Please insert Action Time');
            $('#action_time').focus();
            $('#action_time').css('border','1px solid red');  
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }


        // TAG 1 VALIDATION
        let tag = [];
        if($('#tag-1').val() !== undefined) {
            tag[0] = $('#tag-1').val();
            if(tag[0] === '') {
                $('#error_message_campaign').html('Please select Tag');
                $('#tag-1').focus();
                $('#tag-1').css('border','1px solid red');
                $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                errorCode = true;
                return false;
            }
        }
        // TAG 2 VALIDATION
        if($('#tag-2').val() !== undefined) {
            tag[1] = $('#tag-2').val();
            if(tag[1] === '') {
                $('#error_message_campaign').html('Please select Tag');
                $('#tag-2').focus();
                $('#tag-2').css('border','1px solid red');
                $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                errorCode = true;
                return false;
            }
        }
        // TAG 3 VALIDATION
        if($('#tag-3').val() !== undefined) {
            tag[2] = $('#tag-3').val();
            if(tag[2] === '') {
                $('#error_message_campaign').html('Please select Tag');
                $('#tag-3').focus();
                $('#tag-3').css('border','1px solid red');
                $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                errorCode = true;
                return false;
            }
        }

        let step = new Object();
        
        // STEP 1 VALIDATION
        if($('#action-step-1').val() !== undefined) {
            step['action_name1'] = $('#action-step-1').val();
            step['delay1'] = $('#day-delay-1').val();
            if(step['action_name1'] === 'sms') {
                step['content1'] = $('#content-step-1').val();
                if(step['content1'] === '') {
                    $('#error_message_campaign').html('Please insert Content');
                    $('#content-step-1').focus();
                    $('#content-step-1').css('border','1px solid red');  
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
            }else if(step['action_name1'] === 'push_notification') {
                step['content1'] = $('#content-step-1').val();
                step['title1'] = $('#title-step-1').val();
                if(step['content1'] === '') {
                    $('#error_message_campaign').html('Please insert Content');
                    $('#content-step-1').focus();
                    $('#content-step-1').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
                if(step['title1'] === '') {
                    $('#error_message_campaign').html('Please insert Title');
                    $('#title-step-1').focus();
                    $('#title-step-1').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
            }

            if(step['action_name1'] === '') {
                $('#error_message_campaign').html('Please select Tag');
                $('#action-step-1').focus();
                $('#action-step-1').css('border','1px solid red');
                $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                errorCode = true;
                return false;
            }
            // if(step['delay1'] === '') {
            //     $('#error_message_campaign').html('Please insert delay time');
            //     $('#day-delay-1').focus();
            //     $('#day-delay-1').css('border','1px solid red');
            //     $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            //     errorCode = true;
            //     return false;
            // }
        }

        // STEP 2 VALIDATION
        if($('#action-step-2').val() !== undefined) {
            step['action_name2'] = $('#action-step-2').val();
            step['delay2'] = $('#day-delay-2').val();
            if(step['action_name2'] === 'sms') {
                step['content2'] = $('#content-step-2').val();
                if(step['content2'] === '') {
                    $('#error_message_campaign').html('Please insert Content');
                    $('#content-step-2').focus();
                    $('#content-step-2').css('border','1px solid red');  
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
            }else if(step['action_name2'] === 'push_notification') {
                step['content2'] = $('#content-step-2').val();
                step['title2'] = $('#title-step-2').val();
                if(step['content2'] === '') {
                    $('#error_message_campaign').html('Please insert Content');
                    $('#content-step-2').focus();
                    $('#content-step-2').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
                if(step['title2'] === '') {
                    $('#error_message_campaign').html('Please insert Title');
                    $('#title-step-2').focus();
                    $('#title-step-2').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
            }

            if(step['action_name2'] === '') {
                $('#error_message_campaign').html('Please select Tag');
                $('#action-step-2').focus();
                $('#action-step-2').css('border','1px solid red');
                $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                errorCode = true;
                return false;
            }
            // if(step['delay2'] === '') {
            //     $('#error_message_campaign').html('Please insert delay time');
            //     $('#day-delay-2').focus();
            //     $('#day-delay-2').css('border','1px solid red');
            //     $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            //     errorCode = true;
            //     return false;
            // }
        }

        // STEP 3 VALIDATION
        if($('#action-step-3').val() !== undefined) {
            step['action_name3'] = $('#action-step-3').val();
            step['delay3'] = $('#day-delay-3').val();
            if(step['action_name3'] === 'sms') {
                step['content3'] = $('#content-step-3').val();
                if(step['content3'] === '') {
                    $('#error_message_campaign').html('Please insert Content');
                    $('#content-step-3').focus();
                    $('#content-step-3').css('border','1px solid red');  
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
            }else if(step['action_name3'] === 'push_notification') {
                step['content3'] = $('#content-step-3').val();
                step['title3'] = $('#title-step-3').val();
                if(step['content3'] === '') {
                    $('#error_message_campaign').html('Please insert Content');
                    $('#content-step-3').focus();
                    $('#content-step-3').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
                if(step['title3'] === '') {
                    $('#error_message_campaign').html('Please insert Title');
                    $('#title-step-3').focus();
                    $('#title-step-3').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                    errorCode = true;
                    return false;                  
                }
            }

            if(step['action_name3'] === '') {
                $('#error_message_campaign').html('Please select Tag');
                $('#action-step-3').focus();
                $('#action-step-3').css('border','1px solid red');
                    $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
                errorCode = true;
                return false;
            }
            // if(step['delay3'] === '') {
            //     $('#error_message_campaign').html('Please insert delay time');
            //     $('#day-delay-3').focus();
            //     $('#day-delay-3').css('border','1px solid red');
            //         $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            //     errorCode = true;
            //     return false;
            // }
        }

        if(!errorCode) {
            return true;
        }
    }

</script>