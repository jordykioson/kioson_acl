<?php 
    $nama = $this->session->name;
    $level = $this->session->level;

    /* Getting access session of the user */
    $campaignAccess = false;
    $tagAccess = false;
    $reportAccess = false;
    $csAccess = false;
    $fdsAccess = false;
    foreach($this->session->access as $key) {
        if($key['path'] == 'crm_campaign' && $key['access'] != 'BLOCK') {
            $campaignAccess = true;
        }else if($key['path'] == 'crm_tags' && $key['access'] != 'BLOCK') {
            $tagAccess = true;
        }else if($key['path'] == 'crm_report' && $key['access'] != 'BLOCK') {
            $reportAccess = true;
        }else if($key['path'] == 'crm_cs' && $key['access'] != 'BLOCK') {
            $csAccess = true;
        }else if($key['path'] == 'fds_config' && $key['access'] != 'BLOCK'  || $key['path'] == 'fds_blacklist'  && $key['access'] != 'BLOCK') {
            $fdsAccess = true;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kioson Dashboard</title>
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="<?php echo base_url(); ?>assets/css/login_page-medium.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <style>
        .small-text_onBoarding {
            font-size:11px;
            font-family:'Roboto';
            color:#59595c
        }

        .btn_onBoarding {
            width:210px;
            height:35px;
            background-color:#1594b9;
            color:white;
            font-size:14px;
            margin-bottom:5px;
            border-radius:8px;
            cursor:pointer
        }
    </style>
</head>
<body style="background-color:#f2f2f2!important">
    <div class="wrapper">

        <div>
            <img src="<?php echo base_url(); ?>assets/logo_kioson.png" alt="logo_kioson" width="100" height="25">
        </div>
        <div>
            <label class="small-text_onBoarding">Dashboard System</label>
        </div>
        <div style="margin-top:20px">
            <label for="" class="small-text_onBoarding">Hi <b><?php echo $nama; ?></b> , you have a role as <b><?php echo $level; ?></b>.
            <br><br>
                These are dashboards that you can choose to enter :</label>
        </div>

        <!-- Moving to page as the user access -->
        <input 
            type="button" 
            onclick="movePage('<?php if($campaignAccess){echo 'campaign';}else if($level == 'Superadmin'){echo 'userLibrary';}else if($tagAccess){echo 'tags';}else if($reportAccess){echo 'report';}else if($csAccess){echo 'cs';}else{echo 'none';}?>')" 
            value="CRM Dashboards" class="btn_onBoarding"
        >
        <input 
            type="button" 
            onclick="movePage('<?php if($fdsAccess){
                echo 'watchTower'; 
                }else{echo 'none';}?>')" 
            value="Watch Tower Dashboard" 
            class="btn_onBoarding"
        > 
        <!-- Moving to page as the user access -->
               
        <div style="margin-top:20px" id="">
            <label for="" class="small-text_onBoarding">However, if you suddenly want to cancel your action, you can click</label>
        </div>
        <button onclick="logOut()" class="btn_onBoarding" style="background-color:#59595c;width:120px;">
            <img width="18" height="18" src="<?php echo base_url();?>assets/logout_icon.png" alt="logout_icon">
         Log Out</button>
        
    </div>
    <script>
        function logOut() {
            $.post("<?php echo base_url();?>Main/logout", function(data) {
                location.reload();
            });
        }

        function movePage(page) {
            if(page === 'none') {
                alert('You have no authorize on this page');
            }else{
                $.post("<?php echo base_url();?>Main/"+page, function(data) {
                    window.location.href = "<?php echo base_url();?>Main/"+page;    //Move to declared page
                });
            }
        }

        function fdsPage(id) {
            if(id === 'none') {
                alert('You have no authorize on this page');
            }else{
                    window.location.href = "https://fds.kioson.id/auth/"+id;    //Move to declared page
            }
        }
        
    </script>
</body>
</html>