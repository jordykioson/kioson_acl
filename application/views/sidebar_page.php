<?php
    /* Getting access session of the user */
    $campaignAccess = false;
    $tagAccess = false;
    $reportAccess = false;
    $csAccess = false;
    $userAccess =  $this->session->level;
    foreach($this->session->access as $key) {
        if($key['path'] == 'crm_campaign' && $key['access'] != 'BLOCK') {
            $campaignAccess = true;
        }else if($key['path'] == 'crm_tags' && $key['access'] != 'BLOCK') {
            $tagAccess = true;
        }else if($key['path'] == 'crm_report' && $key['access'] != 'BLOCK') {
            $reportAccess = true;
        }else if($key['path'] == 'crm_cs' && $key['access'] != 'BLOCK') {
            $csAccess = true;
        }
    }
?>

<div class="modal-sidebar" onclick="closeMenu()">
    <div class="sidebar-container">
        <div class="" style="" >
            <div style="display:inline;margin-right:8px">
                <img src="<?php echo base_url(); ?>assets/burger_menu_icon.png" alt="burger_menu" class="" width="15" height="12" onclick="openMenu()">
            </div>
            <div style="display:inline-grid;">
                <img src="<?php echo base_url(); ?>assets/logo_kioson.png" alt="logo_kioson" width="87" height="22" style="" >
                <label for="" class="" style="font-size:12px;margin-top:5px" >Customers Relationship Management</label>
            </div>
        </div>
        <div style="border:0.5px solid #d6d6d6;margin-bottom:20px;"></div>
        <ul style="padding-left: 0px;">
            <!-- if user has no access, the menu not shown -->
            <?php if($userAccess == 'Superadmin'){ ?>
            <li type="none" style="margin-bottom: 25px; font-size: 13px;font-family: 'Roboto'" id="li_user">
                <a href="<?php echo base_url(); ?>Main/userLibrary/" style="color: #59595c;">
                    <span style="margin-right:15px"><img src="<?php echo base_url(); ?>assets/account_icon.png" alt="campaign_icon" width="12" height="13"></span>
                User Library</a>
            </li>
            <?php } ?>

            <!-- if user has no access, the menu not shown -->
            <?php if($campaignAccess){ ?>
            <li type="none" style="margin-bottom: 25px; font-size: 13px;font-family: 'Roboto'" id="li_campaign">
                <a href="<?php echo base_url(); ?>Main/campaign/" style="color: #59595c;">
                    <span style="margin-right:15px"><img src="<?php echo base_url(); ?>assets/campaign_icon.png" alt="campaign_icon" width="12" height="13"></span>
                Campaign Library</a>
            </li>
            <?php } ?>

            <!-- if user has no access, the menu not shown -->
            <?php if($tagAccess){ ?>
            <li type="none" style="margin-bottom: 25px; font-size: 13px;font-family: 'Roboto'" id="li_tag">
                <a href="<?php echo base_url(); ?>Main/tags/" style="color: #59595c;">
                    <span style="margin-right:10px"><img src="<?php echo base_url(); ?>assets/tag_icon.png" alt="tag_icon" width="16" height="13"></span>
                Tag Library</a>
            </li>
            <?php } ?>

            <!-- if user has no access, the menu not shown -->
            <?php if($reportAccess){ ?>            
            <li type="none" style="margin-bottom: 25px; font-size: 13px;font-family: 'Roboto'" id="li_report">
                <a href="<?php echo base_url(); ?>report" style="color: #59595c;">
                    <span style="margin-right:13px"><img src="<?php echo base_url(); ?>assets/report_icon.png" alt="report_icon" width="13" height="13"></span>
                Reporting</a>
            </li>
            <?php } ?>
            
            <!-- if user has no access, the menu not shown -->
            <?php if($csAccess){ ?>            
            <li type="none" style="margin-bottom: 25px; font-size: 13px;font-family: 'Roboto'" id="li_cs">
                <a href="<?php echo base_url(); ?>cs" style="color: #59595c;">
                    <span style="margin-right:13px"><img src="<?php echo base_url(); ?>assets/cs_icon.png" alt="cs_icon" width="13" height="13"></span>
                Customer Service</a>
            </li>
            <?php } ?>

        </ul>
        <div style="bottom:0;position:absolute;padding-bottom:25px;font-size:11px;color:#59595c">
            © Kioson Development Team 2018
        </div>
    </div>
</div>


<script>
    <?php if($page_selected == 'campaign') { ?>
        $('#li_campaign').css('font-weight','bold');
    <?php } else if($page_selected == 'tags') { ?>
        $('#li_tag').css('font-weight','bold');
    <?php } else if($page_selected == 'report') { ?>
        $('#li_report').css('font-weight','bold');
    <?php } else if($page_selected == 'cs') { ?>
        $('#li_cs').css('font-weight','bold');
    <?php } else if($page_selected == 'user') { ?>
        $('#li_user').css('font-weight','bold');
    <?php } else { ?>
        $('#li_campaign').css('font-weight','normal');
        $('#li_tag').css('font-weight','normal');
    <?php } ?>

    function openMenu() {
        $('.modal-sidebar').show('slow');
    }

    function closeMenu() {
        $('.modal-sidebar').hide('slow');
    }

    function noAccess() {
        alert('You don\'t have access to this page');
    }
</script>