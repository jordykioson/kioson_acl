<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if($page_selected == 'watchTower'){ ?>
        <title>Kioson Watch Tower</title>
    <?php }else{ ?>
        <title>Kioson CRM</title>
    <?php } ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="<?php echo base_url(); ?>assets/css/campaign_page-medium.css">
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="<?php echo base_url(); ?>assets/css/tags_page-medium.css">
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="<?php echo base_url(); ?>assets/css/header_page-medium.css">
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="<?php echo base_url(); ?>assets/css/sidebar_page-medium.css">
    
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <style>
        select {
            cursor: pointer;
        }
    </style>
</head>
<body>
    <?php 
        $nama = $this->session->name;
        $level = $this->session->level;

        $campaignAccess = false;
        $tagAccess = false;
        $reportAccess = false;
        $csAccess = false;
        $fdsAccess = false;
        foreach($this->session->access as $key) {
            if($key['path'] == 'crm_campaign' && $key['access'] != 'BLOCK') {
                $campaignAccess = true;
            }else if($key['path'] == 'crm_tags' && $key['access'] != 'BLOCK') {
                $tagAccess = true;
            }else if($key['path'] == 'crm_report' && $key['access'] != 'BLOCK') {
                $reportAccess = true;
            }else if($key['path'] == 'crm_cs' && $key['access'] != 'BLOCK') {
                $csAccess = true;
            }else if($key['path'] == 'fds_config' && $key['access'] != 'BLOCK' ||$key['path'] == 'fds_blacklist' && $key['access'] != 'BLOCK') {
                $fdsAccess = true;
            }
        }
    ?>
    <div class="main-container" >
        <div class="header-container" >

            <!-- Header left -->
            <div class="header-left" >
                <div style="display:inline-block">
                    <img src="<?php echo base_url(); ?>assets/burger_menu_icon.png" alt="burger_menu" class="burger-menu" onclick="openMenu()">
                </div>
                <div style="display:inline-block">
                    <img src="<?php echo base_url(); ?>assets/logo_kioson.png" alt="logo_kioson" class="logo-kioson" >
                    <label for="" class="small-text" >Customers Relationship Management</label>
                </div>                
            </div>

            <!-- Header Right -->
            <div class="header-right" onclick="showMenu()">
                <div style="display:inline-block">
                    <img src="<?php echo base_url(); ?>assets/account_icon.png" alt="account_icon" class="logo-account">
                </div>
                <div style="width: 136px; display:inline-block; padding-left: 10px">
                    <label style="font-size:14px;margin-bottom: 3px; line-height: 14px; color: #59595c"><?php echo $nama; ?></label>
                    <label style="display:block;font-size: 11px; margin-bottom: 0px; line-height: 10px; color: #59595c;"><?php echo $level; ?></label>
                </div>
                <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown_icon" id="show_menu" class="dropdown-icon">
                <img src="<?php echo base_url(); ?>assets/dropup_icon.png" alt="dropdown_icon" id="hide_menu" style="display:none" class="dropdown-icon" onclick="hideMenu()">
            </div>

        </div>

        <!-- Account Menu -->
        <div class="account-menu" style="display:none">
            <div style="color:#aeaeae;margin-bottom:10px">
                <span style="padding-left:10px;">Switch Dashboard to</span>
            </div>

            <?php if($campaignAccess || $tagAccess || $reportAccess || $csAccess){ ?>
            <div id="dashboard-1" onclick="movePage('<?php if($campaignAccess){echo 'campaign';}else if($tagAccess){echo 'tags';}else if($reportAccess){echo 'report';}else if($csAccess){echo 'cs';}else{echo 'none';}?>')" style="padding-bottom:2px;margin-bottom:2px;border-bottom:0.5px solid #979797">
                <span style="padding-left:10px;color:#ffffff">CRM Dashboard</span>
                <span id="crm_status" style="float:right;color:#aeaeae;padding-right:8px;display:none'">current</span>
            </div>
            <?php } ?>

            <?php if($fdsAccess){ ?>
            <div id="dashboard-2" onclick="movePage('<?php if($fdsAccess){
                echo 'watchTower'; 
                }else{echo 'none';}?>')"  style="color:#ffffff;padding-bottom:2px;margin-bottom:2px;border-bottom:0.5px solid #979797">
                <span style="padding-left:10px;">Watch Tower Dashboard</span>
                <span id="watch_tower_status" style="float:right;color:#aeaeae;padding-right:8px;display:none">current</span>                
            </div>
            <?php } ?>

            <div style="padding-left:10px;color:#aeaeae;margin-bottom: 10px">If you want to:</div>
            <div id="logout-menu" onclick="javascript:logOut()" style="padding-bottom:5px;">
                <span style="padding-left:10px;color:#ffffff">Log Out</span>
                <span style="float:right;padding-right:8px;">
                    <img width="15" height="15" src="<?php echo base_url();?>assets/logout_icon.png" alt="">
                </span>
            </div>
        </div>

        <!-- div main container closing tag on home_page -->

        <script>
            function movePage(page) {
                if(page === 'none') {
                    alert('You have no authorize on this page');
                }else{
                    $.post("<?php echo base_url();?>Main/"+page, function(data) {
                        window.location.href = "<?php echo base_url();?>Main/"+page;
                    });
                }
            }

            function fdsPage(id) {
                if(id === 'none') {
                    alert('You have no authorize on this page');
                }else{
                        window.location.href = "https://fds.kioson.id/auth/"+id;    //Move to declared page
                }
            }

            function showMenu() {
                $('.account-menu').toggle('slow',function() {
                    if($('.account-menu').is(":visible")) {
                        $('#show_menu').show();
                        $('#hide_menu').hide();
                    }else {
                        $('#show_menu').hide();
                        $('#hide_menu').show();
                    }
                });
                
            }

            function hideMenu() {
                $('.account-menu').css('display','none');
                $('#hide_menu').hide();
                $('#show_menu').show();
            }

            function logOut() {
                $.post("<?php echo base_url();?>Main/logout", function(data) {
                    location.reload();
                });
            }

            $(document).ready(function() {
                $('.content-container').click(function() {
                    $('.account-menu').css('display','none');
                    $('#hide_menu').hide();
                    $('#show_menu').show();
                })

                <?php if($page_selected == 'campaign') {?>
                    $('#crm_status').show();
                    $('#watch_tower_status').hide(); 
                    $('#dashboard-1').removeAttr('onclick');                   
                <?php }else if($page_selected == 'watchTower'){?>
                    $('#watch_tower_status').show();
                    $('#crm_status').hide();               
                    $('#dashboard-2').removeAttr('onclick');     
                <?php }?>
            })
            
        </script>
