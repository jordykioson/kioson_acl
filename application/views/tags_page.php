<?php 
    $deleteIcon = base_url()."assets/delete_icon.png";

    /* Getting access session of the user */
    $writeAccess = false;
    $readAccess = false;
    foreach($this->session->access as $key) {
        if($key['path'] == 'crm_tags' && $key['access'] == 'FULL') {
            $writeAccess = true;
        }else if($key['path'] == 'crm_tags' && $key['access'] == 'READ') {
            $readAccess = true;
        }
    }
?>
    
        <div class="add-campaign" align="right">
            <?php if($writeAccess){ ?>
            <label for="" class="add-campaign-btn" onclick="openModal('<?php if($writeAccess){echo 'FULL';}else if($readAccess){echo 'READ';}?>')">+ Tambah Tag</label>
            <?php } ?>
        </div>
        <span style="">
            <table style="width:100%">
                <thead style="background-color:#f2f2f2;">
                    <tr style="height:32px;">
                        <th style="width: 35.8%;padding-left:15px;padding-top:8px;" class="header-table">Tag Name
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 14.7%;padding-left:12px;padding-top:8px;" class="header-table">Created Date
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 17%;padding-left:11px;padding-top:8px;" class="header-table">Created By
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 17.12%;padding-left:12px;padding-top:8px;" class="header-table">Requested By
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 15.3%;padding-left:11px;padding-right:11px;padding-top:8px;" class="header-table"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($response_tag)){ ?>
                    <?php foreach($response_tag as $key_tag) { ?>
                    <tr style="font-size: 12px;font-family:'Roboto';">
                        <td style="padding-left:15px;padding-top:12px;padding-bottom:10px">
                            <a href="javascript:viewDetail('<?php echo $key_tag->tag_id;?>')" style="color: #1594b9;">
                                <span id="name_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->tag_name; ?></span>
                            </a>
                        </td>
                        <td style="padding-left:12px;padding-top:12px;padding-bottom:10px" id="id_<?php echo $key_tag->tag_id;?>">
                            <span id="created_date_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->created_date; ?></span>
                        </td>
                        <td style="padding-left:11px;padding-top:12px;padding-bottom:10px">
                            <span id="created_by_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->created_by; ?></span>
                        </td>
                        <td style="padding-left:12px;padding-top:12px;padding-bottom:10px">
                            <span id="requested_by_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->requested_by; ?></span>
                        </td>
                        <?php if($writeAccess){ ?>
                        <td style="padding-left:11px;padding-top:12px;padding-bottom:10px;">
                            <a href="<?php if($writeAccess){echo base_url().'Main/deleteTag/'.$key_tag->tag_id;}else if($readAccess){echo 'javascript:noAccess()';}?>" id="delete_<?php echo $key_tag->tag_id; ?>" style="color:red;">
                                <img id="delete-btn" src="<?php echo $deleteIcon; ?>" alt="delete_button" style="width:12px;height:15px;margin-right:14px;">Delete
                            </a>
                        </td>
                        <?php } ?>
                        <div style="display:none" id="id_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->tag_id;?></div>
                        <div style="display:none" id="query_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->query;?></div>
                        <div style="display:none" id="description_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->description;?></div>
                        <div style="display:none" id="requested_by_<?php echo $key_tag->tag_id;?>"><?php echo $key_tag->requested_by;?></div>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <!-- Modal Box add tag -->
        <div class="modal-tags" style="display:none">
            <div class="modal-tags_content">
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">
                <div align="center" style="margin-bottom:20px;">
                    <label for="" style="color:#f7981d;font-size:14px;font-family:'Roboto';font-weight:500;margin-bottom:0px;line-height:16px;">Add Tag</label>
                </div>
                <div id="error_message_tag" style="text-align:center;color:red;font-size:12px;"></div>

                <form action="<?php echo base_url();?>Main/addTag/" method="post" id="form_add_tag">
                    <label for="" class="tag-style">Tag</label>
                    <input type="text" id="tag-val" name="tag_name" placeholder="Write Tag Here" class="input-tag-style" oninput="onTypeTag(this.value)">
                    <label for="" class="tag-style">Requested By</label>
                    <select name="tag_request_by" id="request-val" class="input-tag-style tag-style">
                        <option value="" class="input-tag-style">- Choose Requester Here -</option>
                        <?php foreach($response_user as $key_user) {?>
                            <option value="<?php echo $key_user->user_id; ?>" class="input-tag-style"><?php echo $key_user->name; ?></option>
                        <?php } ?>
                    </select>
                    <label for="" class="tag-style">Description</label>
                    <textarea id="description-val" name="tag_desciption" placeholder="Write description" class="input-tag-style" oninput="onTypeDescription(this.value)"></textarea>
                    <label for="" class="tag-style">Query</label>
                    <textarea id="query-val" name="tag_query" placeholder="Write query here" class="input-tag-style" style="margin-bottom:25px;" oninput="onTypeQuery(this.value)"></textarea>
                    <div align="right">
                        <input type="submit" value="Submit" class="btn-submit-modal" onclick="return validationTag()" disabled>
                    </div>
                </form>

            </div>
        </div>

        <!-- Modal Box View Detail -->
        <div class="modal-tags-view" style="display:none">
            <div class="modal-tags_content">
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">
                <div align="center" style="margin-bottom:20px;">
                    <label for="" style="color:#f7981d;font-size:14px;font-family:'Roboto';font-weight:500;margin-bottom:0px;line-height:16px;">Tag Details</label>
                </div>
                <div id="error_message_tag" style="text-align:center;color:red;font-size:12px;"></div>
                <form action="<?php echo base_url();?>Main/addTag/" method="post" id="form_add_tag">
                    <label for="" class="tag-style">Tag</label>
                    <input type="text" id="tag-val_view" name="tag_name_view" class="input-tag-style" disabled>
                    <label for="" class="tag-style">Requested By</label>
                    <input type="text" id="requested_by-val_view" name="tag_name_view" class="input-tag-style" disabled>
                    <label for="" class="tag-style">Description</label>
                    <textarea id="description-val_view" name="tag_desciption_view" class="input-tag-style" disabled></textarea>
                    <label for="" class="tag-style">Query</label>
                    <textarea id="query-val_view" name="tag_query_view" class="input-tag-style" style="margin-bottom:25px;" disabled></textarea>
                </form>
            </div>
        </div>

<script>
    <?php if(isset($error_name)){ ?>
        $('.modal-tags').show();    
        $('#error_message_tag').html('Tag Name already exist');
    <?php } ?>

    function closeModal() {
        $('.modal-tags').fadeOut('slow');
        $('.modal-tags-view').fadeOut('slow');
        $('#error_message_tag').html('');
    }

    function openModal(access) {
        if(access === 'FULL') {
            $('.modal-tags').fadeIn('slow');
        }else if(access === 'READ'){
            alert('You have no authorize on this page');
        }
    }

    function onTypeTag(val) {
        let description = $('#description-val').val();
        let query = $('#query-val').val();
        if(description !== '' && query !== '' && val !== '') {
            $('.btn-submit-modal').css('background-color','#1594b9');
            $('.btn-submit-modal').removeAttr('disabled');
        }else {
            $('.btn-submit-modal').css('background-color','#aeaeae');     
            $('.btn-submit-modal').attr('disabled','disabled');       
                   
        }
    }

    function onTypeDescription(val) {
        let tag = $('#tag-val').val();
        let query = $('#query-val').val();
        if(tag !== '' && query !== '' && val !== '') {
            $('.btn-submit-modal').css('background-color','#1594b9');
            $('.btn-submit-modal').removeAttr('disabled');
        }else {
            $('.btn-submit-modal').css('background-color','#aeaeae');   
            $('.btn-submit-modal').attr('disabled','disabled');       
                     
        }
    }

    function onTypeQuery(val) {
        let tag = $('#tag-val').val();
        let description = $('#description-val').val();
        if(description !== '' && tag !== '' && val !== '') {
            $('.btn-submit-modal').css('background-color','#1594b9');
            $('.btn-submit-modal').removeAttr('disabled');
        }else {
            $('.btn-submit-modal').css('background-color','#aeaeae');
            $('.btn-submit-modal').attr('disabled','disabled');       
        }
    }

    function validationTag() {
        let tag = $('#tag-val').val();
        let query = $('#query-val').val();
        let description = $('#description-val').val();
        let errorCode = false;

        if(tag === '') {
            $('#error_message_tag').html('Please insert Tag Name');
            $('#tag-val').focus();
            $('#tag-val').css('border','1px solid red');
            errorCode = true;
            return false;
        }
        if(query === '') {
            $('#error_message_tag').html('Please insert Query');
            $('#query-val').focus();
            $('#query-val').css('border','1px solid red');
            errorCode = true;
            return false;
        }
        if(description === '') {
            $('#error_message_tag').html('Please insert Description');
            $('#description-val').focus();
            $('#description-val').css('border','1px solid red');
            errorCode = true;
            return false;
        }
        if(!errorCode) {
            return true;
        }
    }

    function viewDetail(id) {
        let tagName = $('#name_'+id).text();
        let description = $('#description_'+id).text();
        let query = $('#query_'+id).text();
        let requestedBy = $('#requested_by_'+id).text();
        $('.modal-tags-view').fadeIn('slow');
        $('#tag-val_view').val(tagName);
        $('#description-val_view').val(description);
        $('#query-val_view').val(query);
        $('#requested_by-val_view').val(requestedBy);
    }

    function noAccess() {
        alert('You dont\'t have access to this page');
    }
</script>