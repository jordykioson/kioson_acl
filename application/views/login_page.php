<?php 
    $visible = false;
    $visibleIcon = base_url()."assets/visible_icon.png";
    $invisibleIcon = base_url()."assets/invisible_icon.png";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kioson Dashboard</title>
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="<?php echo base_url(); ?>assets/css/login_page-medium.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
</head>
<body style="background-color:#d7d7d7!important">
    <div class="wrapper">

        <div>
            <img src="<?php echo base_url(); ?>assets/logo_kioson.png" alt="logo_kioson" class="img-logo">
        </div>
        <div>
            <label style="color: #59595c; font-family: 'Roboto'; font-size: 10px">Dashboard System</label>
        </div>
        <div class="border"></div>

        <!-- Form Login -->
        <form action="<?php echo base_url(); ?>Main/onLogin/" method="post">
            <div>
                <label class="label-form">Username</label>
            </div>
            <div>
                <input class="input-form" name="username" id="username" type="text" autofocus>
            </div>
            <div>
                <label class="label-form">Password</label>
            </div>
            <div>
                <input class="input-form" name="password" id="password" type="password">
                <input type="text" value="show" id="spanId" style="display:none">
                <span class="visible-icon" onClick="press()">
                    <img id="img-visible" src="<?php echo $visibleIcon; ?>" alt="visible_icon" width="12px" height="8px">
                </span>
            </div>
            <input type="submit" value="Log In" class="submit-button">
        </form>

        <!-- Error message -->
        <div style="display:none;margin-top:10px" id="error-message">
            <div style="display:inline-block;width:12px;">
                <img src="<?php echo base_url(); ?>assets/warning_icon.png" alt="warning_icon" width="12" height="12" style="margin-top:-21px">
            </div>
            <div style="display:inline-block;width:220px;">
                <label for="" style="color:red;font-size:11px;">Username and Password doesn’t match or not found in database</label>
            </div>
        </div>
        
    </div>

    <script>
        // Condition whether login success or not
        <?php if(isset($response) && $response == 'Gagal'){ ?>
            $('#error-message').css('display','inline-block');
            $('.input-form').css('color','red');
        <?php }else { ?>
            $('#error-message').css('display','none');
            $('.input-form').css('color','black');
        <?php } ?>
        
        /* Function to show or hide password value */
        var status = $('#spanId').val();
        function press() {
            if(status==='show'){
                $('#img-visible').attr('src','<?php echo $invisibleIcon; ?>');
                $('#password').attr('type','text');
                status = 'hide';
            }else {
                $('#img-visible').attr('src','<?php echo $visibleIcon; ?>');
                $('#password').attr('type','password');
                status = 'show';
            }
            
        }
    </script>
</body>
</html>