
    <link href="<?php echo base_url('assets/d/bootstrap/css/bootstrap4.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/d/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
    <!-- <link href="<?php echo base_url('assets/d/datatables/css/jquey.dataTables.css')?>" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/d/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href='https://fonts.googleapis.com/css?family=Roboto Condensed' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    
    <link href="<?php echo base_url('assets/d/cs_style.css')?>" rel='stylesheet' type="text/css" >
     <style>

     @media only screen and (min-width: 1824px)
 {
    
  #buttons{
    margin-left:46% !important;
}
#table_filter{
    margin-left:28%;
}
.dt-buttons{
    text-align:center;
}
#KCPWho{
    margin-left:37px !important;
}
 }
     .orange{
         background-color:#f7981d;
     }
.col-sm-12{
    margin-top: -6px;

}

     #table th{
    color:#59595c;
    font-family: 'Roboto Condensed';
    font-size: 14px;
    height: 29px;

}
     input[type=search] {
        background-image: url('data:image/svg+xml;utf8,<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.1" id="spyglass" viewBox="0 0 22 22" height="22" width="22"><path d="m 10.011718,5.0898489 c -2.7000005,0 -4.9003915,2.2004 -4.9003915,4.9003996 0,2.6999995 2.200391,4.8983995 4.9003915,4.8983995 1.154374,0 2.204251,-0.4184 3.042969,-1.0898 l 3.207031,3.1113 0.626953,-0.6465 -3.185547,-3.0879 c 0.743242,-0.8594 1.208984,-1.964 1.208984,-3.1854995 0,-2.6999996 -2.20039,-4.9003996 -4.90039,-4.9003996 z m 0,1 c 2.2,0 3.90039,1.7004 3.90039,3.9003996 0,2.1999995 -1.70039,3.8983995 -3.90039,3.8983995 -2.2000005,0 -3.9003915,-1.6984 -3.9003915,-3.8983995 0,-2.1999996 1.700391,-3.9003996 3.9003915,-3.9003996 z" style="fill:#808080"/></svg>');
        background-repeat: no-repeat;
        background-color: #fff;
        background-position: 0px 3px !important;
        padding-left: 22px;
}

    .table-striped>tbody>tr:nth-of-type(odd){
    background-color:white;
}
         div.dataTables_wrapper div.dataTables_paginate {
 
  text-align: left !important;
}
         div.dataTables_wrapper div.dataTables_info {
             margin-top:10px;
             color:black;
        }
     .modal-title{
    margin-left: 44%;

}
    
    div.dataTables_wrapper div.dataTables_filter label{
        margin-left: 70% !important;
        position: inherit;
        z-index: 1;
        top: -5px;

    }
    #table_filter{
            position: inherit;
        }
    .form-inline{
        display: inline !important;
    }

     hr{
         margin-top: -3px;
    border-top: 2px solid #5555551c;
    width: 93%;
}

      div.dataTables_length label{
            margin-top:7px;
            /* color:white; */
        }
        div.dataTables_filter label{
            margin-top:14px;
            color:white;
        }
     /* Table Styling down bellow */
     .table>thead:first-child>tr:first-child>th{
        background-color: #f2f2f2;
   }
 

   .wwFind{
        display: grid;
        grid-template-columns: 7% 40% 40% ;
        grid-auto-rows: 125px;

        grid-gap: 3px;
        padding:1em;
        margin-bottom:34px;
    }
    .wwFind > div {
        padding: 1em;

    }
    .wwFind > div:nth-child(odd){
    }

  .nested{
            display: grid;
            grid-template-columns:repeat(2, 1fr);
            grid-auto-rows: 132px;
            grid-gap: 5px;
            margin-top: -13px;
            margin-left: -70px;

        }
        .nested > div{
            padding:1em;
        }
         .andHaveStatus{
            align-items:start;
        }
    .fromToWhen{
        display: grid;
            grid-template-columns:repeat(2, 1fr);
            grid-auto-rows: 70px;
            grid-gap: 5px;
    }    
    .ntainer{
        display: grid;
        grid-template-columns:100%;
        grid-gap: 3px;
    }

    .boxCase:hover{
        background-color:#fbfbfb;
    }
a:hover {
  text-decoration: none;
}

.badges {
    background-color:white;
  padding: 12px;
  margin: 12px 0;
  margin-top: -11px;
}
.badges p{
    font-size: 13px;
    margin: 0 0 4px;
    font-weight: bold;
}



#findFilter{
    padding-top: 0px;
    background-color:#1594b9;
    width:100px;
    height:22px;
}

 .dataTables_length{
    margin-top: -2px;

            float:left;
            color:white;
        }
       .reportTitle{

    padding: 1em;
    font-family: 'Roboto Condensed';
    font-size: 18px;
    /* font-weight: bold; */
    color: #fff;

}
input[type=text] {
    width: 100%;
    padding: 12px 20px;
    margin: 0px 0;
    box-sizing: border-box;
    border: none;
    border-bottom: 1px solid #e6e2e2;
}
.profileData p {
}
#DoAction{
    width: 83px;
    height: 13px;
    background-color: #1594b9;
    border-color :#1594b9;
    padding-top: 3px;
    border-radius: 6px;
}
.form-control{
    border: transparent;
}
.date_and_Status{
            display: grid;
            grid-template-columns: 50% 50%;
            /* grid-column-gap: 1em;
            grid-row-gap: 1em; 
                //same as bellow
             */
            grid-gap: 1px;
        }
        .date_and_Status p {
            font-weight: 500;
            color: #979797;
        }
.modal-header p {
    color:orange;
}

.table-bordered td{
    border:none;
 }
 .material-icons{
         font-size:15px !important;
         /* margin-left:66%; */
     }
  .dt-buttons{
    /* text-align:center; */
    /* background-color: #f7981d; */
    height: 45px;
    position: absolute;
    width: 1518px;
    margin-top: 10px;
    padding: 7px;
    z-index:1; 
}
.dt-buttons a  {
    color : white;

} 
#buttons{
    margin-left: 78%;
}
#campaign1{
    display:none;
}
#tag1{
    display:none;
}
#status1{
    display:none;
}
.form-control{
    -webkit-box-shadow: none;

}
.selectParentM{
    width:100%;
    overflow:hidden;
   /* background-color:#f2f2f2; */
   
}

.selectParentM select{
    width:100%;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    padding: 2px 2px 2px 2px;
    border: none;
    background:  url("<?php echo base_url();?>assets/dropdown_icon_white.png") no-repeat 357px center;
}


.selectParentC{
    width:100%;
    overflow:hidden;
   /* background-color:#f2f2f2; */
   
}

.selectParentC select{
    width:100%;

    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    padding: 2px 2px 2px 2px;
    border: none;
    background: url("<?php echo base_url();?>assets/dropdown_icon_black.png") no-repeat 96% center;
}
.selectParent{
    width:185px;
    overflow:hidden;
   /* background-color:#f2f2f2; */
   
}

.selectParent select{
    width: 172px;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    padding: 2px 2px 2px 2px;
    border: none;
    background: url("<?php echo base_url();?>assets/dropdown_icon_black.png") no-repeat 146px center;
}
div.dataTables_wrapper div.dataTables_length select {

        margin-left: 7px;
    margin-right: 7px;
}
    </style>
    </head> 
<body>

    <div class="headFilterData" style="height:54px;background-color: #f7981d;margin-top: -9px">
            <div><h5 class="reportTitle">Filter Data Customer Service</h5></div>
        </div>
        
    <div class="wwFind">
        <div id="KCPWho"style="margin-left: 12px;    margin-top: 3px;">
            KCP Who
        </div>
            <div class="selectParentC">
           
                            
                        <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="addCampaign"  style="width:17px;position:absolute; margin-left: 38% ;margin-top: -9px;" class="add-icon"> 
                                <?php echo $form_campaign; ?>
                        <p></p>
                <!-- <p style="text-align:center;">with tagged</p>
               

                        <p></p>
                        <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="addTag"  style="width:17px;position:absolute;margin-left:18%;margin-top:-4px;" class="add-icon">  -->

            </div>

        <div>
            <div class="nested ">
                <div class="andHaveStatus" style="justify-self: end">and have status </div>
                    <div class="selectParent" style=" margin-top: -3px;padding-left: 1px;    ">  
                       
                        <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="addStatus"  style="width:17px;position:absolute; margin-left: 186px; margin-top: -10px;" class="add-icon"> 

                        <?php echo $form_status; ?>
                        <p></p>

                    </div>
                 </div>
            </div>
              
        <div class="fromText" style="justify-self: end;">
             From
        </div>
        <div>
            <div class="input-daterange">
                <div class="fromToWhen">
                    <div>
                        <input name="date_start" id="start_date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" style="background:#f2f2f2;color:#59595c;height:31px;width:90%;border-radius:4px;margin-left:-1px;    margin-top: -1px;">
                    </div>
                    <div>
                    <p style="margin-left: -17px;">until</p>
                   <input name="date_end" id="end_date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" style="background:#f2f2f2;color:#59595c;height:31px;margin-top: -38px;margin-left: 30px;width:90%;border-radius:4px;">
                    </div>
                    <div>
                        <button type="button" id="btn-filter" class="btn btn-primary" style="width:134px">Find</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="ntainer">
    
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <div id="buttons"></div>

            <thead>
                <tr>
                    <th>KCP ID</th>                                        
                    <th>Name</th>
                    <th>Campaign</th>
                    <th>Status Call</th>
                    <th>Date Hitted</th>
                    <th>Phone number</th>
                    <th>feedback</th>
                    <th>Agent </th>

                    <th style="width:125px;">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

           
        </table>
    </div>





    <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <p class="modal-title">Call Action</p>

                <!-- <button type="button" class="close" style="margin-top: -21px;"data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">

            </div>
            <div class="modal-body form">
                <!-- <form action="#" id="form" class="form-horizontal"> -->
                    <!-- <input type="hidden" value="" name="retailer_id"/> -->
                    <div class="wrapper3">
                        <div class="profileName">
                            <p>KCP ID</p>
                            <p>Name</p>
                            <p>Address</p>
                            <p>Phone</p>  
                        </div>
                        <div id="profile" style="margin-top: 1px;">
                            <!--  -->
                        </div>
                    
                    </div>  

                    <div class="wrapper2Box">
                        <!-- Case 1 -->
                        <div class="classTitle">
                            <p> History Case</p>
                            <hr width="100%">
                            <div id="case1" class="dropdownCase">
                              
                                
                            </div>
                                        
                        </div>
                        
                        <!-- Case 2 -->
                        <form action="#" id="form" class="form-horizontal">

                        <div class="classTitle" >
                            <p>New Case </p>
                            <hr>
                                <div id="case2"class="dropdownCase">
                            
                                </div>
                        </div>
                    </div>
                    <div class="wrapperCalledBy">
                        <!-- <div >
                            <p>Called at 27/12/2018,4:20</p>
                            <p>by Safira Ryanatami</p>
                        </div>
                        <div>
                            <p>Called at 27/12/2018,4:20</p>
                            <p>by Christian Handoko</p>
                        </div> -->
                        <div>
                        </div>
                        <div id="calledBy">
                        </div>
                    </div>
                   
                    <div class="form-body">
                 
                    </div> 
               
                </form>
            </div>
              
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Submit</button>
                <!-- <input  id="thebutton" type="submit" /> -->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->





 <link href="<?php echo base_url(); ?>assets/d/w3.css" rel="stylesheet">
 <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>  -->
 
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> 
 <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> -->

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css"> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>
<!-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.excel.min.js"></script>   -->
 <!-- <script src="<?php echo base_url('assets/d/jquery/jquery-2.1.4.min.js')?>"></script> -->
 <!-- <script src="<?php echo base_url('assets/d/datatables/js/jquery.dataTables.min.js')?>"></script>  -->

 <script src="<?php echo base_url('assets/d/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/d/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/d/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
	    <!-- <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet"> -->








<script type="text/javascript">


 function closeModal() {
        $('#modal_form').modal('hide');
        $('#error_message_tag').html('');
    }



var save_method; 
var table;

$(function() {



 
    table = $('#table').DataTable({ 
      
        "processing": true, 
        "serverSide": true, 
        "order": [], 
        "columnDefs": [
    { "orderable": false, "targets": 8}
  ],
        "ajax": {
            "url": "<?php echo site_url('cs/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.campaign = $('#campaign').val();                
                data.campaign2 = $('#campaign2').val();
                data.campaign3 = $('#campaign3').val();
                data.tag = $('#tag').val();                
                data.tag2 = $('#tag2').val();
                data.tag3 = $('#tag3').val();
                data.status = $('#status').val();                
                data.status2 = $('#status2').val();
                data.status3 = $('#status3').val();
                
            }
           
        },
        "language": {
                "search": '',
                "searchPlaceholder": "Search Result",
                "oPaginate": {
                "sNext": '<i class="fa fa-angle-double-right" style="color:#1594b9"></i>',
                "sPrevious": '<i class="fa fa-angle-double-left" style="color:#1594b9"></i>'
            }
        },
        "dom": "<'row orange'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",

        "aLengthMenu": [[10, 50, -1], [10, 50, "All"]],
        "iDisplayLength": "10"

    });


    // Add Campaign Clone
    $('#addCampaign').click(function()
    {
        var count = $(".campaign").length;
          $(".campaign:last").clone().attr(`id`,`campaign` + count).insertAfter('.campaign:last');
    })

    // Add Campaign Clone
    $('#addTag').click(function()
    {
        var count = $(".tag").length;
          $(".tag:last").clone().attr(`id`,`tag` + count).insertAfter('.tag:last');
    })
    // Add Status
    $('#addStatus').click(function()
    {
        var count = $(".status").length;
          $(".status:last").clone().attr(`id`,`status` + count).insertAfter('.status:last');
    })




    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });
        
  
    var buttons = new $.fn.dataTable.Buttons(table, {
        //  "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
        // "iDisplayLength": "All",
        buttons: [
        
        { 
            extend: 'excel', 
            text: '<i class="material-icons">file_download</i> Download ' ,
       
        }
        
        ]
    }).container().appendTo($('#buttons'));

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });
    
    
    $('#end_date').on('change', function (){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
         if(end_date != '' )
    {
    table.column(6).search(this.value).draw();
    }
    else
    {
    table.column(6).search(this.value).draw();
    }
        $.post('<?php echo base_url();?>cs/ajax_list', { 
            start: start_date
            , end: end_date
             });
   
    }); 

    //  $('#end_date').on('change', function (){
    //     var start_date = $('#end_date').val();
    // if(start_date != '')
    // {
    // table.column(6).search(this.value).draw();
    // }
    // else
    // {
    // table.column(6).search(this.value).draw();
    // }
    // }); 





    // Custom Filter
    $('#searchStatus').on('change', function () {
            if (!!this.value) {
                table.column(5).search(this.value).draw();
            } else {
                table.column(5).search(this.value).draw();
            }
        } );

    $('#searchCampaign').on('change', function () {
            if (!!this.value) {
                table.column(3).search(this.value).draw();
            } else {
                table.column(3).search(this.value).draw();
            }
        } );
   
    $('#searchTag').on('change', function () {
            if (!!this.value) {
                table.column(4).search(this.value).draw();
            } else {
                table.column(4).search(this.value).draw();
            }
        } );
  $('#searchDatePickerFrom').on('change', function () {
            if (!!this.value) {
                table.column(6).search(this.value).draw();
            } else {
                table.column(6).search(this.value).draw();
            }
        } );    



    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});





function edit_cs(r_id)
{
    save_method = 'update';
    $('#form')[0].reset(); 
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 

    
    $.ajax({
        url : "<?php echo site_url('cs/ajax_edit/')?>/" + r_id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

// console.log(data[1].length);
// console.log(data[0]);



$('#case1').empty();
$('#case2').empty();
$('#profile').empty();
// console.table(data[1]);
// Case History
for (var i = 0; i < data[1].length; i++) {
            var campaign_name = data[1][i].campaign_name;
            var status = data[1][i].status_call;  
            var status_submit = data[1][i].status*1+1;              
            var date_hitted = data[1][i].date_hitted_date;
                if(data[1][i].feedback == null){
                    var feedback_detail = '';
                    var feedback = '- Choose Feedback Category -';
                }else{
            var feedback_detail = data[1][i].feedback_detail;
            var feedback = data[1][i].feedback_detail;

                }
            var tag = data[1][i].tag;
            var retailer_id = data[1][i].retailer_id;
            var kcp_name = data[1][i].kcp_name;
            var phone_number = data[1][i].phone_number;
            var called_by = data[1][i].called_by;

            if(status == 1){
                var status_str= status + 'st';
            }else if(status == 2){
                var status_str = status + 'nd';
            }else{
                var status_str = status + 'rd';
                
            }
                         

            var badges = document.createElement('div');
            badges.className = 'badges';
            badges.innerHTML =
            '<p>Campaign : ' + campaign_name + '</p>' +
            '<p style="color: #59595c;font-weight: 300;">Tag : ' + tag + '</p>' +
                '<div class="date_and_Status">'+
                    '<div><p >Date Hitted : '+ date_hitted +'</p></div>'+
                    '<div><p > Status Call : '+ status_str +'</p></div>'+
                '</div>'+
                '<p style="font-weight:100;color:#979797">Called By : ' + called_by + '</p>' +

                '<input name="campaign_name[]" value="'+campaign_name+'"  id="campaign_name "class="form-control" type="text">'+
                '<input name="status[]" value="'+ status +'" id="status" class="form-control" type="text">'+
                '<input name="tag[]" value="' + tag + '" id="tag" class="form-control" type="text">'+
                '<input name="retailer_id[]" value="' + retailer_id + '" id="retailer_id" class="form-control" type="text">'+
                '<input name="kcp_name[]" value="' +  kcp_name+ '" id="kcp_name" class="form-control" type="text">'+
                '<input name="phone_number[]" value="' +  phone_number+ '"  id="phone_number"class="form-control" type="text">'+
               
                  // show
                  '<div class="selectParentM">'+
                  '<select disabled name="feedback[]"  class="form-control" id="feedback" style="background:#a09b9b;color:#ffffff;height:31px;padding-left:13px;">'+
                '<option value="" selected>'+ feedback + '</option>' +
                // '<input disabled id="feedbackDetail" name="feedback_detail[]" value="'+feedback_detail+ ' " placeholder="Write Feedback Here" class="form-control" type="text">'+
                '</select>'+
                '</div>'
               
                ;

                // if(data[0][i].status == 1){
                //     document.getElementById('case1').appendChild(badges);

                // }else{
                    document.getElementById('case1').appendChild(badges);

                // }
               
            
 }


console.log(data[0]);
// Case New
for (var i = 0; i < data[0].length; i++) {
            var campaign_name = data[0][i].campaign_name;
            var status = data[0][i].status_call;  
            var status_submit = data[0][i].status_call*1+1;   
            
            
            if(status_submit == 1){
                var status_str= status_submit + 'st';
            }else if(status_submit == 2){
                var status_str = status_submit + 'nd';
            }else{
                var status_str = status_submit + 'rd';
                
            }

            var date_hitted = data[0][i].date_hitted;
                if(data[0][i].feedback == null){
                    var feedback_detail = '';
                    var feedback = '- Choose Feedback Category -';
                }else{
            var feedback_detail = data[0][i].feedback_detail;
            var feedback = data[0][i].feedback;

                }
        
            var tag = data[0][i].tag;
            var retailer_id = data[0][i].retailer_id;
            var kcp_name = data[0][i].kcp_name;
            var phone_number = data[0][i].phone_number;
            var badges = document.createElement('div');
            badges.className = 'badges';
            badges.innerHTML =
            '<p>Campaign : ' + campaign_name + '</p>' +
            '<p style="color: #59595c;font-weight: 300;">Tag : ' + tag + '</p>' +
                '<div class="date_and_Status">'+
                    '<div><p style="font-weight:100">Date Hitted : '+ date_hitted +'</p></div>'+
                    '<div><p style="font-weight:100"> Status Call : '+ status_str +'</p></div>'+
                '</div>'+
                '<input name="campaign_name[]" value="'+campaign_name+'"  id="campaign_name "class="form-control" type="text">'+
                '<input name="status_call[]" value="'+ status_submit +'" id="status" class="form-control" type="text">'+
                '<input name="tag[]" value="' + tag + '" id="tag" class="form-control" type="text">'+
                '<input name="retailer_id[]" value="' + retailer_id + '" id="retailer_id" class="form-control" type="text">'+
                '<input name="kcp_name[]" value="' +  kcp_name+ '" id="kcp_name" class="form-control" type="text">'+
                '<input name="phone_number[]" value="' +  phone_number+ '"  id="phone_number"class="form-control" type="text">'+
               
                  // show
                '<div class="selectParentM">'+
                  '<select name="feedback[]"  class="form-control" id="feedbackNew'+ i +'" style="background-color:#1594b9;color:#ffffff;height:31px;padding-left:12px;">'+
                    '<option value="" selected>- Choose Feedback Category -</option>' +
                        '<option value="Need to Follow Up">Need to  Follow Up' + '</option>' +
                        '<option value="Wrong Number">Wrong Number' + '</option>' +
                        '<option value="Need Inspection">Need Inspection' + '</option>' +
                        '<option value="Other">Other' + '</option>' +
                  '</select>'+
                '</div>'+

                '<input id="feedbackDetailNew'+ i +'" name="feedback_detail[]"  placeholder="Write Feedback Here" class="form-control" type="text">'
               
                ;

                 if(data[0][i].status_call < 3){
                    document.getElementById('case2').appendChild(badges);
}else{

}
            
               
            
 }
 $(function () {
    toggleFields(); 
    var j;
    for (j = 0; j < data[0].length; j++) {
    $("#feedbackNew" + j).change(function () {
        toggleFields();
    });
}
});
function toggleFields() {
    let j;
    for (j = 0; j <data[0].length; j++) {
    if ($("#feedbackNew" + j).val() === "Other" )
        $("#feedbackDetailNew" + j).show();
    else
        $("#feedbackDetailNew" + j).hide();
    }
}
 
                $("[name='status[]']").hide();
                $("[name='status_call[]']").hide();
                $("[name='campaign_name[]']").hide();
                $("input[name='retailer_id[]']").hide();
                $("input[name='kcp_name[]']").hide();
                $("input[name='tag[]']").hide();
                $("input[name='phone_number[]']").hide();

 // Profile Data--------------------------------------
                     var profileData = document.createElement('div');
                profileData.className = 'profileData';
                profileData.innerHTML = 
                                        '<p>' + data[0][0].retailer_id +'</p>'+
                                        '<p>' + data[0][0].kcp_name +'</p>'+
                                        '<p>' + data[0][0].address +'</p>'+
                                        '<p>' + data[0][0].phone_number +'</p>'
                                        ;
                document.getElementById('profile').appendChild(profileData);
// Called By--------------------------------------------------
$('#calledBy').empty();

            //    var calledBy = document.createElement('div');
            //    calledBy.className = 'calledBy';
            //    calledBy.innerHTML =
            //                         '<p> Called at : '+ data[0].date_hitted +'</p>'+
            //                         '<p>By : '+ data[0].called_by +'</p>'
            //                          ;
            //      document.getElementById('calledBy').appendChild(calledBy);
               
// // ----------------------------------------------
// $(function () {
//     toggleFields(); 
//     $("#feedback").change(function () {
//         toggleFields();
//     });

// });
// // this toggles the visibility of other server
// function toggleFields() {
//     if ($("#feedback").val() === "feedback1" || $("#feedback").val() === "feedback2")
//         $("#feedbackDetail").show();
//     else
//         $("#feedbackDetail").hide();
// }

            // for ( var i = 0 ; i < data.length ; i++){
                        // // var Rid = JSON.parse(data);
                        // $("#kcp_name").text(data.kcp_name);
                        // $("#retailer_id").text(data.retailer_id);
                        // $("#phone_number").text(data.phone_number);
                        // $("#status").text(data.status);
                        // $("#campaign_name").text(data.campaign_name);
                        // $("#tag").text(data.tag);
                        // $("#date_hitted").text(data.date_hitted);
                        
                        // $('[name="kcp_name"]').val(data.kcp_name);                        
                        // $('[name="phone_number"]').val(data.phone_number);                        
                        // // $('[name="status"]').val(tambah);
                        // $('[name="campaign_name"]').val(data.campaign_name);
                        // $('[name="retailer_id"]').val(data.retailer_id);
                        // $('[name="tag"]').val(data.tag);
                        // $('[name="feedback"]').val(data.feedback);
                        // $('#kcp_name').hide();
                        // $('#tag').hide();
                        // $('#status').hide();                                                
                        $('#modal_form').modal('show'); 
                        $('.modal-title').text('Call Action'); 
            // }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('submiting...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    
    var url = "<?php echo site_url('cs/ajax_update')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('submit'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('submit'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}



</script>

