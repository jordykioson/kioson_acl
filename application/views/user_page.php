<?php 
    $status = 0;
    $toggleOn = base_url()."assets/toggle_button_on.png";
    $toggleOff = base_url()."assets/toggle_button_off.png";
    $deleteIcon = base_url()."assets/delete_icon.png";

    /* Getting access session of the user */
    $level = $this->session->level;
    $writeAccess = false;
    $readAccess = false;

    if ($level == 'Superadmin') {
        $writeAccess = true;
        $readAccess = true;
    }
?>
    
        <div class="add-campaign" align="right">

            <!-- Move to add user page if user has access -->
            <?php if($writeAccess){ ?>
            <label class="add-campaign-btn" onclick="openAddUser('<?php if($writeAccess){echo 'FULL';}else if($readAccess){echo 'READ';}?>')">+ Tambah user</label>
            <?php } ?>
            <!-- Move to add user page if user has access -->

        </div>
        <span style="">
            <table style="width:100%">
                <thead style="background-color:#f2f2f2;">
                    <tr style="height:32px;">
                        <th style="width: 35.8%;padding-left:15px;padding-top:8px;" class="header-table">User Name
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 14.7%;padding-left:12px;padding-top:8px;" class="header-table">Level
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 17%;padding-left:11px;padding-top:8px;" class="header-table">Created Date
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 17.12%;padding-left:12px;padding-top:8px;" class="header-table">Last Login IP
                            <img src="<?php echo base_url(); ?>assets/dropdown_icon.png" alt="dropdown" class="dropdown-icon-" style="visibility:hidden">
                        </th>
                        <th style="width: 15.3%;padding-left:11px;padding-right:11px;padding-top:8px;" class="header-table"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($response_user)){ ?>
                    <?php foreach($response_user as $key_user) { ?>
                    <tr style="font-size: 12px;font-family:'Roboto';">
                        <td style="padding-left:15px;padding-top:12px;padding-bottom:10px">
                            <a href="javascript:viewDetail('<?php echo $key_user->user_id;?>')" style="color: #1594b9;">
                                <span id="name_<?php echo $key_user->user_id;?>"><?php echo $key_user->name; ?></span>
                            </a>
                        </td>
                        <td style="padding-left:11px;padding-top:12px;padding-bottom:10px">
                            <span id="keterangan_<?php echo $key_user->user_id;?>"><?php echo $key_user->keterangan; ?></span>
                        </td>
                        <td style="padding-left:12px;padding-top:12px;padding-bottom:10px" id="id_<?php echo $key_user->user_id;?>">
                            <span id="created_date_<?php echo $key_user->user_id;?>"><?php echo $key_user->created_time; ?></span>
                        </td>
                        <td style="padding-left:12px;padding-top:12px;padding-bottom:10px">
                            <span id="last_login_<?php echo $key_user->user_id;?>"><?php echo $key_user->last_login_ip; ?></span>
                        </td>
                        <td style="padding-left:11px;padding-top:12px;padding-bottom:10px;">
                            <a href="<?php echo base_url().'Main/deleteUser/'.$key_user->user_id;?>" id="delete_<?php echo $key_user->user_id ?>" style="color:red;">
                                <img id="delete-btn" src="<?php echo $deleteIcon; ?>" alt="delete_button" style="width:12px;height:15px;margin-right:14px;">Delete
                            </a>
                        </td>
                        <div style="display:none" id="id_<?php echo $key_user->user_id;?>"><?php echo $key_user->user_id;?></div>
                        <div style="display:none" id="query_<?php echo $key_user->user_id;?>"><?php echo $key_user->query;?></div>
                        <div style="display:none" id="description_<?php echo $key_user->user_id;?>"><?php echo $key_user->description;?></div>
                        <div style="display:none" id="requested_by_<?php echo $key_user->user_id;?>"><?php echo $key_user->requested_by;?></div>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div> 

        <!-- Modal Box add User -->
        <div class="modal-campaign" id="modal-user" style="display:none">
            <div class="modal-campaign_content">
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeAdduser()">
                <div align="center" style="margin-bottom:20px;">
                    <label for="" style="color:#f7981d;font-size:14px;font-family:'Roboto';font-weight:500;margin-bottom:0px;line-height:16px;">Add User</label>
                </div>
                <div id="error_message_campaign" style="font-size:12px;color:red;text-align:center;padding-bottom:10px;"></div>

                <form action="<?php echo base_url();?>Main/addUser/" method="post" id="form_add_user"> 

                    <!-- Fullname -->
                    <div style="float:left;margin-right:20px;">
                        <strong for="" class="campaign-style">Fullname</strong>
                        <input type="text" name="fullname" id="fullname" placeholder="Write Fullname Here" class="input-campaign-style" style="width:450px;">
                    </div>
                    <div style="clear:both;"></div>

                    <!-- Username -->
                    <div style="float:left;margin-right:20px;">
                        <strong for="" class="campaign-style">Username</strong>
                        <input type="text" name="username" id="username" placeholder="Write Username Here" class="input-campaign-style" style="width:450px;">
                    </div>
                    <div style="clear:both;"></div>

                    <!-- Password -->
                    <div style="float:left;margin-top:8px;">
                        <strong for="" class="campaign-style">Password</strong>
                        <input type="text" id="password" name="password" placeholder="Write Password Here" class="input-campaign-style" style="width:190px;margin-right:10px;" >
                    </div>

                    <!-- for clearing after using style float -->
                    <div style="clear:both;"></div>
                    <!-- for clearing after using style float -->

                    <div style="float:left;margin-top:8px;">
                        <strong for="" class="campaign-style">Level</strong>
                        <select name="level" id="level" class="input-campaign-style" style="width:190px;margin-right:10px;">
                            <option value="69">Standard User</option>
                            <option value="79">IT</option>
                            <option value="89">Admin</option>
                            <option value="99">Superadmin</option>
                        </select>
                    </div>

                    <!-- for clearing after using style float -->
                    <div style="clear:both;"></div>
                    <!-- for clearing after using style float -->
                    
                    <div style="width:100%" align="right" class="btn-submit_">
                        <input type="submit" value="Submit" class="btn-campaign-submit-modal" style="cursor:pointer" onclick="return validation()">
                    </div>
                </form>

            </div>
        </div>

        <!-- Modal Box View Detail -->
        <div class="modal-tags-view" style="display:none">
            <div class="modal-tags_content">
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">
                <div align="center" style="margin-bottom:20px;">
                    <label for="" style="color:#f7981d;font-size:14px;font-family:'Roboto';font-weight:500;margin-bottom:0px;line-height:16px;">User Details</label>
                </div>
                <div id="error_message_tag" style="text-align:center;color:red;font-size:12px;"></div>
                <form action="<?php echo base_url();?>Main/updateAccess/" method="post" id="form_add_tag">                    
                    <table>
                    <thead>
                    <tr>
                            <th style="width: 70%;padding-left:15px;padding-top:8px;" class="header-table">Path</th>
                            <th style="width: 30%;padding-left:15px;padding-top:8px;" class="header-table">Access</th>
                    </tr>
                    </thead>
                    <tbody id="user-view-detail">
                    
                    </tbody>
                    </table>
                    <!-- for clearing after using style float -->
                    <div style="clear:both;"></div>
                    <!-- for clearing after using style float -->
                    
                    <div style="width:100%" align="right" class="btn-submit_">
                        <input type="submit" value="Submit" class="btn-campaign-submit-modal" style="cursor:pointer">
                    </div>
                </form>
            </div>
        </div>


<script>
function viewDetail(id) {
    $.post("<?php echo base_url();?>Main/userLibrary", { dataDetail: id }, function(data) {
        let objek = JSON.parse(data);
        
            $('.modal-tags-view').fadeIn('slow');

            for(let i = 0; i < objek.length; i++) {
                // alert(objek[i].path);
                if(objek[i].access == 'READ'){
                    var a = 'READ';
                    var b = 'FULL';
                    var c = 'BLOCK';
                }else if(objek[i].access == 'FULL'){
                    var a = 'FULL';
                    var b = 'READ';
                    var c = 'BLOCK';
                }else{
                    var a = 'BLOCK';
                    var b = 'READ';
                    var c = 'FULL';
                }
                $('#user-view-detail').append(`
                <tr>
                    <td style="width: 50%;padding-left:15px;padding-top:8px;" >${objek[i].nama}</td>
                    <td style="width: 50%;padding-left:15px;padding-top:8px;" name="access" value="${objek[i].access}" >
                    <select name="access[${objek[i].relation_id}]"  class="form-control" id="feedback">
                        <option value="${a}" selected>${a}</option>
                        <option value="${b}" >${b}</option>
                        <option value="${c}" >${c}</option>
                    </select>
                    </td>
                </tr>`);
            }

            // $('#path-val_view').val(path);
            // $('#access-val_view').val(access);
            // $('#action-val_view').val(action);
        });
    }

    function closeModal() {
        $('.modal-tags-view').fadeOut('slow');
        $('#user-view-detail').children().remove(); // Deleting html tag under id=user-view-detail 
        $('#error_message_tag').html('');
    }

    function closeAdduser() {
        $('#modal-user').fadeOut('slow');
        $('#error_message_campaign').html('');
    }

    function openAddUser(access) {
        if(access === 'FULL') {
            $('#modal-user').fadeIn('slow');
        }else if(access === 'READ'){
            alert('You have no authorize on this page');
        }
    }

    function validation(){
        let fullname = $('#fullname').val();
        let username = $('#username').val();
        let password = $('#password').val();
        let level = $('#level').val();
        let errorCode = false;

        $('#fullname').css('border','none');
        $('#username').css('border','none');
        $('#password').css('border','none');
        $('#level').css('border','none');
        $('#error_message_campaign').html('');
        $('.btn-campaign-submit-modal').css('box-shadow','none');

        if (fullname === "") {
            $('#error_message_campaign').html('Please insert Full Name');
            $('#fullname').css('border','1px solid red');
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        if (username === "") {
            $('#error_message_campaign').html('Please insert Username');
            $('#username').css('border','1px solid red');
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        if (password === "") {
            $('#error_message_campaign').html('Please insert Password');
            $('#password').css('border','1px solid red');
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        if (level === "") {
            $('#error_message_campaign').html('Please insert Level');
            $('#level').css('border','1px solid red');
            $('.btn-campaign-submit-modal').css('box-shadow','0px 4px 10px grey');
            errorCode = true;
            return false;
        }

        if(!errorCode) {
            return true;
        }

    }
</script>