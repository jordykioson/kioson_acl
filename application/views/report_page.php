
    <link href="<?php echo base_url('assets/d/bootstrap/css/bootstrap4.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/d/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
    <!-- <link href="<?php echo base_url('assets/d/datatables/css/jquey.dataTables.css')?>" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/d/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto Condensed' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <link href="<?php echo base_url('assets/d/report_style.css')?>" rel='stylesheet' type="text/css" >
  
    <style>
@media only screen 
and (min-width: 1824px)
 {
    
  #buttons{
    margin-left:46% !important;
}
#table_filter{
    margin-left:28%;
}
.dt-buttons{
    text-align:center;
}
#KCPWho{
    margin-left:37px !important;
}

 }

    .orange{
         background-color:#f7981d;
         height:43px;
     }
     .col-sm-12{
    margin-top: -6px;

}
.table-striped>tbody>tr:nth-of-type(odd){
    background-color:white;
}
     .material-icons{
         font-size:17px !important;
         /* margin-left:66%; */
     }
    
         div.dataTables_wrapper div.dataTables_info {
            margin-top:10px;
             color:black;
}
        div.dataTables_wrapper div.dataTables_paginate {
 
 text-align: left !important;
}
.modal-title{
    margin-left: 44%;

}
  
    div.dataTables_wrapper div.dataTables_filter label{
        margin-left: 70% !important;
        position: inherit;
         z-index: 1;
    }
    .form-inline{
        display: inline !important;
    }
        div.dataTables_length label{
            /* display:none; */

        }
        div.dataTables_filter label{
            margin-top:7px;
            color:white;
            
        }
        #table_filter{
            position: inherit;
        }
        .wwFind{
            display: grid;
            grid-template-columns: 40% 40% ;
            grid-auto-rows: 124px;

            grid-gap: 3px;
        }
        .wwFind > div {
            padding: 1em;
        }
        .wwFind > div:nth-child(odd){
        }




        p {
            font-family: 'Roboto Condensed';font-size: 18px;
            
            color: #59595c;
        }

        hr {
            margin-top: -3px;  
            border-top: 2px solid #5555551c; 
            width: 93%;
        }

        .modal-content{
            width: 981px;
        }


        pala {
        align-items: center;
        display:flex;
        align-items: center;

        background-color: #f7981d;  
        /* grid-column: span 12; */
        color: #fff;
        font-family: 'Roboto Condensed';font-size: 18px;
        font-weight: bold;
        }



        .modal-title{
        font-family: 'Roboto Condensed';font-size: 24px;
        font-weight: bold;
        color: #f7981d;
        text-align: center;
        }

        .wrapper3{
            display: grid;
            grid-template-columns: 10% 90%;
            grid-gap: 0px;
            grid-auto-rows: minmax(100px,auto);
            /* justify-items:sceth ; dari kiri tengah kanan
            align-items:sceth ; dari atas tengah bawah*/ 
        }
        .wrapper3 > div{
            background: transparent;
            padding: 1em;
        }
        .wrapper3 > div:nth-child(odd){
            background:transparent;
        }
        .profileName{
            /* align self untuk styling per masing2 berbeda2 */
            justify-self: start;
            font-family: 'Roboto Condensed';font-size: 18px;
            font-weight: bold;
            color: #59595c;
        }
        .profileData{
            /* align-self: end; */
            justify-self: start;
            font-family: 'Roboto Condensed';font-size: 18px;
            
            color: #59595c;
        }

        .wrapper3 p {
            margin: 0 0 3px;
            font-size: small;

        }

        .column {
            display:grid;
            float: left;
            width: 50%;
            padding: 10px;
            grid-auto-rows: minmax(100px,auto);

            /* Should be removed. Only for demonstration */
        }
        .wrapper2Box{
            display: grid;
            grid-template-columns: 100%;
            /* grid-column-gap: 1em;
            grid-row-gap: 1em; 
                //same as bellow
            */
            grid-gap: 3px;
        }

        .classTh{
            text-align:center;
            border-radius: 5px;
            height: 55px;    
        }
        .classTh p {
            font-size: 15px;
        }

        .classTitle{
            background: #f2f2f2;
            text-align:center;
            border-radius: 5px;
            height: 350px;
            overflow-y: scroll;
        }
        .classTitle p {
            font-size: 15px;
        }
        /* .dropdownCase{
            display:grid;
            grid-template-columns:100%;
            grid-auto-rows:minmax(70px, auto) ;
            grid-gap: 3px;
            padding:1em;
        } */

        /* .dropdownCase > div {
            border: #eee 1px solid;
        
            padding:1em;
            border-radius: 5px;
            font-family: 'Roboto Condensed';font-size: 11px;
            font-weight: medium;
            text-align:left;
        } */
        .boxCase p {
                margin: 0 0 3px;
                font-size: 11px;

        }

        .wrapperCalledBy{
            display: grid;
            grid-template-columns: 50% 50%;
            grid-row-gap: 1em; 
            align-items:end;
            margin-top: 10px;

            
        }
        .wrapperCalledBy p{
            text-align:center;
            color:#aeaeae;
        font-size: 11px;
            margin: 0 0 0px;
        }
            /* Table Styling down bellow */
            .table>thead:first-child>tr:first-child>th{
                background-color: #f2f2f2;
        }
       

        .wwFind{
                display: grid;
                grid-template-columns: 7% 40% 40% ;
                /* grid-column-gap: 1em;
                grid-row-gap: 1em; 
                    //same as bellow
                */
                grid-gap: 3px;
                padding:1em;
                margin-bottom: 0px;
            }
            .wwFind > div {
                padding: 1em;

            }
            .wwFind > div:nth-child(odd){
            }

        .filterStatus{
            display: grid;
            grid-template-columns:repeat(2, 1fr);
            grid-auto-rows: 118px;
            grid-gap: 0px;
            margin-top: -6px;
            margin-left: -70px;


        }
        .filterStatus > div{
            padding:1em;
        }
         .andHaveStatus{
            align-items:start;
        }
        .fromToWhen{
            display: grid;
                grid-template-columns:repeat(2, 1fr);
                grid-auto-rows: 70px;
                grid-gap: 5px;
        }    
        .ntainer{
            display: grid;
            grid-template-columns:100%;
            grid-gap: 3px;
        }

        .boxCase:hover{
            background-color:#fbfbfb;
        }




        a:hover {
        text-decoration: none;
        }

       .table-bordered td{
    border:none;
 }

        #findFilter{
            padding-top: 0px;
            background-color:#1594b9;
            width:100px;
            height:22px;
        }

        .nestedTitle{
            display: grid;
            grid-template-columns:repeat(7, 1fr);
            grid-auto-rows: 70px;
            grid-gap: 5px;
        }
        .nestedTitle > div{
            margin-top:29px;
        }
        
        .nestedTitle p{
            font-weight:bold;
        }

        .nested{
            display: grid;
    grid-template-columns: repeat(7, 1fr);
    grid-auto-rows: 34px;
    grid-gap: 5px;
    margin-left: 10px;
    margin-top: 16px;
        }
      
        .nested > div{
            padding:1em;
        }
        /* .nested p{
            font-weight:bold;
        } */

        .modal-dialo{
            width:900px;
            margin:30px auto;
            }

        .dataTables_length{
            float:left;
            color:white;
            margin-top: 4px;

        }
         .reportTitle{

    padding: 1em;
    font-family: 'Roboto Condensed';
    font-size: 18px;
    font-weight: bold;
    color: #fff;

}
.form-control{
    border: transparent;

}

.dt-buttons{
    /* text-align:center; */
    /* background-color: #f7981d; */
    height: 45px;
    position: absolute;
    width: 1518px;
    margin-top: 10px;
    padding: 7px;
    z-index:1; 
    /* pointer-events: none;      */
}
.dt-buttons a  {
    color : white;
    /* z-index:-1;  */

}
#buttons{
    margin-left:78%;
}
/* .material-icons{
    font-size: 18px;
} */

input[type=search] {
        background-image: url('data:image/svg+xml;utf8,<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.1" id="spyglass" viewBox="0 0 22 22" height="22" width="22"><path d="m 10.011718,5.0898489 c -2.7000005,0 -4.9003915,2.2004 -4.9003915,4.9003996 0,2.6999995 2.200391,4.8983995 4.9003915,4.8983995 1.154374,0 2.204251,-0.4184 3.042969,-1.0898 l 3.207031,3.1113 0.626953,-0.6465 -3.185547,-3.0879 c 0.743242,-0.8594 1.208984,-1.964 1.208984,-3.1854995 0,-2.6999996 -2.20039,-4.9003996 -4.90039,-4.9003996 z m 0,1 c 2.2,0 3.90039,1.7004 3.90039,3.9003996 0,2.1999995 -1.70039,3.8983995 -3.90039,3.8983995 -2.2000005,0 -3.9003915,-1.6984 -3.9003915,-3.8983995 0,-2.1999996 1.700391,-3.9003996 3.9003915,-3.9003996 z" style="fill:#808080"/></svg>');
        background-repeat: no-repeat;
        background-color: #fff;
        background-position: 0px 3px !important;
        padding-left: 22px;
}
#table th{
    color:#59595c;
    font-family: 'Roboto Condensed';
    font-size: 14px;
    height: 29px;

}

#campaign1{
    display:none;
}
#tag1{
    display:none;
}
#status1{
    display:none;
}
.form-control{
    -webkit-box-shadow: none;

}

.selectParentC{
    width:100%;
    overflow:hidden;
   /* background-color:#f2f2f2; */
   
}

.selectParentC select{
    width:100%;

    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    padding: 2px 2px 2px 2px;
    border: none;
    background:  url("<?php echo base_url();?>assets/dropdown_icon_black.png") no-repeat 96% center;
}


/* // Select test panah */
.selectParent{
    width:185px;
    overflow:hidden;
   /* background-color:#f2f2f2; */
   
}

.selectParent select{
    width: 172px;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    padding: 2px 2px 2px 2px;
    border: none;
    background:  url("<?php echo base_url();?>assets/dropdown_icon_black.png") no-repeat 146px center;
}
.pagination{
    border:0px solid transparent !important;
}
.reportColor2{
    color:red !important;
}

.StatusThTitle{
    padding-right:35px;
}
div.dataTables_wrapper div.dataTables_length select {

margin-left: 7px;
margin-right: 7px;
}
    </style>
    </head> 
<body>

      <div class="headFilterData" style="height:54px;background-color: #f7981d;margin-top: -9px">
            <div><h5 class="reportTitle" style="font-weight:100;">Filter Data Report</h5></div>
        </div>
    <div class="wwFind">
        <div id="KCPWho"style="color:#59595c;margin-left: 11px;margin-top: 2px;
">
            KCP Who
        </div>
        <div class="selectParentC">
             
                <span id="close-tag-icon1" onclick="closeTag()" style="display:none;cursor:pointer">
                                <img src="<?php echo base_url();?>assets/close_tag_icon.png" alt="close_tag" style="width:13px;vertical-align:baseline">
                            </span>
                        <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="addCampaign"  style="width:17px;position:absolute; margin-left: 38% ;margin-top: -9px;" class="add-icon"> 
                                <?php echo $form_campaign; ?>
                        <p></p>
        <!-- <p style="text-align:center;">with tagged</p>
               

               <?php echo $form_tag; ?>
       <p></p>
       <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="addTag"  style="width:17px;position:absolute;margin-left:18%;margin-top:-4px;" class="add-icon">  -->

        
        
        </div>

        <div>
            <div class="filterStatus ">
                <div class="andHaveStatus" style="justify-self: end;    margin-top: -6px;">and have status </div>
                <div class="selectParent" style="
                margin-top: -3px;padding-left: 1px;
                ">  
                  
                     <img src="<?php echo base_url();?>assets/add_icon.png" alt="add_icon" id="addStatus"  style="width:17px;position:absolute; margin-left: 186px; margin-top: -16px;" class="add-icon"> 

                        <?php echo $form_status; ?>
                        <p></p>
                </div>
                
            </div>
            <!-- <div class="selectParent">
                <select>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>           
                </select>  
            </div> -->
        </div>  
        
        <div class="fromText" style="justify-self: end;">
             From
        </div>
        <div>
             <div class="input-daterange">
                <div class="fromToWhen">
                    <div>
                        <input name="date_start" id="start_date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" style="background:#f2f2f2;color:#59595c;height:31px;width:90%;margin-left: -2px;    border-radius: 4px;margin-top:-4px;">
                    </div>
                    <div>
                    <p style="margin-left: -18px;">until</p>
                    <input name="date_end" id="end_date" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" style="background:#f2f2f2;color:#59595c;height:31px;border-radius: 4px;
   margin-top: -41px;
    margin-left: 32px;width:90%;">
                    </div>
                    <div>
                        <button type="button" id="btn-filter" class="btn btn-primary" style="width:134px;margin-top: -10px;" >Find</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    
    <!-- <div class="ntainer"> -->
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    
            <thead>

                <tr>
                <p id="buttons">   </p>
                    <th style="color:#59595c;font-family: 'Roboto Condensed';font-size: 14px;">KCP ID</th>                                        
                    <th>Name</th>
                    <th>Campaign</th>
                    <th>Tag</th>
                    <th>Status </th>
                    <th>Date Hitted</th>
                    <th>Phone number</th>
                    <th style="width:125px;">    </th>
                   
                </tr>
            </thead>
            <tbody>
            </tbody>

           
        </table>
    <!-- </div> -->


    </div>




        <div style="color:red;"class="dataTables_info" id="table_info" role="status" aria-live="polite"></div>




    <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialo">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title">Details</p>
                <!-- <button type="button" class="close" style="margin-top: -21px;"data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <img src="<?php echo base_url();?>/assets/close_icon.png" alt="close_btn" class="close-modal" onclick="closeModal()">

            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="retailer_id"/>
                    <div class="wrapper3">
                        <div class="profileName">
                            <p>KCP ID</p>
                            <p>Name</p>
                            <p>Address</p>
                            <p>Phone</p>  
                        </div>
                        <div id="profile" style="margin-top: 2px;">
                            <!--  -->
                        </div>
                        <!-- <div>Box</div> -->
                    </div>  

                    <div class="wrapper2Box">
                        <!-- Case 1 -->
                        <div class="classTh">
                            <div class="titleBox">
                                    <div class="nestedTitle">
                                        <div><p>Campaign</p></div>
                                        <div><p>Push Notification</p></div>
                                        <div><p>SMS</p></div>
                                        <div><p>Call 1</p></div>
                                        <div><p>Call 2</p></div>
                                        <div><p>Call 3<p></div>
                                        <div><p class="StatusThTitle">Status<p></div>

                                    </div>
                                
                                
                            </div>
                        </div>
                        <div class="classTitle">
                            
                            <!-- <div class="dropdownCase"> -->
                                <!-- <div class="boxCase">
                                   <p>Campaign : <span id="campaign_name"></span></p> 
                                    <p>Tag : <span id="tag"></span></p>
                                    <p>Call Action : <span id="status"></span></p>
                                    <p>Date Hitted : <span id="date_hitted"></span></p>

                                    <!-- <select name="feedback" class="form-control" id="sel1" style="background:#59595c;color:#ffffff;height:31px;">
                                        <option value="" disabled selected>- Choose Feedback -</option>
                                        <option value="needFollowUp">Need Follow Up</option>
                                        <option value="noAnswer">No Answer</option>
                                        <option value="ok">OK</option>
                                    </select> -->
                                  
                                    <div id="detailReporting" \>
                                             <div ></div>                                   
                                    </div>
                                
                            <!-- </div> -->
                                        
                        </div>
                        
                    
                   
                    <div class="form-body">
                        <!-- <div class="form-group">
                            <label class="control-label col-md-3">feedback</label>
                            <div class="col-md-9">
                                <select name="feedback" class="form-control">
                                    <option value="">--Select Gender--</option>
                                    <option value="followUp">Follow Up</option>
                                    <option value="noAnswer">No answer</option>
                                    <option value="OK">Ok</option>
                                </select>                               
                                 <span class="help-block"></span>
                            </div>
                        </div> -->
                    </div> 
                    <!-- <div class="form-group">
                            <label class="control-label col-md-3">kcp naem</label>
                            <div class="col-md-9">
                                <input name="kcp_name" placeholder="status" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                    </div> -->
                </form>
            </div>
              
          
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->







<link href="<?php echo base_url(); ?>assets/d/w3.css" rel="stylesheet">
 <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>  -->
 
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> 
 <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> -->

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css"> -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>
<!-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.excel.min.js"></script>   -->
 <!-- <script src="<?php echo base_url('assets/d/jquery/jquery-2.1.4.min.js')?>"></script> -->
 <!-- <script src="<?php echo base_url('assets/d/datatables/js/jquery.dataTables.min.js')?>"></script>  -->

 <script src="<?php echo base_url('assets/d/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/d/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/d/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
	    <!-- <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet"> -->


<script type="text/javascript">

function closeModal() {
        $('#modal_form').modal('hide');
        $('#error_message_tag').html('');
    }


var save_method; 
var table;

$(document).ready(function() {

    
    table = $('#table').DataTable({ 

        "processing": true, 
        "serverSide": true, 
        "order": [], 
        "columnDefs": [
    { "orderable": false, "targets": 7}
  ],
        
        "ajax": {
            "url": "<?php echo site_url('report/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.campaign = $('#campaign').val();                
                data.campaign2 = $('#campaign2').val();
                data.campaign3 = $('#campaign3').val();
                data.tag = $('#tag').val();                
                data.tag2 = $('#tag2').val();
                data.tag3 = $('#tag3').val();
                data.status = $('#status').val();                
                data.status2 = $('#status2').val();
                data.status3 = $('#status3').val();

                // 
            }
        },
        "language": {
                "search": '',
                "searchPlaceholder": "Search Result",
                "oPaginate": {
                "sNext": '<i class="fa fa-angle-double-right" style="color:#1594b9"></i>',
                "sPrevious": '<i class="fa fa-angle-double-left" style="color:#1594b9"></i>'
            }
        },
        "dom": "<'row orange'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",

        // "oLanguage": {
        //     "oPaginate": {
        //         "sNext": '<i class="fa fa-chevron-right" ></i>',
        //         "sPrevious": '<i class="fa fa-chevron-left" ></i>'
        //     }
        // }
        "aLengthMenu": [[10, 50, -1], [10, 50, "All"]],
        "iDisplayLength": "10"


    });
    // Add Clone
    $('#addCampaign').click(function()
    {
        var count = $(".campaign").length;

          $(".campaign:last").clone().attr(`id`,`campaign` + count).insertAfter('.campaign:last');
    })

     // Add Campaign Clone
     $('#addTag').click(function()
    {
        var count = $(".tag").length;

          $(".tag:last").clone().attr(`id`,`tag` + count).insertAfter('.tag:last');
    })

    // Add Status
    $('#addStatus').click(function()
    {
        var count = $(".status").length;
          $(".status:last").clone().attr(`id`,`status` + count).insertAfter('.status:last');
    })


    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });


        var buttons = new $.fn.dataTable.Buttons(table, {
        buttons: [
        
        { extend: 'excel', text: '<i class="material-icons">file_download</i> Download ' }
        
        ]
    }).container().appendTo($('#buttons'));





         //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });
    
    
    $('#end_date').on('change', function (){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
         if(end_date != '' )
    {
    table.column(6).search(this.value).draw();
    }
    else
    {
    table.column(6).search(this.value).draw();
    }
        $.post('<?php echo base_url();?>cs/ajax_list', { 
            start: start_date
            , end: end_date
             });
   
    }); 




    $('#searchStatus').on('change', function () {
            if (!!this.value) {
                table.column(5).search(this.value).draw();
            } else {
                table.column(5).search(this.value).draw();
            }
        } );

    $('#searchTag').on('change', function () {
            if (!!this.value) {
                table.column(4).search(this.value).draw();
            } else {
                table.column(4).search(this.value).draw();
            }
        } );    




    $('#searchCampaign').on('change', function () {
            if (!!this.value) {
                table.column(3).search(this.value).draw();
            } else {
                table.column(3).search(this.value).draw();
            }
        } );
   

     
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});





function edit_report(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 

    $.ajax({
        url : "<?php echo site_url('report/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
// //-----------------------------------------------
console.table(data[1]);
$('#detailReporting').empty();
$('#profile').empty();

for (var i = 0; i < data[0].length; i++) {
   


                var campaign = data[0][i].campaign_name;

            if(data[0][i].Call1 == null){
                var Call1_date = '-';
            }else{
                var Call1_date_int = data[0][i].Call1;
                var Call1_date_str = "" + Call1_date_int;
                var Call1_date = Call1_date_str.replace( new RegExp("^(\\d{4})(\\d{2})(\\d{2})", "gm"),"$1/$2/$3");            
            }
              if(data[0][i].Call2 == null){
                var Call2_date = '-';
            }else{
                var Call2_date_int = data[0][i].Call2;
                var Call2_date_str = "" + Call2_date_int;
                var Call2_date = Call2_date_str.replace( new RegExp("^(\\d{4})(\\d{2})(\\d{2})", "gm"),"$1/$2/$3");
            }
            if(data[0][i].Call3 == null){
                var Call3_date = '-';
            }else{
                var Call3_date_int = data[0][i].Call3;
                var Call3_date_str = "" + Call3_date_int;
                var Call3_date = Call3_date_str.replace( new RegExp("^(\\d{4})(\\d{2})(\\d{2})", "gm"),"$1/$2/$3");
            }

            if(data[0][i].Push_Notification == null){
                var push_date = '-';
            }else{

        var push_date_int = data[0][i].Push_Notification;
        var push_date_str = "" + push_date_int;
        var push_date = push_date_str.replace( new RegExp("^(\\d{4})(\\d{2})(\\d{2})", "gm"),"$1/$2/$3");
            
            }
            if(data[0][i].SMS == null){
                var SMS_date = '-';
            }else{
        var SMS_date_int = data[0][i].SMS;
        var SMS_date_str = "" + SMS_date_int;
        var SMS_date = SMS_date_str.replace( new RegExp("^(\\d{4})(\\d{2})(\\d{2})", "gm"),"$1/$2/$3");   
                 }

                 if(data[1][i].status == 1){
                     var status = "solved";
                 }else if(data[1][i].status == 2){
                 var status = "Need Inspection";
                 }else {
                     var status = "on progress";
                 }
            var badges = document.createElement('div');
            badges.className = 'nested';
            badges.innerHTML =
                '<p class="reportColor'+ data[1][i].status+'" style="color:#1594b9;font-weight:bold;">' + campaign + '</p>' +
                '<p  class="reportColor'+ data[1][i].status+'">' + push_date + '</p>' +
                '<p  class="reportColor'+ data[1][i].status+'">' + SMS_date + '</p>' +
                '<p  class="reportColor'+ data[1][i].status+'">' + Call1_date + '</p>' +
                '<p  class="reportColor'+ data[1][i].status+'">' + Call2_date + '</p>' +
                '<p  class="reportColor'+ data[1][i].status+'">' + Call3_date + '</p>' +
                '<p class="reportColor'+ data[1][i].status+'">' + status + '</p>' 

                ;
            document.getElementById('detailReporting').appendChild(badges);
 }
 let profileData = document.createElement('div');
profileData.className = 'profileData';
profileData.innerHTML = 
'<p>' + data[0][0].retailer_id +'</p>'+
'<p>' + data[0][0].kcp_name +'</p>'+
'<p>' + data[0][0].address +'</p>'+
'<p>' + data[0][0].phone_number +'</p>'
;
document.getElementById('profile').appendChild(profileData);
// // ----------------------------------------------
            // console.log(test);



            // console.log(data);
            // for ( var i = 0 ; i < data.length ; i++){
                        // var Rid = JSON.parse(data);
                        // $("#kcp_name").text(data.kcp_name);
                        // $("#retailer_id").text(data.retailer_id);
                        // $("#phone_number").text(data.phone_number);
                        // // $("#status").text(data.status);
                        // $("#campaign_name").text(data.campaign_name);
                        // $("#tag").text(data.tag);
                        // $("#date_hitted").text(data.data_hitted);
                        
                        
                        // $('[name="retailer_id"]').val(data.retailer_id);
                        // // $('[name="kcp_name"]').val(data.kcp_name);
                        // $('[name="status"]').val(data.status);
                        // $('[name="feedback"]').val(data.feedback);
                        $('#modal_form').modal('show'); // show bootstrap modal when compvare loaded
                        $('.modal-title').text('Details'); // Set title to Bootstrap modal title
            // }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); 
}

function save()
{
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true);  
    
    var url = "<?php echo site_url('report/ajax_update')?>";
    
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false);  

        }
    });
}



</script>

