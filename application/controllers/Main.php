<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		if($this->session->name) {
			$this->onBoarding();
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function fdsAuth(){
		$id = $this->input->get('id');
		$this->load->model('Access_model' , 'access');
		$data = $this->access->fdsAccess($id);
		echo $data;
		
	}

	public function onLogin(){
		$this->load->model('Login_model','login');
		$data = $this->login->authenticate();
		$response = json_decode($data);
		
		if($response->status == 'Gagal'){
			$var['response'] = $response->status;
			$this->load->view('login_page', $var);
		}else if($response->status == 'Sukses') {
			redirect(base_url().'Main/onBoarding');
		}
	}

	public function onBoarding() {
		if($this->session->name) {
			$this->load->view('onBoarding_page');
		}else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function watchTower() {
		if($this->session->name) {
			$var['page_selected'] = 'watchTower';
			$this->load->view('home_page', $var);
		}else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function addUser(){
		if($this->session->name) {
			$this->load->model('User_model' , 'user');
			$response = $this->user->create();
			if($response == 'sukses'){
				redirect(base_url().'Main/userLibrary');
			}
		}else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function updateAccess(){
		$post = $this->input->post('access');
		$data = array();
		foreach ($post as $key => $value) {
			$data[] = array(
				'relation_id' => $key,
				'access' => $value
			); 
		}
		$this->load->model('Access_model','access');
		$response = $this->access->edit($data);
		if($response == 'sukses'){
			redirect(base_url().'Main/userLibrary');
		}
	}

	public function deleteUser($id) {
		if($this->session->name) {
			$this->load->model('User_model','user');
			$response = $this->user->delete($id);
			redirect(base_url().'Main/userLibrary');		
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}


	public function userLibrary(){
		if($this->session->level == 'Superadmin') {
			$this->load->model('User_model','user');
			$userData = $this->user->getDetail();
			$response = json_decode($userData);
			// print_r($response->data);
			
			/* Assigning data to view using $var */
			if(isset($response)) {
				$var['response_user'] = $response->data;
				$var['page_selected'] = 'user';

				/* Condition if user detail has been clicked */
				if($detailId = $this->input->post('dataDetail')) {

					$this->load->model('Access_model','access');
					$add = $this->access->firstAdd($detailId);
					if ($add == 'sukses') {
						$detailData = $this->access->getAccess($detailId);
						echo json_encode($detailData);	// Sent to user page javascript function viewDetail()
					}	
				}/* Go to campage page normally */								
				else{
					$this->load->view('home_page', $var);
				}
			}	
		}else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function campaign(){
		if($this->session->name) {
			/* Getting acces session */
			$access = array();
			foreach($this->session->access as $key) {
				if($key['path'] == 'crm_campaign') {
					$access = $key;
				}
			}

			/* If user has access to campaign */
			if(isset($access['path']) && $access['path'] == 'crm_campaign') {
				/* Fetching data from model to database */
				$this->load->model('Campaign_model','campaign');
				$this->load->model('Tag_model','tag');
				$this->load->model('User_model','user');
				$data = $this->campaign->getAll();
				$tagData = $this->tag->getAll();
				$userData = $this->user->getAll();
				$response = json_decode($data);
				$responseTag = json_decode($tagData);
				$responseUser = json_decode($userData);
				
				/* Assigning data to view using $var */
				if(isset($response)) {
					$var['response'] = $response->data;
				}
				$var['page_selected'] = 'campaign';
				$var['response_tag'] = $responseTag->data;
				$var['response_user'] = $responseUser->data;

				/* Condition if campaign detail has been clicked */
				if($this->input->post('dataDetail')) {
					$detailId = $this->input->post('dataDetail');

					$this->load->model('Sequences_model','sequences');
					$sequenceData = $this->sequences->getDetails($detailId);
					$detailData = $this->campaign->getCampaignDetails($detailId);
					
					$arrayTotal = array();
					$arrayTotal[] = $sequenceData; 
					$arrayTotal[] = $detailData;
					echo json_encode($arrayTotal);	// Sent to campaign page javascript function viewDetail()

				/* Error if campaign name already exist when add new campaign */
				}else if(isset($_REQUEST['error']) && $_REQUEST['error'] == '1'){
					$var['error_name'] = '1';
					$this->load->view('home_page', $var);

				/* Go to campage page normally */								
				}else {
					$this->load->view('home_page', $var);				
				}
			
			/* User don't have access to campaign page */
			}else {
				$var['access'] = 'unauthorized';
				$this->load->view('onBoarding_page', $var);
			}
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function tags(){
		if($this->session->name) {
			$this->load->model('Tag_model','tag');
			$this->load->model('User_model','user');
			$tagData = $this->tag->getAll();
			$userData = $this->user->getAll();
			$response = json_decode($tagData);
			$responseUser = json_decode($userData);

			/* Assigning data to view using $var */
			if(isset($response)) {
				$var['response_tag'] = $response->data;
			}			
			$var['page_selected'] = 'tags';
			$var['response_user'] = $responseUser->data;
			
			/* Condition if tag name already exist when add new tag */
			if(isset($_REQUEST['error']) && $_REQUEST['error'] == '1') {
				$var['error_name'] = '1';
				$this->load->view('home_page', $var);	
			
			/* Go to tag page normally */
			}else {
				$this->load->view('home_page', $var);
			}
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function addCampaign() {
		if($this->session->name) {
			/* Get data from view form */
			$campaign_name = $_POST['campaign_name'];
			$start_cron = $_POST['start_date'];
			$action_time = $_POST['action_time'];
			$repeat = $_POST['repeat_num'];
			$requested_by = $_POST['request_by'];
			$action_id = $_POST['action-step-1'];
			$created_by = $this->session->ID;
			$created_date = sekarang();

			$action_type = array();

			/* =============================================Hard coded limiting into 3 tag for urgent demand======================================== */
			/* ========================================PS: Should be looping for the future development======================================== */
			
			/* Getting data from tag selector */
			$tag_id = array();
			
			if(isset($_POST['tag-3'])) {			// if 3 tag choosen
				$count = 3;
				$tag_id[2] = $_POST['tag-3'];
				$tag_id[1] = $_POST['tag-2'];
				$tag_id[0] = $_POST['tag-1'];

			}else if(isset($_POST['tag-2'])) {		// if 2 tag choosen
				$count = 2;
				$tag_id[1] = $_POST['tag-2'];
				$tag_id[0] = $_POST['tag-1'];
							
			}else if(isset($_POST['tag-1'])) {		// if 1 tag choosen
				$count = 1;
				$tag_id[0] = $_POST['tag-1'];	
			}

			/* Getting data from title in step box */
			$title = array();
			$title[0] = null;
			$title[1] = null;
			$title[2] = null;
			
			/* Getting data from content in step box */
			$content = array();
			$content[0] = null;
			$content[1] = null;
			$content[2] = null;

			/* Getting data from action (type, delay) in step box */
			$delay = array();
			if(isset($_POST['action-step-3'])) {							// if 3 step declared
				$count_seq = 3;
				$delay[2] = $_POST['day-delay-3'];
				$delay[1] = $_POST['day-delay-2'];
				$delay[0] = $_POST['day-delay-1'];

				/* Getting action detail in step 3 */
				if($_POST['action-step-3'] == 'push_notification') {
					$title[2] = $_POST['title-step-3'];
					$content[2] = $_POST['content-step-3'];
					$action_seq[2] = 1;
				}else if($_POST['action-step-3'] == 'sms') {
					$content[2] = $_POST['content-step-3'];
					$action_seq[2] = 2;
				}else if($_POST['action-step-3'] == 'call') {
					$action_seq[2] = 3;
				}

				/* Getting action detail in step 2 */
				if($_POST['action-step-2'] == 'push_notification') {
					$title[1] = $_POST['title-step-2'];
					$content[1] = $_POST['content-step-2'];
					$action_seq[1] = 1;
				}else if($_POST['action-step-2'] == 'sms') {
					$content[1] = $_POST['content-step-2'];
					$action_seq[1] = 2;
				}else if($_POST['action-step-2'] == 'call') {
					$action_seq[1] = 3;
				}

				/* Getting action detail in step 1 */
				if($_POST['action-step-1'] == 'push_notification') {
					$title[0] = $_POST['title-step-1'];
					$content[0] = $_POST['content-step-1'];
					$action_seq[0] = 1;
				}else if($_POST['action-step-1'] == 'sms') {
					$content[0] = $_POST['content-step-1'];
					$action_seq[0] = 2;
				}else if($_POST['action-step-1'] == 'call') {
					$action_seq[0] = 3;
				}

				
				
			}else if(isset($_POST['action-step-2'])) {						// if 2 step declared
				$count_seq = 2;
				$delay[1] = $_POST['day-delay-2'];
				$delay[0] = $_POST['day-delay-1'];

				/* Getting action detail in step 2 */
				if($_POST['action-step-2'] == 'push_notification') {
					$title[1] = $_POST['title-step-2'];
					$content[1] = $_POST['content-step-2'];
					$action_seq[1] = 1;
				}else if($_POST['action-step-2'] == 'sms') {
					$content[1] = $_POST['content-step-2'];
					$action_seq[1] = 2;
				}else if($_POST['action-step-2'] == 'call') {
					$action_seq[1] = 3;
				}

				/* Getting action detail in step 1 */
				if($_POST['action-step-1'] == 'push_notification') {
					$title[0] = $_POST['title-step-1'];
					$content[0] = $_POST['content-step-1'];
					$action_seq[0] = 1;
				}else if($_POST['action-step-1'] == 'sms') {
					$content[0] = $_POST['content-step-1'];
					$action_seq[0] = 2;
				}else if($_POST['action-step-1'] == 'call') {
					$action_seq[0] = 3;
				}
			}else if (isset($_POST['action-step-1'])) {						// if 1 step declared
				$count_seq = 1;
				$delay[0] = $_POST['day-delay-1'];

				/* Getting action detail in step 1 */
				if($_POST['action-step-1']=='push_notification') {
					$title[0] = $_POST['title-step-1'];
					$content[0] = $_POST['content-step-1'];
					$action_seq[0] = 1;
				}else if($_POST['action-step-1'] == 'sms') {
					$content[0] = $_POST['content-step-1'];
					$action_seq[0] = 2;
				}else if($_POST['action-step-1'] == 'call') {
					$action_seq[0] = 3;
				}
			}

			/* Assigning data into array for insert batch parameter (tb_campaign_details) */
			$parameterCampaign = array();
			for($i = 0; $i < $count; $i++) {
				$parameterCampaign[$i] = array(
					'tag_id' 		=> $tag_id[$i],
					'start_cron' 	=> $start_cron,
					'action_time' 	=> $action_time,
					'repeater' 		=> $repeat,
					'requested_by' 	=> $requested_by,
					'created_by' 	=> $created_by,
					'created_date' 	=> $created_date,	
				);
			}
			$this->load->model('Campaign_model','campaign');
			$data = $this->campaign->add($parameterCampaign);
			$response = json_decode($data);

			/* If inserting data to tb_campaign_details SUCCESS, */
			/* Assigning data into array for insert batch parameter (tb_sequences) */
			if($response->status != 'Gagal') {
				$campaign_name_id = $response->campaign_name_id;
				
				$parameterSequenceAction = array();
				for($j = 0; $j < $count_seq; $j++) {
					$parameterSequenceAction[$j] = array(
						"campaign_name_id" 	=> $campaign_name_id,
						"action_id" 		=> $action_seq[$j],
						"sequence" 			=> $j + 1,
						"title" 			=> $title[$j],
						"content" 			=> $content[$j],
						"delay_time" 		=> $delay[$j],
						"created_date" 		=> sekarang(),
					);
				}
				$this->load->model('Sequences_model','sequence_action');
				$data_action = $this->sequence_action->add($parameterSequenceAction);
				redirect(base_url().'Main/campaign');
			
			/* If error, assigning parameter error which is campaign name already exist */
			}else if ($response->status == 'Gagal') {
				redirect(base_url().'Main/campaign?error=1');				
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}	
	}

	public function addTag() {
		if($this->session->name) {
			$this->load->model('Tag_model','tag');
			$addTag = $this->tag->add();
			$response = json_decode($addTag);

			if($response->status != 'Gagal'){
				redirect(base_url().'Main/tags');	

			/* If error, assigning parameter error which is tag name already exist */
		}else {
				redirect(base_url().'Main/tags?error=1');								
			}
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}	
	}

	public function deleteTag($id) {
		if($this->session->name) {
			$this->load->model('Tag_model','tag');
			$deleteTag = $this->tag->delete($id);
			$response = json_decode($deleteTag);

			redirect(base_url().'Main/tags');		
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function updateStatusCampaign($id, $status) {
		if($this->session->name) {
			$this->load->model('Campaign_model','campaign');
			$updateStatus = $this->campaign->updateStatus($id, $status);
			$response = json_decode($updateStatus);
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function campaignDetail($campaign_name_id) {
		if($this->session->name) {
			$this->load->model('Campaign_model','campaign');
			$data = $this->campaign->getCampaignDetails($campaign_name_id);
			$response = json_decode($data);
			$this->load->view('home_page', $var);
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function report(){		
		$access = array();
		foreach($this->session->access as $key) {
			if($key['path'] == 'crm_campaign') {
				$access = $key;
			}
		}

		if(isset($access['path']) && $access['path'] == 'crm_campaign') {
			$var['page_selected'] = 'report';		
			$this->load->view('home_page', $var);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
		

	}

	public function cs(){
		$access = array();
		foreach($this->session->access as $key) {
			if($key['path'] == 'crm_campaign') {
				$access = $key;
			}
		}

		if(isset($access['path']) && $access['path'] == 'crm_campaign') {
			$var['page_selected'] = 'cs';
			$this->load->view('home_page', $var);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}
		$var['page_selected'] = 'cs';
			
		$this->load->view('home_page', $var);

	}
}
