<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_campaign extends CI_Controller {

    var $updateStartCron = false;

    public function index() {
        /* CRON CAMPAIGN JALAN TIAP SEKIAN MENIT */
        // $this->load->model('History_model','history');  
        $this->load->model('Campaign_model','campaign');
        $response = $this->campaign->getAll_cron('cron');
        $this->load->model('Report_model','report');      
        $res = json_decode($response)->data;
        $limiterCampaign = count($res);
        // print_r($res);die();
        $reportData = array();
        $emailData = array();
        
        /* LOOPING CAMPAIGN YG STATUSNYA ACTIVE */
        for($i=0; $i<$limiterCampaign; $i++) {
            /* CHECKING APAKAH HARI INI COCOK DENGAN HARI START CRON */
            if(dateNow() >= $res[$i]->start_cron) {     //harus dateNow() untuk testing dihardcode '2018-04-16'
                /* PILIH BATCH YG MASUK DALAM CAMPAIGN DAN STATUS = 0 */
                $this->load->model('Batch_model','batch');
                $responseBatch = $this->batch->getBatchInactive($res[$i]->campaign_name_id);
                

                $resBatch = json_decode($responseBatch)->data;
                $limiterBatch = count($resBatch);
                /* LOOPING BATCH */
                for($u=0; $u<$limiterBatch; $u++) {

                    /* CHECKING APAKAH JAM SEKARANG SUDAH SESUAI DENGAN JAM DI COLUMN ACTION_TIME TABLE CAMPAIGN */
                    if(timeNow() >= $res[$i]->action_time) {

                        /* YANG LAST ACTION NYA 0 = BELUM ADA ACTION */
                        if($resBatch[$u]->last_action_sequence == '0') {
                            $this->load->model('Sequences_model','sequence');
                            $responseSequence = $this->sequence->getDetails($resBatch[$u]->campaign_name_id, '1');
                            $resSequence = json_decode($responseSequence)->data;
                            $limiterSequence = count($resSequence);
                            /* LOOPING SEQUENCE PER MASING2 UNIQUE ID BATCH PER CAMPAIGN */
                            for($j=0; $j<$limiterSequence; $j++) {

                                /* KALO DELAY TIME NYA MEMENUHI */
                                $this->load->model('Action_model','action');
                                $responseAction = $this->action->getById($resSequence[$j]->action_id);
                                $resAction = json_decode($responseAction)->data;
                                $limiterAction = count($resAction);
                                if($resAction[0]->action_name == 'Push Notification') {
                                    echo "=====Action push notif ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                    $responsePushNotif = 'SUCCESS';

                                    /* KALO RESPONSE API SUCCESS */
                                    if($responsePushNotif=='SUCCESS') {
                                        $this->load->model('Batch_model','batch');
                                        $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'1');
                                        $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                        echo 'last action update to 1<br>';                                                                                                                      
                                    }

                                    $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['1']['Push Notification'][] = [
                                        'kcp_id' => $resBatch[$u]->kcp_id, 
                                        'kcp_name' => $resBatch[$u]->kcp_name, 
                                        'mobile_no' => $resBatch[$u]->mobile_no, 
                                        'title' => $resSequence[$j]->title, 
                                        'content' => $resSequence[$j]->content,
                                    ];

                                    $reportData[] = [
                                        'retailer_id' => $resBatch[$u]->kcp_id,
                                        'kcp_name' => $resBatch[$u]->kcp_name,
                                        'campaign_name' => $resBatch[$u]->campaign_name,
                                        'tag' => $resBatch[$u]->tag_name,
                                        'type_name' => 'Push_Notification',
                                        'phone_number' => $resBatch[$u]->mobile_no,
                                        'date_hitted' => $responseUpdateDate,
                                        'status' => $resBatch[$u]->status,
                                        'batch_id' => $resBatch[$u]->batch_id,
                                        'address' => $resBatch[$u]->address,
                                        'status_progress' => 'Push_Notification',
                                        // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                        // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                        // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                        // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                    ];
                                }else if($resAction[0]->action_name == 'SMS') {
                                    echo "=====Action SMS ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                    $responseSMS = 'SUCCESS';
                                    if($responseSMS=='SUCCESS') {
                                        $this->load->model('Batch_model','batch');
                                        $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'1');
                                        $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                        echo 'last action update to 1<br>';
                                    }
                                    $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['1']['SMS'][] = [
                                        'kcp_id' => $resBatch[$u]->kcp_id, 
                                        'kcp_name' => $resBatch[$u]->kcp_name, 
                                        'mobile_no' => $resBatch[$u]->mobile_no, 
                                        'title' => $resSequence[$j]->title, 
                                        'content' => $resSequence[$j]->content,
                                    ];

                                    $reportData[] = [
                                        'retailer_id' => $resBatch[$u]->kcp_id,
                                        'kcp_name' => $resBatch[$u]->kcp_name,
                                        'campaign_name' => $resBatch[$u]->campaign_name,
                                        'tag' => $resBatch[$u]->tag_name,
                                        'type_name' => $resSequence[$j]->action_name,
                                        'phone_number' => $resBatch[$u]->mobile_no,
                                        'date_hitted' => $responseUpdateDate,
                                        'status' => $resBatch[$u]->status,
                                        'batch_id' => $resBatch[$u]->batch_id,
                                        'address' => $resBatch[$u]->address,
                                        'status_progress' => 'SMS',
                                        // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                        // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                        // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                        // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                    ];
                                }else if($resAction[0]->action_name == 'Call') {
                                    echo "=====Action Call ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                    $responseCall = 'SUCCESS';
                                    if($responseCall=='SUCCESS') {
                                        $this->load->model('Batch_model','batch');
                                        $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'1');
                                        $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                        echo 'last action update to 1<br>';
                                    }
                                    $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['1']['Call'][] = [
                                        'kcp_id' => $resBatch[$u]->kcp_id, 
                                        'kcp_name' => $resBatch[$u]->kcp_name, 
                                        'mobile_no' => $resBatch[$u]->mobile_no, 
                                        'title' => $resSequence[$j]->title, 
                                        'content' => $resSequence[$j]->content,
                                    ];

                                    $reportData[] = [
                                        'retailer_id' => $resBatch[$u]->kcp_id,
                                        'kcp_name' => $resBatch[$u]->kcp_name,
                                        'campaign_name' => $resBatch[$u]->campaign_name,
                                        'tag' => $resBatch[$u]->tag_name,
                                        'type_name' => $resSequence[$j]->action_name,
                                        'phone_number' => $resBatch[$u]->mobile_no,
                                        'date_hitted' => $responseUpdateDate,
                                        'status' => $resBatch[$u]->status,
                                        'batch_id' => $resBatch[$u]->batch_id,
                                        'address' => $resBatch[$u]->address,
                                        'status_progress' => 'Call (Open)',
                                        // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                        // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                        // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                        // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                    ];
                                }
                                
                            }
                        }else if($resBatch[$u]->last_action_sequence == '1') {
                            $this->load->model('Sequences_model','sequence');

                            /* AMBIL SEQUENCE KE-1 UNTUK DAPET DELAY_TIME NYA */
                            $responseSequencePrev = $this->sequence->getDetails($resBatch[$u]->campaign_name_id, '1');
                            $resSequencePrev = json_decode($responseSequencePrev)->data;

                            /* DAPETIN JEDA HARI SETELAH ACTION SEBELUMNYA DI EXECUTE */                        
                            $responseIntervalDay = $this->batch->getIntervalDay($resBatch[$u]->batch_id, $resSequencePrev[0]->delay_time);
                            
                            /* CEK APAKAH HARI INI JEDA HARI NYA SUDAH TERLEWATI */    
                            // updated_date di table batch = waktu action sebelumnya di eksekusi
                            // jika hari ini adalah waktu eksekusi nya berdasarkan update_date + delay_time                    
                            if(sekarang()>=$responseIntervalDay) {   //harud sekarang() untuk testing dihardcode'2018-04-14 19:00:00'

                                $responseSequence = $this->sequence->getDetails($resBatch[$u]->campaign_name_id, '2');
                                $resSequence = json_decode($responseSequence)->data;
                                $limiterSequence = count($resSequence);
                                /* LOOPING SEQUENCE PER MASING2 UNIQUE ID BATCH PER CAMPAIGN */
                                for($j=0; $j<$limiterSequence; $j++) {
                                    /* KALO ACTION ID = 1 */

                                    // print_r($responseIntervalDay);die();
                                    
                                    $this->load->model('Action_model','action');
                                    $responseAction = $this->action->getById($resSequence[$j]->action_id);
                                    $resAction = json_decode($responseAction)->data;
                                    $limiterAction = count($resAction);

                                    if($resAction[0]->action_name == 'Push Notification') {
                                        echo "=====Action push notif ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                        $responsePushNotif = 'SUCCESS';

                                        if($responsePushNotif=='SUCCESS') {
                                            $this->load->model('Batch_model','batch');
                                            $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'2');
                                            $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                            echo 'last action update to 2<br>';
                                        }
                                        $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['2']['Push Notification'][] = [
                                            'kcp_id' => $resBatch[$u]->kcp_id, 
                                            'kcp_name' => $resBatch[$u]->kcp_name, 
                                            'mobile_no' => $resBatch[$u]->mobile_no, 
                                            'title' => $resSequence[$j]->title, 
                                            'content' => $resSequence[$j]->content,
                                        ];

                                        $reportData[] = [
                                            'retailer_id' => $resBatch[$u]->kcp_id,
                                            'kcp_name' => $resBatch[$u]->kcp_name,
                                            'campaign_name' => $resBatch[$u]->campaign_name,
                                            'tag' => $resBatch[$u]->tag_name,
                                            'type_name' => 'Push_Notification',
                                            'phone_number' => $resBatch[$u]->mobile_no,
                                            'date_hitted' => $responseUpdateDate,
                                            'status' => $resBatch[$u]->status,
                                            'batch_id' => $resBatch[$u]->batch_id,
                                            'address' => $resBatch[$u]->address,
                                            'status_progress' => 'Push_Notification',
                                            // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                            // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                            // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                            // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                        ];
                                    }else if($resAction[0]->action_name == 'SMS') {
                                        echo "=====Action SMS ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                        $responseSMS = 'SUCCESS';

                                        if($responseSMS=='SUCCESS') {
                                            $this->load->model('Batch_model','batch');
                                            $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'2');
                                            $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                            echo 'last action update to 2<br>';
                                        }
                                        $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['2']['SMS'][] = [
                                            'kcp_id' => $resBatch[$u]->kcp_id, 
                                            'kcp_name' => $resBatch[$u]->kcp_name, 
                                            'mobile_no' => $resBatch[$u]->mobile_no, 
                                            'title' => $resSequence[$j]->title, 
                                            'content' => $resSequence[$j]->content,
                                        ];

                                        $reportData[] = [
                                            'retailer_id' => $resBatch[$u]->kcp_id,
                                            'kcp_name' => $resBatch[$u]->kcp_name,
                                            'campaign_name' => $resBatch[$u]->campaign_name,
                                            'tag' => $resBatch[$u]->tag_name,
                                            'type_name' => $resSequence[$j]->action_name,
                                            'phone_number' => $resBatch[$u]->mobile_no,
                                            'date_hitted' => $responseUpdateDate,
                                            'status' => $resBatch[$u]->status,
                                            'batch_id' => $resBatch[$u]->batch_id,
                                            'address' => $resBatch[$u]->address,
                                            'status_progress' => 'SMS',
                                            // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                            // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                            // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                            // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                        ];
                                    }else if($resAction[0]->action_name == 'Call') {
                                        echo "=====Action Call ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                        $responseCall = 'SUCCESS';
                                        
                                        if($responseCall=='SUCCESS') {
                                            $this->load->model('Batch_model','batch');
                                            $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'2');
                                            $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                            echo 'last action update to 2<br>';
                                        }
                                        $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['2']['Call'][] = [
                                            'kcp_id' => $resBatch[$u]->kcp_id, 
                                            'kcp_name' => $resBatch[$u]->kcp_name, 
                                            'mobile_no' => $resBatch[$u]->mobile_no, 
                                            'title' => $resSequence[$j]->title, 
                                            'content' => $resSequence[$j]->content,
                                        ];

                                        $reportData[] = [
                                            'retailer_id' => $resBatch[$u]->kcp_id,
                                            'kcp_name' => $resBatch[$u]->kcp_name,
                                            'campaign_name' => $resBatch[$u]->campaign_name,
                                            'tag' => $resBatch[$u]->tag_name,
                                            'type_name' => $resSequence[$j]->action_name,
                                            'phone_number' => $resBatch[$u]->mobile_no,
                                            'date_hitted' => $responseUpdateDate,
                                            'status' => $resBatch[$u]->status,
                                            'batch_id' => $resBatch[$u]->batch_id,
                                            'address' => $resBatch[$u]->address,
                                            'status_progress' => 'Call (Open)',
                                            // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                            // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                            // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                            // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                        ];
                                    }
                                }
                            }                            
                        }else if($resBatch[$u]->last_action_sequence == '2') {
                            $this->load->model('Sequences_model','sequence');
                            /* AMBIL SEQUENCE KE-2 UNTUK DAPET DELAY_TIME NYA */                        
                            $responseSequencePrev = $this->sequence->getDetails($resBatch[$u]->campaign_name_id, '2');
                            $resSequencePrev = json_decode($responseSequencePrev)->data;

                            /* DAPETIN JEDA HARI SETELAH ACTION SEBELUMNYA DI EXECUTE */
                            $responseIntervalDay = $this->batch->getIntervalDay($resBatch[$u]->batch_id, $resSequencePrev[0]->delay_time);

                            /* CEK APAKAH HARI INI JEDA HARI NYA SUDAH TERLEWATI */
                            if(sekarang()>=$responseIntervalDay) {   //harus sekarang() untuk testing dihardcode '2018-04-16 19:00:00'
                                /* LOOPING SEQUENCE PER MASING2 UNIQUE ID BATCH PER CAMPAIGN */
                                $responseSequence = $this->sequence->getDetails($resBatch[$u]->campaign_name_id, '3');
                                $resSequence = json_decode($responseSequence)->data;
                                $limiterSequence = count($resSequence);
                                for($j=0; $j<$limiterSequence; $j++) {
                                    /* KALO ACTION ID = 1 */                                 
                                        $this->load->model('Action_model','action');
                                        $responseAction = $this->action->getById($resSequence[$j]->action_id);
                                        $resAction = json_decode($responseAction)->data;
                                        $limiterAction = count($resAction);
                                        if($resAction[0]->action_name == 'Push Notification') {
                                            echo "=====Action push notif ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                            $responsePushNotif = 'SUCCESS';
                                            if($responsePushNotif=='SUCCESS') {
                                                $this->load->model('Batch_model','batch');
                                                $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'3');
                                                $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                                echo 'last action update to 3<br>';
                                            }
                                            $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['3']['Push Notification'][] = [
                                                'kcp_id' => $resBatch[$u]->kcp_id, 
                                                'kcp_name' => $resBatch[$u]->kcp_name, 
                                                'mobile_no' => $resBatch[$u]->mobile_no, 
                                                'title' => $resSequence[$j]->title, 
                                                'content' => $resSequence[$j]->content,
                                            ];

                                            $reportData[] = [
                                                'retailer_id' => $resBatch[$u]->kcp_id,
                                                'kcp_name' => $resBatch[$u]->kcp_name,
                                                'campaign_name' => $resBatch[$u]->campaign_name,
                                                'tag' => $resBatch[$u]->tag_name,
                                                'type_name' => 'Push_Notification',
                                                'phone_number' => $resBatch[$u]->mobile_no,
                                                'date_hitted' => $responseUpdateDate,
                                                'status' => $resBatch[$u]->status,
                                                'batch_id' => $resBatch[$u]->batch_id,
                                                'address' => $resBatch[$u]->address,
                                                'status_progress' => 'Push_Notification',
                                                // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                                // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                                // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                                // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                            ];
                                        }else if($resAction[0]->action_name == 'Call') {
                                            echo "=====Action Call ke {$resBatch[$u]->kcp_id} dengan nomor {$resBatch[$u]->mobile_no}=====<br>";
                                            $responseCall = 'SUCCESS';
                                            if($responseCall=='SUCCESS') {
                                                $this->load->model('Batch_model','batch');
                                                $responseUpdateLastAction = $this->batch->updateLastAction($resBatch[$u]->batch_id,'3');
                                                $responseUpdateDate = $this->batch->updateDate($resBatch[$u]->batch_id);
                                                echo 'last action update to 3<br>';
                                            }
                                            $emailData[$res[$i]->campaign_name][$resBatch[$u]->tag_name]['3']['Call'][] = [
                                                'kcp_id' => $resBatch[$u]->kcp_id, 
                                                'kcp_name' => $resBatch[$u]->kcp_name, 
                                                'mobile_no' => $resBatch[$u]->mobile_no, 
                                                'title' => $resSequence[$j]->title, 
                                                'content' => $resSequence[$j]->content,
                                            ];

                                            $reportData[] = [
                                                'retailer_id' => $resBatch[$u]->kcp_id,
                                                'kcp_name' => $resBatch[$u]->kcp_name,
                                                'campaign_name' => $resBatch[$u]->campaign_name,
                                                'tag' => $resBatch[$u]->tag_name,
                                                'type_name' => $resSequence[$j]->action_name,
                                                'phone_number' => $resBatch[$u]->mobile_no,
                                                'date_hitted' => $responseUpdateDate,
                                                'status' => $resBatch[$u]->status,
                                                'batch_id' => $resBatch[$u]->batch_id,
                                                'address' => $resBatch[$u]->address,
                                                'status_progress' => 'Call (Open)',
                                                // 'campaign_name_id' => $resBatch[$u]->campaign_name_id,       // untuk tb_history_batch tadinya
                                                // 'sequence_id' => $resSequence[$j]->sequences_id,             // untuk tb_history_batch tadinya
                                                // 'tag_id' => $resBatch[$u]->tag_id,                           // untuk tb_history_batch tadinya
                                                // 'action_status' => $responsePushNotif,                       // untuk tb_history_batch tadinya
                                            ];
                                        }
                                }
                            }
                            // update start_cron berdasarkan repeater di table campaign_details
                            if ($this->updateStartCron != true) {
                                $this->campaign->updateDate($res[$i]->repeater, $res[$i]->campaign_name_id);
                                $this->updateStartCron = true;
                            }
                        }
                    }
                }
                // $this->campaign->updateDate($res[$i]->repeater, $res[$i]->campaign_name_id);
            }
            

        }

        /* Untuk insert ke reporting table */
        // print_r($reportData);
        if(count($reportData)>0) {
            // $this->history->add($reportData);
            $this->report->add($reportData);
        }

        /* Untuk trial send email */
        // print_r($emailData);
        echo '<br><br><br>';
        // echo sendEmail($emailData);
    }

}
