<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Report_model','report');
	}

	public function index()
	{
		if($this->session->name) {
			$data['page_selected'] = 'report';
			$this->load->helper('url');
			$this->load->helper('form');
			

			$campaigns = $this->report->get_list_campaigns();
			$opt = array('' => 'All Campaigns');
			foreach ($campaigns as $campaign) {
				$opt[$campaign] = $campaign;
			}

			$tags = $this->report->get_list_tags();
			$opt2 = array('' => 'All Tags');
			foreach ($tags as $tag) {
				$opt2[$tag] = $tag;
			}

			$statuses = $this->report->get_list_status();
			$opt3 = array('' => 'All Status');
			foreach ($statuses as $status) {
				$opt3[$status] = $status;
			}

		$data['form_campaign'] = form_dropdown('',$opt,'','id="campaign" class="form-control campaign" style="background-color:#f2f2f2;color:#59595c;height:31px;margin-bottom:7px;padding: 5px;padding-left: 12px;"');
		$data['form_tag'] = form_dropdown('',$opt2,'','id="tag" class="form-control tag" style="background-color:#f2f2f2;color:#59595c;height:31px;margin-bottom:9px;"');
		$data['form_status'] = form_dropdown('',$opt3,'','id="status" class="form-control status" style="background-color:#f2f2f2;color:#59595c;height:32px;margin-bottom:13px;padding: 6px;border-radius: 5px;margin-left:6px;margin-top:-7px;"');

			$this->load->view('home_page', $data);
			}else{
				$this->session->sess_destroy();
				$this->load->view('login_page');
			}		
	}

	public function ajax_list()
	{
		$list = $this->report->get_datatables();
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $report) {
			$no++;
			$row = [];
			$row[] = $report->retailer_id;
			$row[] = $report->kcp_name;
			$row[] = $report->campaign_name;
			$row[] = $report->tag;
			$row[] = $report->status_progress;	
			$row[] = $report->date_hitted;
			$row[] = $report->phone_number;
			$row[] = '<a  style="color:#1594b9;font-family: Roboto Condensed;font-weight:bold;" href="javascript:void(0)" title="Edit" onclick="edit_report('."'".$report->retailer_id."'".')">Open Details</a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->report->count_all(),
						"recordsFiltered" => $this->report->count_filtered(),
						"data" => $data,
						);
		echo json_encode($output);
	}

	public function ajax_edit($r_id)
	{
		$data = [];
		$data1 = $this->report->get_by_id($r_id);
		$data2 = $this->report->get_last_status_report($r_id);
		$data[] = $data1;
		$data[] = $data2;
		// $data = $this->report->get_reporting_detail($r_id);
		echo json_encode($data);
	}


} 
