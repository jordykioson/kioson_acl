<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cs_model','cs');
		
	}

	public function index()
	{
		if($this->session->name) {
			// print_r($_SESSION["name"]);
		$data['page_selected'] = 'cs';
		$this->load->helper('url');
		$this->load->helper('form');

		$campaigns = $this->cs->get_list_campaigns();
		$opt = array('' => 'All Campaigns');
		foreach ($campaigns as $campaign) {
			$opt[$campaign] = $campaign;
		}

		$tags = $this->cs->get_list_tags();
		$opt2 = array('' => 'All Tags');
		foreach ($tags as $tag) {
			$opt2[$tag] = $tag;
		}
		
		$statuses = $this->cs->get_list_status();
		$opt3 = array('' => 'All Status');
		foreach ($statuses as $status) {
			$opt3[$status] = $status;
		}

		$data['form_campaign'] = form_dropdown('',$opt,'','id="campaign" class="form-control campaign" style="background-color:#f2f2f2;color:#59595c;height:31px;margin-bottom:7px;padding: 5px;padding-left: 12px;"');
		$data['form_tag'] = form_dropdown('',$opt2,'','id="tag" class="form-control tag" style="background-color:#f2f2f2;color:#59595c;height:31px;margin-bottom:9px;"');
		$data['form_status'] = form_dropdown('',$opt3,'','id="status" class="form-control status" style="background-color:#f2f2f2;color:#59595c;height:31px;margin-bottom:9px;padding: 6px;border-radius: 5px;margin-left:6px;"');



		$this->load->view('home_page', $data);
		}else{
			$this->session->sess_destroy();
			$this->load->view('login_page');
		}	
	}

	public function ajax_list()
	{
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		// print_r($start.' '.$end);die();
		
		$list = $this->cs->get_datatables($start);
		$data = [];
		$no = $_POST['start'];
		foreach ($list as $cs) {
			$no++;
			$row = [];
			$row[] = $cs->retailer_id;
			$row[] = $cs->kcp_name;
			$row[] = $cs->campaign_name;
			
			$row[] = $cs->status_progress;	
			$row[] = $cs->date_hitted;
			$row[] = $cs->phone_number;
			$row[] = $cs->feedback_detail;
			$row[] = $cs->called_by;
			$row[] = '<a id="DoAction" class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_cs('."'".$cs->retailer_id."'".')">Do Action</a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->cs->count_all(),
						"recordsFiltered" => $this->cs->count_filtered($start),
						"data" => $data,
						);
		echo json_encode($output);
	}

	public function ajax_edit($r_id)
	{
		// echo $r_id;
		// die();
		$array = [];
		$datax = $this->cs->get_by_id($r_id);
		$data2 = $this->cs->get_historycase_by_id($r_id);
		$array[] = $datax;
		$array[] = $data2;
		echo json_encode($array);
		// echo json_encode($data2);

	}

	public function ajax_update()
	{
		$this->_validate();
		$campaign_name=$this->input->post('campaign_name');
		$status_call=$this->input->post('status_call');
		$status_progress=$this->input->post('status_progress');
		
		$kcp_name=$this->input->post('kcp_name');
		$tag=$this->input->post('tag');
		$retailer_id=$this->input->post('retailer_id');
		$phone_number=$this->input->post('phone_number');
		$feedback=$this->input->post('feedback');
		$feedback_detail=$this->input->post('feedback_detail');
		// $called_by =$this->input->post($this->session->name);
		// print_r($this->input->post('campaign_name'));
		// die();
		// $explode=explode(',',$this->input->post('campaign_name'));
		$data = [];
for($i = 0 ; $i < count($campaign_name); $i++){
	if($status_call[$i] == 1){
		$CallProgress = '1st Call';
		$CallTypeName = 'Call1';
	}else if($status_call[$i] == 2){
		$CallProgress = '2nd Call';
		$CallTypeName = 'Call2';
	}else{
		$CallProgress = '3rd Call';
		$CallTypeName = 'Call3';
	}

	if($feedback[$i] == "Need Inspection"){
		$statusAfterCall = 2;
	}else{
		$statusAfterCall = 1;
	}


	if($feedback[$i] == "Need Inspection"){
		$NewFeedbackDetail = "Need Inspection";
	}else if($feedback[$i] == "Need to Follow Up") {
		$NewFeedbackDetail = "Need to Follow Up";
	}else if($feedback[$i] == "Wrong Number") {
		$NewFeedbackDetail = "Wrong Number";
	}
	else if ($feedback[$i] == "Other"){
		$NewFeedbackDetail = $feedback_detail[$i];

	}


		$data[$i]= [
			'campaign_name' => $campaign_name[$i],
			'status' => $statusAfterCall,			
			'status_call' => $status_call[$i],
			'status_progress' => $CallProgress,
			'type_name' => $CallTypeName,
			'kcp_name' => $kcp_name[$i],
			'tag' => $tag[$i],
			'retailer_id' => $retailer_id[$i],
			'phone_number' => $phone_number[$i],
			'feedback' => $feedback[$i],
			'feedback_detail' => $NewFeedbackDetail,
			'called_by' => $this->session->name,
			'date_hitted' => date('Y-m-d')
			
		];

}		
	
			$inseert = $this->cs->save($data);
		echo json_encode(array("status" => TRUE));

		
	}









	// public function ajax_update()
	// {
	// 	$this->_validate();
	// 	$campaign_name=implode(',',$this->input->post('campaign_name'));
	// 	// $explode=explode(',',$this->input->post('campaign_name'));
		
	// 	// $campaign_name=$this->input->post('campaign_name');

		
	// 	$data = [
	// 			// 'feedback' => $this->input->post('feedback'),
	// 			// 'feedback_detail' => $this->input->post('feedback_detail'),
	// 			// 'retailer_id' => $this->input->post('retailer_id'),
	// 			'campaign_name' => $campaign_name,
	// 			// 'status' => $this->input->post('status'),
	// 			// 'kcp_name' => $this->input->post('kcp_name'),
	// 			// 'tag' => $this->input->post('tag'),
	// 			// 'phone_number' => $this->input->post('phone_number'),
	// 			];
	
	// 		$insert = $this->cs->save($data);
	// 		// $insert = $this->person->save($data);

	// 	echo json_encode(array("status" => TRUE));

		
	// }
		// when  try to +1
	// public function ajax_update() {
	// 	$this->_validate();

	// 	$feedback = $this->input->post('feedback');
	// 	$retailer_id = $this->input->post('retailer_id');
	// 	if (!is_null($feedback) && !is_null($retailer_id)) {
	// 		// only increment: "whenever the "feedback" has submited or post"
	// 		$this->cs->update_feedback($retailer_id, $feedback);
	// 		echo json_encode(["status" => TRUE]);
	// 	} else {
	// 		echo json_encode(['status' => FALSE]); //?
	// 	}
	// }

	private function _validate()
	{
		$data = [];
		$data['error_string'] = [];
		$data['inputerror'] = [];
		$data['status'] = TRUE;

		if($this->input->post('feedback') == '')
		{
			$data['inputerror'][] = 'feedback';
			$data['error_string'][] = 'feedback is required';
			$data['status'] = FALSE;
		}

		// if($this->input->post('retailer_id') == '')
		// {
		// 	$data['inputerror'][] = 'retailer_id';
		// 	$data['error_string'][] = 'retailer_id is required';
		// 	$data['status'] = FALSE;
		// }
		if($this->input->post('campaign_name') == '')
		{
			$data['inputerror'][] = 'campaign_name';
			$data['error_string'][] = 'campaign name required!';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}



}
